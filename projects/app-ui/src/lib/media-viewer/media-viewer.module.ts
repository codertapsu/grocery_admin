import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatDialogModule } from '@angular/material/dialog';

import { MediaViewerComponent } from './media-viewer.component';
import { MediaViewerService } from './media-viewer.service';

@NgModule({
  declarations: [MediaViewerComponent],
  imports: [CommonModule, MatDialogModule],
  exports: [MediaViewerComponent],
  providers: [MediaViewerService],
})
export class MediaViewerModule {}
