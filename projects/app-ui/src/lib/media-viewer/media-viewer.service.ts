import { Injectable } from '@angular/core';
import { Media } from '@core/models/media.model';
import {MatDialog} from '@angular/material/dialog';

@Injectable()
export class MediaViewerService {

  public constructor() { }

  public open(medias: Media[]): void {
    this.dialog.open(DialogDataExampleDialog, {
      data: {
        animal: 'panda',
      },
    });
  }

  public select(medias: Media[]): void {}
}
