import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ui-media-viewer',
  templateUrl: './media-viewer.component.html',
  styleUrls: ['./media-viewer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MediaViewerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
