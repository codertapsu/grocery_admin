import { ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ui-split-button',
  templateUrl: './split-button.component.html',
  styleUrls: ['./split-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SplitButtonComponent implements OnInit {
  @ViewChild('splitButton', { static: true }) splitButton: ElementRef<HTMLButtonElement>;
  @ViewChild('popupButton', { static: true }) popupButton: ElementRef<HTMLElement>;

  constructor() {}

  ngOnInit(): void {
    // // support escape key
    // popupButtons.on('keyup', e => {
    //   if (e.code === 'Escape') e.target.blur();
    // });

    // popupButtons.on('focusin', e => {
    //   e.currentTarget.setAttribute('aria-expanded', true);
    // });

    // popupButtons.on('focusout', e => {
    //   e.currentTarget.setAttribute('aria-expanded', false);
    // });

    // // respond to any button interaction
    // splitButtons.on('click', event => {
    //   if (event.target.nodeName !== 'BUTTON') return;
    //   console.info(event.target.innerText);
    // });
  }
}
