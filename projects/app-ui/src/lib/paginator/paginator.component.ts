import { ChangeDetectionStrategy, Component, Input, OnInit, Output, TemplateRef } from '@angular/core';

import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, tap, map, pairwise } from 'rxjs/operators';

import { OverlayRef, OverlayService } from '../overlay';
import { PageItem } from './models/page-item.model';

function rangePagination(start: number, end: number): PageItem[] {
  return Array(end - start + 1)
    .fill('')
    .map((_, idx) => ({ page: start + idx, type: 'page' } as PageItem));
}

@Component({
  selector: 'ui-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginatorComponent implements OnInit {
  private readonly currentPage$ = new BehaviorSubject<number>(1);
  private readonly pageSize$ = new BehaviorSubject<number>(20);
  private readonly total$ = new BehaviorSubject<number>(0);

  private _optionsBoxRef: OverlayRef<unknown>;
  private _maxPage: number;

  public readonly sizeOptions = [20, 50, 100];

  public pageItems$: Observable<PageItem[]>;

  @Input() public step = 2;
  @Output() public pageChange = combineLatest([
    this.currentPage$.pipe(distinctUntilChanged(), tap(console.log)),
    this.pageSize$.pipe(distinctUntilChanged()),
  ]).pipe(
    map(([index, size]) => ({
      size,
      index,
    })),
  );

  @Input() set currentPage(value: number) {
    if (value && this.currentPage$.value !== value) {
      this.currentPage$.next(value);
    }
  }
  get currentPage(): number {
    return this.currentPage$.value;
  }

  @Input() set pageSize(value: number) {
    if (value && this.pageSize$.value !== value) {
      this.pageSize$.next(value);
    }
  }
  get pageSize(): number {
    return this.pageSize$.value;
  }

  @Input() set length(value: number) {
    if (!(value === null || value === undefined) && this.total$.value !== value) {
      this.total$.next(value);
    }
  }
  get total(): number {
    return this.total$.value;
  }

  public constructor(private _overlayService: OverlayService) {}

  public ngOnInit(): void {
    this.pageItems$ = combineLatest([this.currentPage$, this.pageSize$, this.total$]).pipe(
      map(([currentPage, pageSize, total]) => {
        this._maxPage = Math.ceil(total / pageSize);

        let paginationValue: PageItem[] = [];

        if (this._maxPage < this.step * 2 + 6) {
          paginationValue = rangePagination(1, this._maxPage);
        } else if (currentPage < this.step * 2 + 1) {
          paginationValue = [
            ...rangePagination(1, this.step * 2 + 4),
            { type: 'more', page: undefined },
            { type: 'page', page: this._maxPage },
          ];
        } else if (currentPage > this._maxPage - this.step * 2) {
          paginationValue = [
            { page: 1, type: 'page' },
            { page: undefined, type: 'more' },
            ...rangePagination(this._maxPage - this.step * 2 - 2, this._maxPage),
          ];
        } else {
          paginationValue = [
            { page: 1, type: 'page' },
            { page: undefined, type: 'more' },
            ...rangePagination(currentPage - this.step, currentPage + this.step + 1),
            { type: 'more', page: undefined },
            { type: 'page', page: this._maxPage },
          ];
        }

        return paginationValue;
      }),
    );
  }

  public openOptionsBox(event: Event, template: TemplateRef<unknown>): void {
    this._optionsBoxRef = this._overlayService.openPopover(event.currentTarget as HTMLElement, template);
  }

  public changSize(size: number): void {
    if (this.pageSize$.value !== size) {
      if (Math.ceil(this.total$.value / size) < this.currentPage$.value) {
        this.currentPage$.next(1);
      }
      this.pageSize$.next(size);
    }
    this._optionsBoxRef.close();
  }

  public selectPage(value: number): void {
    if (this.currentPage$.value !== value) {
      this.currentPage$.next(value);
    }
  }

  public onPrev(): void {
    if (this.currentPage$.value > 1) {
      const newPageIndex = this.currentPage$.value - 1;
      this.currentPage$.next(newPageIndex);
    }
  }

  public onNext(): void {
    if (this.currentPage$.value < this._maxPage) {
      const newPageIndex = this.currentPage$.value + 1;
      this.currentPage$.next(newPageIndex);
    }
  }
}
