export interface Pagination {
  size: number;
  index: number;
}
