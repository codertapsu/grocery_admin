export interface PageItem {
  type: 'page' | 'more';
  page: number;
}
