import { DOCUMENT } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { fromEvent, merge, Subject } from 'rxjs';
import { filter, switchMap, takeUntil, tap } from 'rxjs/operators';

import { BaseControl, BrowserWindow, WINDOW } from '@common';

import { BasicPoint } from '../models/basic-point.model';
import { Line } from '../models/line.model';
import { Point } from '../models/point.model';

@Component({
  selector: 'ui-signature-pad',
  templateUrl: './signature-pad.component.html',
  styleUrls: ['./signature-pad.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SignaturePadComponent),
      multi: true,
    },
  ],
})
export class SignaturePadComponent extends BaseControl<number[][][]> implements OnInit, OnDestroy {
  private readonly destroy$ = new Subject<void>();
  private readonly svgns = 'http://www.w3.org/2000/svg';
  private readonly smoothing = 0.15;

  @ViewChild('padRef', { static: true }) private pad: ElementRef<HTMLElement>;
  @ViewChild('strokePointsRef', { static: true }) private strokePoints: ElementRef<HTMLElement>;

  private _signing: boolean;
  private _previous: Point;
  private _pointer: BasicPoint;
  private _strokes: Point[];
  private _points: number[][];
  private _cachedValue: number[][][] = [];

  private _requestAF: (callback: FrameRequestCallback) => number;

  public constructor(
    protected override changeDetectorRef: ChangeDetectorRef,
    private renderer: Renderer2,
    private elementRef: ElementRef<HTMLElement>,
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window: BrowserWindow,
  ) {
    super(changeDetectorRef);
  }

  public override writeValue(value: number[][][]): void {
    if (value?.length) {
      setTimeout(() => {
        for (const iterator of value) {
          this.redraw(iterator);
        }
      }, 1);
    }

    super.writeValue(value);
  }

  public ngOnInit(): void {
    this.redraw = this.redraw.bind(this);
    this.draw = this.draw.bind(this);

    this._requestAF =
      this.window.requestAnimationFrame ||
      (this.window as any).webkitRequestAnimationFrame ||
      (this.window as any).mozRequestAnimationFrame;

    const start$ = fromEvent<MouseEvent>(this.pad.nativeElement, 'mousedown').pipe(
      filter(() => !this.readonly),
      takeUntil(this.destroy$),
    );
    const click$ = fromEvent<MouseEvent>(this.pad.nativeElement, 'click').pipe(filter(() => !this.readonly));
    const end$ = fromEvent<MouseEvent>(this.document, 'mouseup').pipe(filter(() => !this.readonly));
    const sign$ = fromEvent<MouseEvent>(this.document, 'mousemove').pipe(
      filter(() => !this.readonly),
      takeUntil(end$),
    );

    start$
      .pipe(
        tap(event => this.start(event)),
        switchMap(() => sign$),
      )
      .subscribe(event => this.sign(event));

    merge(click$, end$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(event => this.end(event));
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  public clear(): void {
    try {
      this.renderer.setAttribute(this.strokePoints.nativeElement, 'points', '');
      this.renderer.removeChild(this.pad.nativeElement, this.pad.nativeElement.querySelector('g'));
      this._cachedValue.length = 0;
      this.updateValue(undefined);
    } catch {}
  }

  public fullscreen(): void {
    this.elementRef.nativeElement.requestFullscreen().catch(err => {
      this.window.alert(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
    });
  }

  public download(): void {
    const svgString = this.pad.nativeElement.outerHTML.toString();
    const blob = new Blob([svgString], { type: 'image/svg+xml;charset=utf-8' });
    const uriContent = URL.createObjectURL(blob);
    const link = this.document.createElement('a');
    link.setAttribute('href', uriContent);
    link.setAttribute('download', 'Your signature');
    this.document.body.appendChild(link);

    link.dispatchEvent(
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: this.window,
      }),
    );
    setTimeout(() => {
      URL.revokeObjectURL(uriContent);
      link.remove();
    }, 1);
  }

  private start(event: MouseEvent): void {
    event.preventDefault();

    this._signing = true;

    const start = Date.now();
    this._pointer = this.position(event);

    const stroke: Point = {
      x: this._pointer.x,
      y: this._pointer.y,
      time: start,
      delta: 0,
      elapsed: 0,
      pressure: this._pointer.pressure,
    };
    this._points = [[this._pointer.x, this._pointer.y]];
    this._strokes = [stroke];
    this._previous = stroke;

    this.markAsTouched();
  }

  private end(event: MouseEvent): void {
    if (this._signing) {
      event.preventDefault();
      this.renderer.setAttribute(this.strokePoints.nativeElement, 'points', '');
      this._requestAF(this.redraw.bind(this, this._points));
    }
    this._signing = false;
  }

  private sign(event: MouseEvent): void {
    event.preventDefault();

    const time = Date.now();
    const elapsed = time - this._previous.time;

    this._pointer = this.position(event);

    const stroke = {
      time,
      elapsed,
      x: this._pointer.x,
      y: this._pointer.y,
      delta: this._previous.delta + this.distance(this._previous, event),
      pressure: this._pointer.pressure,
    };

    this._previous = stroke;
    this._strokes.push(stroke);
    this._points.push([this._pointer.x, this._pointer.y]);

    this._requestAF(this.draw);
  }

  private position(event: MouseEvent): BasicPoint {
    const { top, right, bottom, left } = this.pad.nativeElement.getBoundingClientRect();
    const clientX = Math.min(Math.max(event.clientX, left), right);
    const clientY = Math.min(Math.max(event.clientY, top), bottom);

    return {
      x: Math.round(clientX - left),
      y: Math.round(clientY - top),
      pressure: 0.5,
    };
  }

  private distance(point: Point, event: MouseEvent): number {
    const dx = event.x - point.x;
    const dy = event.y - point.y;

    return Number(Math.sqrt(dx * dx + dy * dy).toFixed(4));
  }

  private draw(): void {
    this.renderer.setAttribute(this.strokePoints.nativeElement, 'points', this._points as unknown as string);
  }

  private redraw(points: number[][]): void {
    this._cachedValue.push(points);

    const path = this.createElement('path', this.svgns);
    const g =
      this.pad.nativeElement.querySelector('g.strokes') ||
      this.createElement('g', this.svgns, this.pad.nativeElement, 'strokes');

    this.renderer.setAttribute(
      this.pad.nativeElement,
      'viewBox',
      '0,0,' +
        this.pad.nativeElement.getBoundingClientRect().width +
        ',' +
        this.pad.nativeElement.getBoundingClientRect().height,
    );

    this.renderer.setAttribute(path, 'd', this.optimize(this.decimate(6, points)));
    this.renderer.setAttribute(g, 'fill', 'none');
    this.renderer.setAttribute(g, 'stroke', 'currentColor');
    this.renderer.setAttribute(g, 'stroke-width', '3');
    this.renderer.appendChild(g, path);
    this.renderer.setAttribute(this.strokePoints.nativeElement, 'points', '');

    this.updateValue(this._cachedValue);
  }

  private decimate(n: number, points: number[][]): number[][] {
    const dec = [];
    dec[0] = points[0];
    for (let i = 0; i < points.length; i++) {
      if (i % n === 0) {
        dec[dec.length] = points[i];
      }
    }

    return dec;
  }

  private bezier(point: number[], i: number, a: number[][]): string {
    const cps = this.joint(a[i - 1], a[i - 2], point);
    const cpe = this.joint(point, a[i - 1], a[i + 1], true);

    return 'C ' + cps[0] + ',' + cps[1] + ' ' + cpe[0] + ',' + cpe[1] + ' ' + point[0] + ',' + point[1];
  }

  private joint(current: number[], prev: number[], next: number[], reverse?: boolean): number[] {
    const p = prev || current;
    const n = next || current;
    const o = this.line(p, n);
    const angle = o.angle + (reverse ? Math.PI : 0);
    const length = o.length * this.smoothing;
    const x = current[0] + Math.cos(angle) * length;
    const y = current[1] + Math.sin(angle) * length;

    return [x, y];
  }

  private line(a: number[], b: number[]): Line {
    const lenX = b[0] - a[0];
    const lenY = b[1] - a[1];

    return {
      length: Math.sqrt(Math.pow(lenX, 2) + Math.pow(lenY, 2)),
      angle: Math.atan2(lenY, lenX),
    };
  }

  private optimize(points: number[][]): string {
    const d = points.reduce((acc, point, i, a) => {
      return i === 0 ? 'M ' + point[0] + ',' + point[1] : acc + ' ' + this.bezier(point, i, a);
    }, '');

    return d;
  }

  private createElement(tag: string, ns: string, target?: HTMLElement, className?: string): HTMLElement {
    const element: HTMLElement = !ns ? this.renderer.createElement(tag) : this.document.createElementNS(ns, tag);
    if (className) {
      this.renderer.addClass(element, className);
    }

    if (!target) {
      return element;
    }

    if (target instanceof Element && target.tagName) {
      this.renderer.appendChild(target, element);
    }

    return element;
  }
}
