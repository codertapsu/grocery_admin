import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SignaturePadComponent } from './signature-pad/signature-pad.component';
import { SignaturePadTwoComponent } from './signature-pad-two/signature-pad-two.component';

@NgModule({
  declarations: [SignaturePadComponent, SignaturePadTwoComponent],
  imports: [CommonModule],
  exports: [SignaturePadComponent, SignaturePadTwoComponent],
})
export class SignaturePadModule {}
