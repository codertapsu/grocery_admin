export interface Line {
  length: number;
  angle: number;
}
