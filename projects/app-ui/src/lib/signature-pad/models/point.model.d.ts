import { BasicPoint } from './basic-point.model';

export interface Point extends BasicPoint {
  time: number;
  delta: number;
  elapsed: number;
}
