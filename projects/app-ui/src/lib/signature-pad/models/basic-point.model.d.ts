import { Coordinates } from '@common';

export interface BasicPoint extends Coordinates {
  pressure: number;
}
