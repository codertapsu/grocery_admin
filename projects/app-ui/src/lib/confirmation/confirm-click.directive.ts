import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  NgZone,
  OnDestroy,
  Output,
  Renderer2,
} from '@angular/core';

import { fromEvent, Subscription } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import { ConfirmationService } from './confirmation.service';

@Directive({
  selector: '[click.confirm]',
})
export class ConfirmClickDirective implements AfterViewInit, OnDestroy {
  private _subscription: Subscription;
  @Output('click.confirm') public confirmed = new EventEmitter<Event>();

  public constructor(
    private readonly zone: NgZone,
    private readonly renderer: Renderer2,
    private confirmationService: ConfirmationService,
    private elementRef: ElementRef<HTMLElement>,
  ) {}

  public ngAfterViewInit(): void {
    // this.zone.runOutsideAngular(() => {
    // this.renderer.listen(this.elementRef.nativeElement, 'click', (event: MouseEvent) => {
    //   console.log('ClickZonelessDirective2');
    //   // this.clickZoneless.emit(event);
    // });
    this._subscription = fromEvent(this.elementRef.nativeElement, 'click')
      .pipe(
        switchMap(event =>
          this.confirmationService.confirm().pipe(
            filter(Boolean),
            map(() => event),
          ),
        ),
      )
      .subscribe(event => {
        console.log('event');

        this.confirmed.emit(event);
      });
    // });
  }
  public ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }
}
