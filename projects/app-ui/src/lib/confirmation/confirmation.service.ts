import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Observable } from 'rxjs';

import { ConfirmationComponent } from './confirmation.component';

@Injectable()
export class ConfirmationService {
  public constructor(private dialog: MatDialog) {}

  public confirm(message?: string, title?: string): Observable<boolean> {
    const dialogRef = this.dialog.open(ConfirmationComponent);

    dialogRef.componentInstance.title = title || 'Confirm Dialog';
    dialogRef.componentInstance.message = message || 'Are you sure you want to do this?';

    return dialogRef.afterClosed();
  }
}
