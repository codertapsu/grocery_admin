import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { ConfirmClickDirective } from './confirm-click.directive';
import { ConfirmationComponent } from './confirmation.component';
import { ConfirmationService } from './confirmation.service';

@NgModule({
  declarations: [ConfirmationComponent, ConfirmClickDirective],
  imports: [CommonModule, MatDialogModule, MatButtonModule],
  providers: [ConfirmationService],
  exports: [ConfirmClickDirective],
})
export class ConfirmationModule {}
