import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  NgZone,
  OnDestroy,
} from '@angular/core';

import { timer } from 'rxjs';

import { WINDOW } from '@common';

@Component({
  selector: 'ui-digital',
  templateUrl: './digital.component.html',
  styleUrls: ['./digital.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DigitalComponent implements AfterViewInit, OnDestroy {
  private _animationFrameId: number;

  public constructor(
    private elementRef: ElementRef<HTMLElement>,
    private ngZone: NgZone,
    @Inject(WINDOW) private window: Window,
  ) {}

  public ngAfterViewInit(): void {
    this.ngZone.runOutsideAngular(() => {
      const current = new Date();
      const animationElements = this.elementRef.nativeElement.querySelectorAll('.movable') as NodeListOf<HTMLElement>;
      animationElements.forEach(element => {
        element.addEventListener('animationend', this.animationend.bind(this));
      });

      const initialSeconds = current.getSeconds().toString().split('').map(Number);
      const currentSecondSecondsElement = this.elementRef.nativeElement.querySelector(
        '.seconds .second .current',
      ) as HTMLElement;
      const nextSecondSecondsElement = this.elementRef.nativeElement.querySelector(
        '.seconds .second .next',
      ) as HTMLElement;
      if (initialSeconds.length > 1) {
      }
      currentSecondSecondsElement.innerText = `${initialSeconds[initialSeconds.length - 1]}`;
      nextSecondSecondsElement.innerText = `${Number(currentSecondSecondsElement.innerText) + 1}`;
      timer(0, 1000).subscribe(() => {
        currentSecondSecondsElement.innerText =
          Number(currentSecondSecondsElement.innerText) === 9
            ? '0'
            : `${Number(currentSecondSecondsElement.innerText) + 1}`;
        nextSecondSecondsElement.innerText =
          Number(nextSecondSecondsElement.innerText) === 9
            ? '0'
            : `${Number(nextSecondSecondsElement.innerText) + 1}`;
      });
    });
    // this.secondNumbers$ = timer(0, 1000).pipe(
    //   map(() => new Date().getSeconds()),
    //   map(value => value.toString().split('').map(Number)),
    //   map(numbers => {
    //     if (numbers.length === 1) {
    //       numbers.unshift(0);
    //     }
    //     const [first, second] = numbers;
    //     const nextFirst = first + 1 > 9 ? 0 : first + 1;
    //     const nextSecond = second + 1 > 9 ? 0 : second + 1;
    //     return [
    //       [first, nextFirst],
    //       [second, nextSecond],
    //     ];
    //   }),
    // );
  }

  public ngOnDestroy(): void {
    this.window.cancelAnimationFrame(this._animationFrameId);
  }

  public animationend(event: any): void {
    console.log(event);
  }

  private updateTime(): void {
    // $("<div />").appendTo("body");
    console.log('updateTime: ', new Date().getMinutes());

    this._animationFrameId = this.window.requestAnimationFrame(this.updateTime.bind(this));
  }
}
