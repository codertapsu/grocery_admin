import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';

import { Observable, of, Subject, timer } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'ui-analog',
  templateUrl: './analog.component.html',
  styleUrls: ['./analog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnalogComponent implements OnInit, OnDestroy {
  private readonly _destroyed$ = new Subject<void>();

  public secondsRotation$: Observable<string>;
  public minutesRotation$: Observable<string>;
  public hoursRotation$: Observable<string>;

  public constructor() {}

  public ngOnInit(): void {
    const current = new Date();
    const initialSeconds = current.getSeconds();
    this.secondsRotation$ = of(initialSeconds).pipe(
      switchMap(initial => timer(0, 1000).pipe(map(increment => (initial + increment) % 60))),
      map(increment => increment * (360 / 60)),
      map(degrees => `rotate(${degrees}deg)`),
    );

    const initialMinutes = current.getMinutes() * 60 + initialSeconds;
    this.minutesRotation$ = of(initialMinutes).pipe(
      switchMap(initial => timer(0, 1000).pipe(map(increment => (initial + increment) % (60 * 60)))),
      map(increment => increment * (360 / (60 * 60))),
      map(degrees => `rotate(${degrees}deg)`),
    );

    const initialHours = current.getHours() * (60 * 60) + initialMinutes;
    this.hoursRotation$ = of(initialHours).pipe(
      switchMap(initial => timer(0, 1000).pipe(map(increment => (initial + increment) % (60 * 60 * 12)))),
      map(increment => increment * (360 / (60 * 60 * 12))),
      map(degrees => `rotate(${degrees}deg)`),
    );
  }

  public ngOnDestroy(): void {}
}
