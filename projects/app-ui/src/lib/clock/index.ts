export { AnalogComponent } from './analog/analog.component';
export { DigitalComponent } from './digital/digital.component';
export { ClockModule } from './clock.module';
