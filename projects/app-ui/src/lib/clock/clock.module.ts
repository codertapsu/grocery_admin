import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AnalogComponent } from './analog/analog.component';
import { DigitalComponent } from './digital/digital.component';

@NgModule({
  declarations: [AnalogComponent, DigitalComponent],
  imports: [CommonModule],
  exports: [AnalogComponent, DigitalComponent],
})
export class ClockModule {}
