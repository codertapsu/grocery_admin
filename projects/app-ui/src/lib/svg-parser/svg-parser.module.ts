import { NgModule } from '@angular/core';
import { SvgParserComponent } from './svg-parser.component';

@NgModule({
  declarations: [SvgParserComponent],
  exports: [SvgParserComponent],
})
export class SvgParserModule {}
