import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  Renderer2,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'ui-svg-parser',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class SvgParserComponent implements OnChanges, OnDestroy {
  private _abortController: AbortController;

  @Input() public className: string;
  @Input() public viewBox: string;
  @Input() public width: string;
  @Input() public height: string;
  @Input() public src: string;

  public constructor(private renderer: Renderer2, private elementRef: ElementRef) {}

  public ngOnChanges(): void {
    this._abortController = new AbortController();
    this.adjustSvgFile(this.src);
  }

  public ngOnDestroy(): void {
    this._abortController.abort();
  }

  private async adjustSvgFile(path: string): Promise<void> {
    const response = await fetch(path, {
      signal: this._abortController.signal,
      headers: {
        'Content-Type': 'image/svg+xml',
      },
    });
    const svgString = await response.text();
    const doc = new DOMParser();
    const xml = doc.parseFromString(svgString, 'image/svg+xml');
    const svgNode = xml.documentElement;

    // this.renderer.setAttribute(svgNode, 'fill', 'currentColor');

    if (this.viewBox) {
      this.renderer.setAttribute(svgNode, 'viewBox', this.viewBox);
    }

    if (this.width) {
      this.renderer.setAttribute(svgNode, 'width', this.width);
    }

    if (this.height) {
      this.renderer.setAttribute(svgNode, 'height', this.height);
    }

    if (this.className) {
      this.className.split(' ').forEach(item => this.renderer.addClass(svgNode, item));
    }

    this.renderer.appendChild(this.elementRef.nativeElement, svgNode);
  }
}
