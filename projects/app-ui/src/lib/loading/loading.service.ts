import { OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { ElementRef, Injectable } from '@angular/core';

import { Subscription, timer } from 'rxjs';

import { LoadingComponent } from './loading.component';
import { OverlayService } from './overlay-services/overlay.service';

export declare type ProgressRef = { subscription: Subscription; overlayRef: OverlayRef };

@Injectable()
export class LoadingService {
  public constructor(private overlayService: OverlayService) {}

  public showProgress(elementRef: ElementRef<HTMLElement>) {
    if (elementRef) {
      const result: ProgressRef = { subscription: null, overlayRef: null };
      result.subscription = timer(500).subscribe(() => {
        this.overlayService.setContainerElement(elementRef.nativeElement);
        const positionStrategy = this.overlayService.position().global().centerHorizontally().centerVertically();
        result.overlayRef = this.overlayService.create({
          positionStrategy: positionStrategy,
          hasBackdrop: true,
        });
        result.overlayRef.attach(new ComponentPortal(LoadingComponent));
      });
      return result;
    } else {
      return null;
    }
  }

  public detach(result: ProgressRef): void {
    if (result) {
      result.subscription.unsubscribe();
      if (result.overlayRef) {
        result.overlayRef.detach();
      }
    }
  }
}
