import { NgModule } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { LoadingComponent } from './loading.component';
import { LoadingService } from './loading.service';
import { OverlayContainerService } from './overlay-services/overlay-container.service';
import { OverlayService } from './overlay-services/overlay.service';

@NgModule({
  declarations: [LoadingComponent],
  imports: [MatProgressSpinnerModule],
  exports: [LoadingComponent],
  providers: [OverlayContainerService, OverlayService, LoadingService],
  bootstrap: [LoadingComponent],
})
export class LoadingModule {}
