import { Directionality } from '@angular/cdk/bidi';
import {
  Overlay,
  OverlayKeyboardDispatcher,
  OverlayOutsideClickDispatcher,
  OverlayPositionBuilder,
  ScrollStrategyOptions,
} from '@angular/cdk/overlay';
import { DOCUMENT, Location } from '@angular/common';
import { ComponentFactoryResolver, Inject, Injectable, Injector, NgZone, Renderer2, RendererFactory2 } from '@angular/core';

import { OverlayContainerService } from './overlay-container.service';

@Injectable()
export class OverlayService extends Overlay {
  private readonly renderer: Renderer2;

  public constructor(
    private rendererFactory: RendererFactory2,
    public override scrollStrategies: ScrollStrategyOptions,
    protected overlayContainerService: OverlayContainerService,
    protected componentFactoryResolver: ComponentFactoryResolver,
    protected positionBuilder: OverlayPositionBuilder,
    protected keyboardDispatcher: OverlayKeyboardDispatcher,
    protected injector: Injector,
    protected ngZone: NgZone,
    protected directionality: Directionality,
    protected location: Location,
    protected outsideClickDispatcher: OverlayOutsideClickDispatcher,
    @Inject(DOCUMENT) protected document: Document,
  ) {
    super(
      scrollStrategies,
      overlayContainerService,
      componentFactoryResolver,
      positionBuilder,
      keyboardDispatcher,
      injector,
      ngZone,
      document,
      directionality,
      location,
      outsideClickDispatcher,
    );
    this.renderer = this.rendererFactory.createRenderer(null, null);
  }

  public setContainerElement(containerElement: HTMLElement): void {
    this.renderer.setStyle(containerElement, 'transform', 'translateZ(0)');
    this.overlayContainerService.setContainerElement(containerElement);
  }
}
