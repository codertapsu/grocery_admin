import { BehaviorSubject, defer, interval } from 'rxjs';
import { filter, map, reduce, share, take, withLatestFrom } from 'rxjs/operators';

import { PauseableTimer } from './models/pauseable-timer.model';

const REFRESHING_SCALE = 10;

export function getPauseableTimer(timeoutSeconds: number, pause: BehaviorSubject<boolean>): PauseableTimer {
  const scaledInterval = 1000 / REFRESHING_SCALE;
  const scaledTimeout = timeoutSeconds * REFRESHING_SCALE;
  const pauseableTimer$ = defer(() => {
    let passedValues = 0;
    return interval(scaledInterval).pipe(
      withLatestFrom(pause),
      filter(([v, paused]) => !paused),
      take(scaledTimeout),
      map(v => passedValues++),
    );
  }).pipe(share());

  return {
    stepTimer: pauseableTimer$,
    completeTimer: pauseableTimer$.pipe(reduce((x, y) => y)),
    remainingPercent: pauseableTimer$.pipe(map(v => ((scaledTimeout - (v + REFRESHING_SCALE)) / scaledTimeout) * 100)),
  };
}
