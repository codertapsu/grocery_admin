import { Observable } from 'rxjs';

export interface PauseableTimer {
  stepTimer: Observable<number>;
  completeTimer: Observable<number>;
  remainingPercent: Observable<number>;
}
