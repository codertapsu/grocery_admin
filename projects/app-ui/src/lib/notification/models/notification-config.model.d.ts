export interface NotificationConfig {
  className?: string;
  closeButton?: boolean;
  closeOnClick?: boolean;
  maxNotifications?: number;
  pauseOnHover?: boolean;
  position?: 'topRight' | 'topLeft' | 'topCenter' | 'bottomRight' | 'bottomLeft' | 'bottomCenter';
  timeoutSeconds?: number;
}
