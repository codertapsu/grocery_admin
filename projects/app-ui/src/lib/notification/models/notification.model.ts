import { BehaviorSubject } from 'rxjs';

import { NotificationType } from './notification-type.model';
import { PauseableTimer } from './pauseable-timer.model';

export class Notification {
  public id: number;
  public type: NotificationType;
  public title: string;
  public message: string;
  public timeoutSeconds: number;
  public paused: BehaviorSubject<boolean>;
  public pauseableTimer: PauseableTimer;

  public constructor(id: number, type: NotificationType, title: string, message: string, timeoutSeconds: number) {
    this.id = id;
    this.type = type;
    this.title = title;
    this.message = message;
    this.timeoutSeconds = timeoutSeconds;
    this.paused = new BehaviorSubject<boolean>(false);
  }
}
