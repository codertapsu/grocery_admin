import { InjectionToken } from '@angular/core';
import { NotificationConfig } from './models/notification-config.model';

export const NOTIFICATION_CONFIG = new InjectionToken<NotificationConfig>('NotificationConfig');
