import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, Injector, Optional, ViewContainerRef } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import { NOTIFICATION_CONFIG } from './injection.token';
import { NotificationConfig } from './models/notification-config.model';
import { NotificationType } from './models/notification-type.model';
import { Notification } from './models/notification.model';
import { NotificationComponent } from './notification.component';

@Injectable()
export class NotificationService {
  private readonly _notification$ = new Subject<Notification>();
  private readonly _removed$ = new Subject<number>();

  private _idx = 0;
  private _viewContainerRef: ViewContainerRef;

  set viewContainerRef(viewContainerRef: ViewContainerRef) {
    if (!this._viewContainerRef) {
      this._viewContainerRef = viewContainerRef;
      this.createContainerComponent();
    }
  }

  public constructor(
    private injector: Injector,
    @Inject(DOCUMENT) private document: Document,
    @Optional() @Inject(NOTIFICATION_CONFIG) private notificationConfig: NotificationConfig,
  ) {}

  get notification$(): Observable<Notification> {
    return this._notification$.asObservable();
  }

  get removed$(): Observable<number> {
    return this._removed$.asObservable();
  }

  public info(message: string, title = 'Info', timeoutSeconds?: number): number {
    const id = this._idx++;
    this._notification$.next(
      new Notification(
        id,
        NotificationType.Info,
        title,
        message,
        timeoutSeconds ?? this.notificationConfig.timeoutSeconds,
      ),
    );

    return id;
  }

  public success(message: string, title = 'Success', timeoutSeconds?: number): number {
    const id = this._idx++;
    this._notification$.next(
      new Notification(
        id,
        NotificationType.Success,
        title,
        message,
        timeoutSeconds ?? this.notificationConfig.timeoutSeconds,
      ),
    );

    return id;
  }

  public warning(message: string, title = 'Warning', timeoutSeconds?: number): number {
    const id = this._idx++;
    this._notification$.next(
      new Notification(
        id,
        NotificationType.Warning,
        title,
        message,
        timeoutSeconds ?? this.notificationConfig.timeoutSeconds,
      ),
    );

    return id;
  }

  public error(message: string, title = 'Error', timeoutSeconds?: number): number {
    const id = this._idx++;
    this._notification$.next(
      new Notification(
        id,
        NotificationType.Error,
        title,
        message,
        timeoutSeconds ?? this.notificationConfig.timeoutSeconds,
      ),
    );

    return id;
  }

  public remove(id: number) {
    this._removed$.next(id);
  }

  private createContainerComponent(): void {
    const containerComponentRef = this._viewContainerRef.createComponent(NotificationComponent, {
      injector: this.injector,
      // projectableNodes: [[this._viewContainerRef.element.nativeElement]],
    });
    containerComponentRef.changeDetectorRef.detectChanges();
    this.document.body.appendChild(containerComponentRef.location.nativeElement);
  }
}
