import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { NOTIFICATION_CONFIG } from './injection.token';
import { NotificationConfig } from './models/notification-config.model';
import { Notification } from './models/notification.model';
import { NotificationService } from './notification.service';
import { getPauseableTimer } from './utils';

@Component({
  selector: 'ui-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('flyInOut', [
      state('in', style({ transform: 'translateX(0)' })),
      transition('void => *', [style({ transform: 'translateX(100%)' }), animate('100ms ease-in-out')]),
      transition('* => void', [animate('200ms ease-in-out', style({ opacity: 0 }))]),
    ]),
  ],
})
export class NotificationComponent implements OnInit, OnDestroy {
  private _subscriptions = new Array<Subscription>();

  public readonly notifications: Notification[] = [];

  public constructor(
    private notificationService: NotificationService,
    private changeDetectorRef: ChangeDetectorRef,
    @Inject(NOTIFICATION_CONFIG) public notificationConfig: NotificationConfig,
  ) {}

  public ngOnInit(): void {
    const adding$ = this.notificationService.notification$.subscribe(notification => this.add(notification));
    const removing$ = this.notificationService.removed$.subscribe(id => this.remove(id));
    this._subscriptions.push(adding$, removing$);
  }

  public ngOnDestroy(): void {
    this._subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  public trackNotification(index: number, notification: Notification): number {
    return notification.id;
  }

  public pause(notification: Notification): void {
    if (this.notificationConfig.pauseOnHover) {
      notification.paused.next(true);
    }
  }

  public unPause(notification: Notification): void {
    if (this.notificationConfig.pauseOnHover) {
      notification.paused.next(false);
    }
  }

  public remove(id: number): void {
    const index = this.notifications.findIndex(notification => notification.id === id);
    if (index > -1) {
      this.notifications.splice(index, 1);
      this.changeDetectorRef.detectChanges();
    }
  }

  private add(notification: Notification): void {
    if (notification.timeoutSeconds !== 0) {
      notification.pauseableTimer = getPauseableTimer(notification.timeoutSeconds, notification.paused);
      notification.pauseableTimer.completeTimer.subscribe(() => this.remove(notification.id));
    }
    this.notifications.push(notification);
    if (this.notifications.length > this.notificationConfig.maxNotifications) {
      this.notifications.shift();
    }
    this.changeDetectorRef.detectChanges();
  }
}
