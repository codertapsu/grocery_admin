import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

import { NOTIFICATION_CONFIG } from './injection.token';
import { NotificationConfig } from './models/notification-config.model';
import { NotificationComponent } from './notification.component';
import { NotificationService } from './notification.service';

const DEFAULT_CONFIG: NotificationConfig = {
  className: 'notification',
  closeButton: true,
  closeOnClick: false,
  maxNotifications: 5,
  pauseOnHover: true,
  position: 'topRight',
  timeoutSeconds: 2.5,
};

@NgModule({
  declarations: [NotificationComponent],
  imports: [CommonModule, MatIconModule, MatTooltipModule, MatButtonModule],
  providers: [
    NotificationService,
    {
      provide: NOTIFICATION_CONFIG,
      useValue: DEFAULT_CONFIG,
    },
  ],
  bootstrap: [NotificationComponent],
})
export class NotificationModule {
  public static forRoot(config: NotificationConfig): ModuleWithProviders<NotificationModule> {
    return {
      ngModule: NotificationModule,
      providers: [
        {
          provide: NOTIFICATION_CONFIG,
          useValue: {
            className: config.className ?? DEFAULT_CONFIG.className,
            closeButton: config.closeButton ?? DEFAULT_CONFIG.closeButton,
            closeOnClick: config.closeOnClick ?? DEFAULT_CONFIG.closeOnClick,
            maxNotifications: config.maxNotifications ?? DEFAULT_CONFIG.maxNotifications,
            pauseOnHover: config.pauseOnHover ?? DEFAULT_CONFIG.pauseOnHover,
            position: config.position ?? DEFAULT_CONFIG.position,
            timeoutSeconds: config.timeoutSeconds ?? DEFAULT_CONFIG.timeoutSeconds,
          },
        },
      ],
    };
  }
}
