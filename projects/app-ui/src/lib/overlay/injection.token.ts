import { InjectionToken } from '@angular/core';

export const OVERLAY_DATA = new InjectionToken('OverlayData');
export const CONNECTED_TARGET = new InjectionToken('ConnectedTarget');
