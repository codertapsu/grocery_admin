import { ChangeDetectionStrategy, Component, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  template: `<ng-template #containerRef></ng-template>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverlayTemplateContainerComponent {
  @ViewChild('containerRef', { static: true, read: ViewContainerRef }) public containerRef: ViewContainerRef;
}
