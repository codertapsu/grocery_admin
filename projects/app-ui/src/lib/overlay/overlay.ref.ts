import { ComponentRef } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

import { fromEvent, merge, Observable, Subject } from 'rxjs';
import { filter, map, take, takeUntil } from 'rxjs/operators';

import { ClosedCause } from './models/closed-cause.model';
import { ClosedResult } from './models/closed-result.model';
import { OverlayConfig } from './models/overlay-config.model';
import { OverlayContainerComponent } from './overlay-container/overlay-container.component';

export class OverlayRef<R> {
  private readonly _afterClosed$ = new Subject<ClosedResult<R>>();
  private readonly _beforeClosed$ = new Subject<void>();
  private readonly _emitted$ = new Subject<R>();

  private _overlayComponentRef: ComponentRef<OverlayContainerComponent>;

  set overlayComponentRef(ref: ComponentRef<OverlayContainerComponent>) {
    this._overlayComponentRef = ref;

    const pressEscape$ = fromEvent<KeyboardEvent>(this.document, 'keyup').pipe(
      filter(event => event.key === 'Escape'),
      map<unknown, ClosedCause>(() => 'pressEscape'),
    );
    const navigation$ = this.router.events.pipe(
      filter(event => event instanceof NavigationStart),
      map<unknown, ClosedCause>(() => 'navigated'),
    );
    merge(pressEscape$, navigation$)
      .pipe(takeUntil(this._beforeClosed$))
      .subscribe(reason => this.destroyDialog(reason));

    if (!this.overlayConfig.disableClose) {
      const backgroundElement = this._overlayComponentRef.location.nativeElement as HTMLElement;
      fromEvent<MouseEvent>(backgroundElement, 'click')
        .pipe(
          filter(event => event.currentTarget === event.target),
          map<unknown, ClosedCause>(() => 'clickBackground'),
          takeUntil(this._beforeClosed$),
        )
        .subscribe(reason => this.destroyDialog(reason));
    }
  }

  get afterClosed$(): Observable<ClosedResult<R>> {
    return this._afterClosed$.asObservable();
  }

  get beforeClosed$(): Observable<void> {
    return this._beforeClosed$.asObservable();
  }

  get emitted$(): Observable<R> {
    return this._emitted$.asObservable();
  }

  public constructor(
    private overlayConfig: Partial<OverlayConfig>,
    private router: Router,
    private document: Document,
  ) {}

  public close(result?: R): void {
    this.destroyDialog('programmatically', result);
  }

  public emit(data: R): void {
    this._emitted$.next(data);
  }

  private destroyDialog(closedCause: ClosedCause, result?: R): void {
    this._overlayComponentRef.instance.animationStateChanged
      .pipe(
        filter(event => event.phaseName === 'start'),
        take(1),
      )
      .subscribe(() => {
        this._beforeClosed$.next();
        this._beforeClosed$.complete();
      });
    this._overlayComponentRef.instance.animationStateChanged
      .pipe(
        filter(event => event.phaseName === 'done' && event.toState === 'leave'),
        take(1),
      )
      .subscribe(() => {
        this._afterClosed$.next({ closedCause, result });
        this._afterClosed$.complete();
        this._overlayComponentRef.destroy();
      });
    this._overlayComponentRef.instance.startExitAnimation();
  }
}
