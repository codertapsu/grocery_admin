import { CommonModule } from '@angular/common';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { SvgParserModule } from '../svg-parser';

import { OverlayContainerComponent } from './overlay-container/overlay-container.component';
import { OverlayTemplateContainerComponent } from './overlay-template-container/overlay-template-container.component';
import { OverlayService } from './overlay.service';

@NgModule({
  declarations: [OverlayContainerComponent, OverlayTemplateContainerComponent],
  imports: [CommonModule, SvgParserModule],
  exports: [OverlayContainerComponent],
  providers: [OverlayService],
})
export class OverlayModule {
  public constructor(@Optional() @SkipSelf() parentModule: OverlayModule) {
    if (parentModule) {
      throw new Error('OverlayModule is already loaded. Import it in the AppModule only');
    }
  }
  // public static withSelector(selector: string): ModuleWithProviders<OverlayModule> {
  //   return {
  //     ngModule: OverlayModule,
  //     providers: [
  //       {
  //         provide: OVERLAY_SELECTOR,
  //         useValue: selector,
  //       },
  //     ],
  //   };
  // }
}
