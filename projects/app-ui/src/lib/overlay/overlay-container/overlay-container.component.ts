import { AnimationEvent } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  OnInit,
  Optional,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';

import { fromEvent, merge } from 'rxjs';
import { filter, startWith, takeUntil } from 'rxjs/operators';

import { ANIMATIONS, BrowserWindow, Coordinates, WINDOW } from '@common';

import { CONNECTED_TARGET } from '../injection.token';

@Component({
  selector: 'ui-overlay-container',
  templateUrl: './overlay-container.component.html',
  styleUrls: ['./overlay-container.component.scss'],
  animations: ANIMATIONS,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverlayContainerComponent implements OnInit {
  @ViewChild('contentRef', { static: true }) private contentRef: ElementRef<HTMLDivElement>;

  public animationState: 'void' | 'enter' | 'leave' = 'enter';

  @Output() public animationStateChanged = new EventEmitter<AnimationEvent>();

  public constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _elementRef: ElementRef<HTMLElement>,
    private _renderer: Renderer2,
    @Inject(WINDOW) private _window: Window,
    @Optional() @Inject(CONNECTED_TARGET) private _connectedTarget: ElementRef<HTMLElement> | HTMLElement | Coordinates,
  ) {}

  public ngOnInit(): void {
    this._renderer.addClass(this._elementRef.nativeElement, this._connectedTarget ? 'flexible' : 'global');

    if (this._connectedTarget) {
      this._renderer.setStyle(this.contentRef.nativeElement, 'position', 'absolute');

      const margin = 20;

      merge(fromEvent(this._window, 'resize'),
      // mutationObservable(this.contentRef.nativeElement),
      )
        .pipe(
          startWith(1),
          takeUntil(
            this.animationStateChanged
              .asObservable()
              .pipe(filter(animationEvent => animationEvent.toState === 'leave')),
          ),
        )
        .subscribe(() => {
          const { height: contentHeight, width: contentWidth } = this.contentRef.nativeElement.getBoundingClientRect();
          const {
            left: connectedTargetLeft,
            top: connectedTargetTop,
            height: connectedTargetHeight,
            width: connectedTargetWidth,
          } = this.calculateTargetProperties();

          const isBottomOverlapped =
            this._window.innerHeight <= connectedTargetTop + connectedTargetHeight + contentHeight + margin;
          if (isBottomOverlapped) {
            const posTop = connectedTargetTop - contentHeight / 2 + connectedTargetHeight / 2;
            if (this._window.innerHeight <= posTop + contentHeight + margin) {
              this._renderer.setStyle(this.contentRef.nativeElement, 'top', `${connectedTargetTop - contentHeight}px`);
            } else {
              this._renderer.setStyle(this.contentRef.nativeElement, 'top', `${posTop}px`);
            }
          } else {
            this._renderer.setStyle(
              this.contentRef.nativeElement,
              'top',
              `${connectedTargetTop + connectedTargetHeight}px`,
            );
          }

          const isRightOverlapped = this._window.innerWidth <= connectedTargetLeft + contentWidth + margin;
          if (isRightOverlapped) {
            this._renderer.setStyle(this.contentRef.nativeElement, 'left', `${connectedTargetLeft - contentWidth + connectedTargetWidth}px`);
          } else {
            this._renderer.setStyle(this.contentRef.nativeElement, 'left', `${connectedTargetLeft}px`);
          }
        });
    }
  }

  public onAnimationStart(event: AnimationEvent): void {
    this.animationStateChanged.emit(event);
  }

  public onAnimationDone(event: AnimationEvent): void {
    this.animationStateChanged.emit(event);
  }

  public startExitAnimation(): void {
    this.animationState = 'leave';
    this._changeDetectorRef.detectChanges();
  }

  private calculateTargetProperties(): {
    left: number;
    top: number;
    bottom: number;
    right: number;
    height: number;
    width: number;
  } {
    let top = 0;
    let right = 0;
    let bottom = 0;
    let left = 0;
    let height = 0;
    let width = 0;

    if (this._connectedTarget instanceof ElementRef) {
      const boundingClientRect = this._connectedTarget.nativeElement.getBoundingClientRect();
      top = boundingClientRect.top;
      right = boundingClientRect.right;
      bottom = boundingClientRect.bottom;
      left = boundingClientRect.left;
      height = boundingClientRect.height;
      width = boundingClientRect.width;
    } else if (this._connectedTarget instanceof HTMLElement) {
      const boundingClientRect = this._connectedTarget.getBoundingClientRect();
      top = boundingClientRect.top;
      right = boundingClientRect.right;
      bottom = boundingClientRect.bottom;
      left = boundingClientRect.left;
      height = boundingClientRect.height;
      width = boundingClientRect.width;
    } else {
      top = this._connectedTarget.y;
      left = this._connectedTarget.x;
      bottom = this._window.innerHeight - top;
      right = this._window.innerWidth - left;
    }

    return {
      left,
      top,
      bottom,
      right,
      height,
      width,
    };
  }
}
