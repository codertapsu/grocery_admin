import { NgModuleRef } from '@angular/core';

export interface OverlayConfig {
  position?: 'center' | 'top' | 'bottom' | 'left' | 'right';
  hasBackdrop?: boolean;
  disposeOnNavigation: boolean;
  backdropClass?: string;
  panelClass?: string;
  backdropClick?: boolean;
  data?: unknown;
  ngModuleRef?: NgModuleRef<unknown>;
  disableClose?: boolean;
}
