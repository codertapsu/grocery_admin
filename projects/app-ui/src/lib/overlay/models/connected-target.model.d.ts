import { ElementRef } from '@angular/core';
import { Coordinates } from '@common';

export type ConnectedTarget = ElementRef<HTMLElement> | HTMLElement | Coordinates;
