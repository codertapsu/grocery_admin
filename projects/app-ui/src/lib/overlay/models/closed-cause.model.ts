export type ClosedCause = 'pressEscape' | 'clickBackground' | 'programmatically' | 'navigated';
