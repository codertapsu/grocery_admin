import { ClosedCause } from './closed-cause.model';

export interface ClosedResult<R> {
  closedCause: ClosedCause;
  result: R;
}
