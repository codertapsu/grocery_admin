import { DOCUMENT } from '@angular/common';
import {
  ComponentRef,
  ElementRef,
  Inject,
  Injectable,
  Injector,
  TemplateRef,
  Type,
  ViewContainerRef,
} from '@angular/core';
import { Router } from '@angular/router';

import { Coordinates } from '@common';

import { CONNECTED_TARGET, OVERLAY_DATA } from './injection.token';
import { ConnectedTarget } from './models/connected-target.model';
import { OverlayConfig } from './models/overlay-config.model';
import { OverlayContainerComponent } from './overlay-container/overlay-container.component';
import { OverlayTemplateContainerComponent } from './overlay-template-container/overlay-template-container.component';
import { OverlayRef } from './overlay.ref';

@Injectable()
export class OverlayService {
  private readonly _defaultModalConfig: Partial<OverlayConfig> = {
    disableClose: false,
  };

  private _viewContainerRef: ViewContainerRef;

  set viewContainerRef(viewContainerRef: ViewContainerRef) {
    if (!this._viewContainerRef) {
      this._viewContainerRef = viewContainerRef;
    }
  }

  public constructor(
    private _router: Router,
    private _injector: Injector,
    @Inject(DOCUMENT) private _document: Document,
  ) {}

  public open<R, C = unknown>(
    component: Type<C> | TemplateRef<unknown>,
    config: Partial<OverlayConfig> = {},
  ): OverlayRef<R> {
    return this.buildOverlay<R>({ ...this._defaultModalConfig, ...config }, component);
  }

  public openPopover<R>(
    connectedTarget: ConnectedTarget,
    component: Type<unknown> | TemplateRef<unknown>,
    data?: unknown,
  ): OverlayRef<R> {
    return this.buildOverlay<R>({ ...this._defaultModalConfig, data }, component, connectedTarget);
  }

  private buildOverlay<R>(
    config: Partial<OverlayConfig>,
    component: Type<unknown> | TemplateRef<unknown>,
    connectedTarget?: ElementRef<HTMLElement> | HTMLElement | Coordinates,
  ): OverlayRef<R> {
    const overlayRef = this.createOverlayRef<R>(config);
    const contentComponentRef = this.createContentComponent(component, overlayRef, config);
    const containerComponentRef = this.createContainerComponent(contentComponentRef, overlayRef, connectedTarget);
    this.updateDom(containerComponentRef, contentComponentRef);

    return overlayRef;
  }

  private createOverlayRef<R>(config: Partial<OverlayConfig>) {
    return new OverlayRef<R>(config, this._router, this._document);
  }

  private createContentComponent<C, R = unknown>(
    component: Type<C> | TemplateRef<unknown>,
    overlayRef: OverlayRef<R>,
    config: Partial<OverlayConfig>,
  ): ComponentRef<unknown> {
    const injector = Injector.create({
      providers: [
        {
          provide: OverlayRef,
          useValue: overlayRef,
        },
        {
          provide: OVERLAY_DATA,
          useValue: config.data,
        },
      ],
      parent: this._injector,
    });
    if (component instanceof Type) {
      return this._viewContainerRef.createComponent(component, { injector, ngModuleRef: config.ngModuleRef });
    }

    const componentRef = this._viewContainerRef.createComponent(OverlayTemplateContainerComponent, { injector });
    componentRef.instance.containerRef.createEmbeddedView(component, {
      overlayRef,
      $implicit: config.data,
    });

    return componentRef;
  }

  private createContainerComponent<C, R>(
    contentComponentRef: ComponentRef<C>,
    overlayRef: OverlayRef<R>,
    connectedTarget?: ElementRef<HTMLElement> | HTMLElement | Coordinates,
  ): ComponentRef<OverlayContainerComponent> {
    const injector = Injector.create({
      providers: [
        {
          provide: CONNECTED_TARGET,
          useValue: connectedTarget,
        },
      ],
      parent: this._injector,
    });
    const containerComponentRef = this._viewContainerRef.createComponent(OverlayContainerComponent, {
      injector,
      projectableNodes: [[contentComponentRef.location.nativeElement]],
    });
    overlayRef.overlayComponentRef = containerComponentRef;
    containerComponentRef.changeDetectorRef.detectChanges();

    return containerComponentRef;
  }

  private updateDom(
    containerComponentRef: ComponentRef<OverlayContainerComponent>,
    _contentComponentRef: ComponentRef<unknown>,
  ): void {
    this._document.body.appendChild(containerComponentRef.location.nativeElement);

    // this.applicationRef.attachView(contentComponent.hostView);
    // this.applicationRef.attachView(modalComponent.hostView);

    (this._document.activeElement as HTMLElement).blur();
  }
}
