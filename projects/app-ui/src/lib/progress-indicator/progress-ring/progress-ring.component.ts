import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  Renderer2,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'ui-progress-ring',
  templateUrl: './progress-ring.component.svg',
  styleUrls: ['./progress-ring.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgressRingComponent implements OnChanges, OnInit {
  private _circumference = 0;
  private _circle: SVGCircleElement;

  @Input() public percent = 0;

  public constructor(private _renderer: Renderer2, private _elementRef: ElementRef<HTMLElement>) {}

  public ngOnChanges(changes: SimpleChanges): void {
    this.setProgress(Math.min(Math.abs(this.percent), 100));
  }

  public ngOnInit(): void {
    this._circle = this._elementRef.nativeElement.querySelector('circle');
    const radius = this._circle.r.baseVal.value;
    this._circumference = radius * 2 * Math.PI;
    this._renderer.setStyle(this._circle, 'stroke-dasharray', `${this._circumference} ${this._circumference}`);
    this._renderer.setStyle(this._circle, 'stroke-dashoffset', `${this._circumference}`);
    this.setProgress(Math.min(Math.abs(this.percent), 100));
  }

  private setProgress(percent: number): void {
    if (this._circle) {
      const offset = this._circumference - (percent / 100) * this._circumference;
      this._renderer.setStyle(this._circle, 'stroke-dashoffset', `${offset}`);
    }
  }
  private setSize(percent: number): void {
    const offset = this._circumference - (percent / 100) * this._circumference;
    this._renderer.setStyle(this._circle, 'stroke-dashoffset', `${offset}`);
  }
}
