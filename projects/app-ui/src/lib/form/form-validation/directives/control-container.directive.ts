import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[uiControlContainer]',
})
export class ControlContainerDirective {
  public constructor(public viewContainerRef: ViewContainerRef) {}
}
