import { ComponentRef, Directive, Inject, Input, OnInit, Optional, Self, ViewContainerRef } from '@angular/core';
import { NgControl } from '@angular/forms';

import { map, takeWhile } from 'rxjs/operators';

import { ErrorContainerComponent } from '../error-container/error-container.component';
import { FORM_ERRORS } from '../injection.token';
import { ControlContainerDirective } from './control-container.directive';

@Directive({
  selector: '[uiErrorCatcher]',
})
export class ErrorCatcherDirective implements OnInit {
  private readonly _container: ViewContainerRef;

  private _errorContainerComponentRef: ComponentRef<ErrorContainerComponent>;

  @Input() public customErrors: { [p: string]: string } = {};

  public constructor(
    private viewContainerRef: ViewContainerRef,
    @Inject(FORM_ERRORS) private errors: Record<string, (arg?: unknown) => string>,
    @Self() private ngControl: NgControl,
    @Optional() private controlErrorContainer: ControlContainerDirective,
  ) {
    if (!this.ngControl) {
      throw new Error('The directive [errorValid] must be integrated with formControl directive.');
    }
    this._container = this.controlErrorContainer ? this.controlErrorContainer.viewContainerRef : this.viewContainerRef;
  }

  public ngOnInit(): void {
    this.ngControl.valueChanges
      .pipe(
        takeWhile(() => !this.ngControl.pristine),
        map(() => this.ngControl.errors),
      )
      .subscribe(validationErrors => {
        if (validationErrors) {
          const firstKey = Object.keys(validationErrors)[0];
          const getError = this.errors[firstKey];
          const errMessage =
            this.customErrors[firstKey] || (getError ? getError(validationErrors[firstKey]) : 'Invalid value');
          this.setError(errMessage);
        } else if (this._errorContainerComponentRef) {
          this._errorContainerComponentRef.destroy();
          this._errorContainerComponentRef = undefined;
        }
      });
  }

  private setError(errMessage: string): void {
    if (!this._errorContainerComponentRef) {
      this._errorContainerComponentRef = this._container.createComponent(ErrorContainerComponent);
    }
    this._errorContainerComponentRef.instance.errMessage = errMessage;
  }
}
