import { NgModule } from '@angular/core';

import { ControlContainerDirective } from './directives/control-container.directive';
import { ErrorCatcherDirective } from './directives/error-catcher.directive';
import { ErrorContainerComponent } from './error-container/error-container.component';
import { FORM_ERRORS } from './injection.token';

export const DefaultErrors = {
  required: () => 'This field is required.',
  email: () => 'Invalid email address.',
  minlength: ({ requiredLength, actualLength }: { requiredLength: number; actualLength: number }) =>
    `Expect ${requiredLength} but got ${actualLength}.`,
  maxlength: ({ requiredLength, actualLength }: { requiredLength: number; actualLength: number }) =>
    `Limit: ${requiredLength} but got ${actualLength}.`,
};

@NgModule({
  declarations: [ErrorContainerComponent, ErrorCatcherDirective, ControlContainerDirective],
  exports: [ErrorCatcherDirective, ControlContainerDirective],
  providers: [
    {
      provide: FORM_ERRORS,
      useFactory: () => DefaultErrors,
    },
  ],
})
export class FormValidationModule {}
