import { InjectionToken } from '@angular/core';

export const FORM_ERRORS = new InjectionToken<Record<string, (arg?: unknown) => string>>('FormErrors');
