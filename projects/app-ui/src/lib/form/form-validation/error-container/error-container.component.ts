import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ui-error-container',
  templateUrl: './error-container.component.html',
  styleUrls: ['./error-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorContainerComponent {
  public text: string;

  @Input()
  set errMessage(value: string) {
    if (value !== this.text) {
      this.text = value;
    }
    this.changeDetectorRef.detectChanges();
  }

  public constructor(private changeDetectorRef: ChangeDetectorRef) {}
}
