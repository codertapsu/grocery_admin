import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { InputComponent } from './input/input.component';
import { OtpComponent } from './otp/otp.component';

@NgModule({
  declarations: [InputComponent, OtpComponent],
  imports: [CommonModule, FormsModule],
  exports: [InputComponent, OtpComponent],
})
export class InputModule {}
