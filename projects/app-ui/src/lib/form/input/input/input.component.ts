import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControl } from '@common';
import { InputType } from '../models/input-type.model';

@Component({
  selector: 'ui-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    },
  ],
})
export class InputComponent extends BaseControl<string> implements OnInit {
  private _type: InputType;

  @Input() public name = '';
  @Input() public placeholder = 'Insert value';
  @Input() public required = false;
  public hasEyeIcon = false;

  @Input()
  set type(value: InputType) {
    this._type = value;
    this.hasEyeIcon = value === 'password';
  }

  get type(): InputType {
    return this._type;
  }

  public constructor(protected _changeDetectorRef: ChangeDetectorRef) {
    super(_changeDetectorRef);
  }

  ngOnInit(): void {}

  public toggle(event: Event): void {
    (event.target as HTMLElement).classList.toggle('fa-eye');
    (event.target as HTMLElement).classList.toggle('fa-eye-slash');
    this._type = this._type === 'password' ? 'text' : 'password';
  }
}
