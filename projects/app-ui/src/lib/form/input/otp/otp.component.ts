import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  OnInit,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseControl } from '@common';

@Component({
  selector: 'ui-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => OtpComponent),
      multi: true,
    },
  ],
})
export class OtpComponent extends BaseControl<string> implements AfterViewInit {
  public constructor(protected _changeDetectorRef: ChangeDetectorRef, private _elementRef: ElementRef) {
    super(_changeDetectorRef);
  }

  public ngAfterViewInit(): void {
    // if (this.autofocus) {
    //   (this.elementRef.nativeElement as HTMLElement).querySelector('input').select();
    // }
  }

  public onPaste(event: ClipboardEvent): void {
    this.adjustInputValue(event.clipboardData.getData('text/plain'));
  }

  public onKeyUp(event: KeyboardEvent): void {
    const targetInput = event.target as HTMLInputElement;

    const previousElement = targetInput.previousElementSibling as HTMLInputElement;
    if (['ArrowLeft', 'Backspace', 'Delete'].includes(event.code) && previousElement) {
      previousElement.select();
    }

    const nextElement = targetInput.nextElementSibling as HTMLInputElement;
    if (
      (event.code.startsWith('Digit') || event.code.startsWith('Key') || event.code === 'ArrowRight') &&
      nextElement
    ) {
      nextElement.select();
    }

    const inputs = (this._elementRef.nativeElement as HTMLElement).querySelectorAll('input');
    this.updateValue(Array.from(inputs).reduce((value, input) => `${value}${input.value}`, ''));
  }

  public override writeValue(value: string): void {
    this.adjustInputValue(value);
    super.writeValue(value);
  }

  private adjustInputValue(value: string): void {
    const inputs = (this._elementRef.nativeElement as HTMLElement).querySelectorAll('input');
    if (value.length === 6) {
      inputs.forEach((input, index) => (input.value = value[index]));
    }
  }
}
