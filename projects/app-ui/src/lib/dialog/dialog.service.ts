import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Observable } from 'rxjs';

import { PromptComponent } from './components/prompt/prompt.component';
import { PromptData } from './models/prompt-data.model';

@Injectable()
export class DialogService {
  public constructor(public dialog: MatDialog) {}

  public prompt(title: string = '', defaultValue: string = ''): Observable<string> {
    const dialogRef = this.dialog.open<PromptComponent, PromptData, string>(PromptComponent, {
      width: '250px',
      data: { title, value: defaultValue },
    });

    return dialogRef.afterClosed();
  }
}
