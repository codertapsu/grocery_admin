import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from '../material';
import { PromptComponent } from './components/prompt/prompt.component';
import { DialogService } from './dialog.service';

@NgModule({
  declarations: [PromptComponent],
  imports: [CommonModule, FormsModule, MaterialModule],
  providers: [DialogService],
})
export class DialogModule {}
