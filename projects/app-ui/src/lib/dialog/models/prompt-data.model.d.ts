export interface PromptData {
  title: string;
  value: string;
}
