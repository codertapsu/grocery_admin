import { ConnectedPosition, Overlay, OverlayPositionBuilder, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { DOCUMENT } from '@angular/common';
import { Directive, ElementRef, Inject, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

import { fromEvent, merge, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { BrowserWindow, WINDOW } from '@common';

import { TooltipComponent } from './tooltip.component';

@Directive({
  selector: '[uiTooltip]',
})
export class TooltipDirective implements OnInit {
  private readonly _positions: ConnectedPosition[] = [
    /* Quadrant I */
    {
      originX: 'end',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'bottom',
      offsetX: 0,
      offsetY: 0,
    },
    /* Quadrant II */
    {
      originX: 'start',
      originY: 'top',
      overlayX: 'end',
      overlayY: 'bottom',
      offsetX: 0,
      offsetY: 0,
    },
    /* Quadrant III */
    {
      originX: 'start',
      originY: 'bottom',
      overlayX: 'end',
      overlayY: 'top',
      offsetX: 0,
      offsetY: 0,
    },
    /* Quadrant IV */
    {
      originX: 'end',
      originY: 'bottom',
      overlayX: 'start',
      overlayY: 'top',
      offsetX: 0,
      offsetY: 0,
    },
    /* Bottom Center */
    {
      originX: 'center',
      originY: 'bottom',
      overlayX: 'center',
      overlayY: 'top',
      offsetX: 0,
      offsetY: 6,
    },
    /* Top Center */
    {
      originX: 'center',
      originY: 'top',
      overlayX: 'center',
      overlayY: 'bottom',
      offsetX: 0,
      offsetY: -6,
    },
  ];

  private _overlayRef: OverlayRef;
  private _timeoutId: number;
  private _subscriptions = new Array<Subscription>();

  @Input() public uiTooltip: string | TemplateRef<unknown> = '';
  @Input() public scrollable = false;

  public constructor(
    private overlay: Overlay,
    private overlayPositionBuilder: OverlayPositionBuilder,
    private elementRef: ElementRef<HTMLElement>,
    private viewContainerRef: ViewContainerRef,
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window: BrowserWindow,
  ) {}

  public ngOnInit(): void {
    const scroll$ = merge(fromEvent(this.window, 'scroll'), fromEvent(this.window, 'resize'))
      .pipe(filter(() => this._overlayRef.hasAttached()))
      .subscribe(() => {
        if (this.scrollable) {
          if (!this.isHostInViewport()) {
            this.hide();
          } else {
            this._overlayRef.updatePosition();
          }
        } else {
          this.hide();
        }
      });
    const shown$ = merge(
      // fromEvent<TouchEvent>(this.elementRef.nativeElement, 'touchstart'),
      fromEvent<PointerEvent>(this.elementRef.nativeElement, 'click').pipe(
        map(event => ({
          x: event.clientX,
          y: event.clientY,
        })),
      ),
      fromEvent<MouseEvent>(this.elementRef.nativeElement, 'mouseenter').pipe(
        map(event => ({
          x: event.clientX,
          y: event.clientY,
        })),
      ),
    ).subscribe(connectionTarget => {
      const positionStrategy = this.overlayPositionBuilder
        .flexibleConnectedTo({...connectionTarget, height: this.elementRef.nativeElement.clientHeight, width: this.elementRef.nativeElement.clientWidth})
        .withPositions(this._positions)
        .withFlexibleDimensions(true)
        .withPush(true);
      if (this._overlayRef) {
        this._overlayRef.dispose();
      }
      this._overlayRef = this.overlay.create({ positionStrategy });
      const componentPortal = new ComponentPortal(TooltipComponent);
      const componentRef = this._overlayRef.attach(componentPortal);
      if (this.uiTooltip instanceof TemplateRef) {
        componentPortal.viewContainerRef.createEmbeddedView(this.uiTooltip);
      } else {
        componentRef.instance.text = this.uiTooltip;
      }
      this._timeoutId = this.document.defaultView.setTimeout(() => {
        let innerWidth = this._overlayRef.overlayElement.clientWidth;
        const viewportWidth = this.document.defaultView.innerWidth;
        innerWidth += viewportWidth * 0.1;
        if (innerWidth >= viewportWidth) {
          this._overlayRef.addPanelClass('tooltip-overlay-panel-full-width');
        } else {
          this._overlayRef.removePanelClass('tooltip-overlay-panel-full-width');
        }
      });
      // this.show();
    });
    const targetClicked$ = fromEvent<PointerEvent>(this.window, 'click').pipe(
      map(event => event.target as HTMLElement),
    );
    const targetTouched$ = fromEvent<TouchEvent>(this.window, 'touchstart').pipe(
      filter(event => !!event.touches.length),
      map(event => event.touches.item(0).target as HTMLElement),
    );
    const hidden$ = merge(
      merge(targetClicked$, targetTouched$).pipe(filter(target => !this.elementRef.nativeElement.contains(target))),
      fromEvent(this.elementRef.nativeElement, 'mouseleave'),
    )
      .pipe(filter(() => this._overlayRef.hasAttached()))
      .subscribe(() => this.hide());

    this._subscriptions.push(scroll$, shown$, hidden$);
  }

  public ngOnDestroy(): void {
    this._overlayRef.dispose();
    this._subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private isHostInViewport(): boolean {
    const bounding = this.elementRef.nativeElement.getBoundingClientRect();
    return (
      bounding.top >= 0 &&
      bounding.left >= 0 &&
      bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  private show() {
    if (!this._overlayRef.hasAttached()) {
      const componentPortal = new ComponentPortal(TooltipComponent);
      const componentRef = this._overlayRef.attach(componentPortal);
      if (this.uiTooltip instanceof TemplateRef) {
        componentPortal.viewContainerRef.createEmbeddedView(this.uiTooltip);
      } else {
        componentRef.instance.text = this.uiTooltip;
      }
      this._timeoutId = this.document.defaultView.setTimeout(() => {
        let innerWidth = this._overlayRef.overlayElement.clientWidth;
        const viewportWidth = this.document.defaultView.innerWidth;
        innerWidth += viewportWidth * 0.1;
        if (innerWidth >= viewportWidth) {
          this._overlayRef.addPanelClass('tooltip-overlay-panel-full-width');
        } else {
          this._overlayRef.removePanelClass('tooltip-overlay-panel-full-width');
        }
      });
    }
  }

  private hide() {
    if (this._overlayRef.hasAttached()) {
      this._overlayRef.detach();
    }
    if (this._timeoutId) {
      this.document.defaultView.clearTimeout(this._timeoutId);
    }
  }
}
