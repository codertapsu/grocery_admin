import { HttpClient, HttpErrorResponse, HttpEventType, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { FileQueue, FileQueueStatus } from './models/file-queue.model';

@Injectable()
export class MediaUploaderService {
  public url: string = 'https://jsonplaceholder.typicode.com/posts';

  private readonly _fileQueues: FileQueue[] = [];
  private readonly _fileQueues$ = new BehaviorSubject<FileQueue[]>(this._fileQueues);

  constructor(private http: HttpClient) {}

  // the queue
  public get fileQueues$() {
    return this._fileQueues$.asObservable();
  }

  // public events
  public onCompleteItem(queueObj: FileQueue, response: any): any {
    return { queueObj, response };
  }

  // public functions
  public addToQueue(files: File[]) {
    files.forEach(file => this._addToQueue(file));
  }

  public clearQueue() {
    // clear the queue
    this._fileQueues.length = 0;
    this._fileQueues$.next(this._fileQueues);
  }

  public uploadAll() {
    // upload all except already successful or in progress
    this._fileQueues.forEach(queueObj => {
      if (queueObj.isUploadable()) {
        this._upload(queueObj);
      }
    });
  }

  // private functions
  private _addToQueue(file: File) {
    const queueObj = new FileQueue(file);

    // set the individual object events
    queueObj.upload = () => this._upload(queueObj);
    queueObj.remove = () => this._removeFromQueue(queueObj);
    queueObj.cancel = () => this._cancel(queueObj);

    // push to the queue
    this._fileQueues.push(queueObj);
    this._fileQueues$.next(this._fileQueues);
  }

  private _removeFromQueue(queueObj: FileQueue) {
    // _.remove(this._files, queueObj);
  }

  private _upload(queueObj: FileQueue) {
    // create form data for file
    const form = new FormData();
    form.append('file', queueObj.file, queueObj.file.name);

    // upload file and report progress
    const req = new HttpRequest('POST', this.url, form, {
      reportProgress: true,
    });

    // upload file and report progress
    queueObj.request = this.http.request(req).subscribe(
      (event: any) => {
        if (event.type === HttpEventType.UploadProgress) {
          this._uploadProgress(queueObj, event);
        } else if (event instanceof HttpResponse) {
          this._uploadComplete(queueObj, event);
        }
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          // A client-side or network error occurred. Handle it accordingly.
          this._uploadFailed(queueObj, err);
        } else {
          // The backend returned an unsuccessful response code.
          this._uploadFailed(queueObj, err);
        }
      },
    );

    return queueObj;
  }

  private _cancel(queueObj: FileQueue) {
    queueObj.request.unsubscribe();
    queueObj.progress = 0;
    queueObj.status = FileQueueStatus.Pending;
    this._fileQueues$.next(this._fileQueues);
  }

  private _uploadProgress(queueObj: FileQueue, event: any) {
    const progress = Math.round((100 * event.loaded) / event.total);
    queueObj.progress = progress;
    queueObj.status = FileQueueStatus.Progress;
    this._fileQueues$.next(this._fileQueues);
  }

  private _uploadComplete(queueObj: FileQueue, response: HttpResponse<any>) {
    queueObj.progress = 100;
    queueObj.status = FileQueueStatus.Success;
    queueObj.response = response;
    this._fileQueues$.next(this._fileQueues);
    this.onCompleteItem(queueObj, response.body);
  }

  private _uploadFailed(queueObj: FileQueue, response: HttpErrorResponse) {
    queueObj.progress = 0;
    queueObj.status = FileQueueStatus.Error;
    queueObj.response = response;
    this._fileQueues$.next(this._fileQueues);
  }
}
