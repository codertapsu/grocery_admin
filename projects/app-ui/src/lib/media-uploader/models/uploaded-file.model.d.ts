import { Media } from '@core/models/media.model';

export type UploadedFile = Media;
