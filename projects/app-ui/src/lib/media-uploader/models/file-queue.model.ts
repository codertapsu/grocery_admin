import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';

export enum FileQueueStatus {
  Pending,
  Success,
  Error,
  Progress,
}

export class FileQueue {
  public file: File;
  public status: FileQueueStatus = FileQueueStatus.Pending;
  public progress: number = 0;
  public request: Subscription = null;
  public response: HttpResponse<any> | HttpErrorResponse = null;

  public constructor(file: File) {
    this.file = file;
  }

  // actions
  public upload = () => {
    /* set in service */
  };
  public cancel = () => {
    /* set in service */
  };
  public remove = () => {
    /* set in service */
  };

  // statuses
  public isPending = () => this.status === FileQueueStatus.Pending;
  public isSuccess = () => this.status === FileQueueStatus.Success;
  public isError = () => this.status === FileQueueStatus.Error;
  public inProgress = () => this.status === FileQueueStatus.Progress;
  public isUploadable = () => this.status === FileQueueStatus.Pending || this.status === FileQueueStatus.Error;
}
