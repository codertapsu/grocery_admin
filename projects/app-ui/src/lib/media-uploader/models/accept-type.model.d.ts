export type AcceptType = 'image/*' | 'video/*' | 'audio/*' | 'application/*';
