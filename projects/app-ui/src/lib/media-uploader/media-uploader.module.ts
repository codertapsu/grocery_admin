import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { MediaUploaderComponent } from './media-uploader.component';
import { MediaUploaderService } from './media-uploader.service';
import { BlobSrcDirective } from './directives/blob-src.directive';
import { SwitchCaseDirective } from './directives/switch-case.directive';
import { MatchDirective } from './directives/match.directive';

@NgModule({
  declarations: [MediaUploaderComponent, BlobSrcDirective, SwitchCaseDirective, MatchDirective],
  imports: [CommonModule, MatIconModule, MatProgressBarModule],
  exports: [MediaUploaderComponent],
})
export class MediaUploaderModule {}
