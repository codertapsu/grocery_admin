import { Directive, ElementRef, Inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { BrowserWindow, WINDOW } from '@common';

@Directive({
  selector: 'img[blob],source[blob]',
})
export class BlobSrcDirective implements OnChanges, OnInit, OnDestroy {
  @Input() public blob: Blob | MediaSource;
  // @Input() public retryTimes = 5;
  // @Input() public delay = 2000; // milliseconds

  private _timerId: number;
  public constructor(private elementRef: ElementRef<HTMLImageElement>, @Inject(WINDOW) private window: BrowserWindow) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes['blob']?.currentValue) {
      this.window.URL.revokeObjectURL(this.elementRef.nativeElement.src);
      this.elementRef.nativeElement.src = this.window.URL.createObjectURL(this.blob);
    }
  }

  public ngOnInit(): void {
    // let counter = this.retryTimes;
    // this.elementRef.nativeElement.onerror = () => {
    //   if (counter) {
    //     this._timerId = this.document.defaultView.setTimeout(() => {
    //       counter--;
    //       const source = this.elementRef.nativeElement.src;
    //       this.elementRef.nativeElement.src = source;
    //     }, this.delay);
    //   } else {
    //     this.elementRef.nativeElement.src = DEFAULT_IMAGE_PATH;
    //   }
    // };
  }

  public ngOnDestroy(): void {
    this.window.URL.revokeObjectURL(this.elementRef.nativeElement.src);
    // this.document.defaultView.clearTimeout(this._timerId);
  }
}
