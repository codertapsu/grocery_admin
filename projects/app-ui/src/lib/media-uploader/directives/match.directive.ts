import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[ngMatch]',
})
export class MatchDirective {
  @Input()
  set ngMatch(show: boolean) {
    show ? this.viewContainerRef.createEmbeddedView(this.templateRef) : this.viewContainerRef.clear();
  }

  public constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {}
}
