import { NgSwitch } from '@angular/common';
import { Directive, DoCheck, Host, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[uiSwitchCase]',
})
export class SwitchCaseDirective implements OnInit, DoCheck {
  private ngSwitch: any;
  private _created = false;

  @Input() public uiSwitchCase: any[];

  constructor(
    private viewContainer: ViewContainerRef,
    private templateRef: TemplateRef<Object>,
    @Host() ngSwitch: NgSwitch,
  ) {
    this.ngSwitch = ngSwitch;
  }

  public ngOnInit(): void {
    (this.uiSwitchCase || []).forEach(() => this.ngSwitch._addCase());
  }

  public ngDoCheck(): void {
    let enforce = false;
    (this.uiSwitchCase || []).forEach(value => (enforce = this.ngSwitch._matchCase(value) || enforce));
    this.enforceState(enforce);
  }

  private enforceState(created: boolean): void {
    if (created && !this._created) {
      this._created = true;
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else if (!created && this._created) {
      this._created = false;
      this.viewContainer.clear();
    }
  }
}
