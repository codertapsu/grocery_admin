import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { Observable } from 'rxjs';

import { API_END_POINT, BaseControl } from '@common';

import { MediaUploaderService } from './media-uploader.service';
import { FileQueue } from './models/file-queue.model';
import { UploadedFile } from './models/uploaded-file.model';

@Component({
  selector: 'ui-media-uploader',
  templateUrl: './media-uploader.component.html',
  styleUrls: ['./media-uploader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MediaUploaderComponent),
      multi: true,
    },
    MediaUploaderService,
  ],
})
export class MediaUploaderComponent extends BaseControl<UploadedFile[]> implements OnInit {
  private _multiple: boolean;
  private _acceptableExt: string;

  @Input() set multiple(value: boolean) {
    this._multiple = value !== null && `${value}` !== 'false';
  }

  get multiple(): boolean {
    return this._multiple;
  }

  @Input() set acceptableExt(value: string) {
    this._acceptableExt = value;
  }

  get acceptableExt(): string {
    return this._acceptableExt || 'image/*,video/*';
  }

  @Output() public uploadCompleted = new EventEmitter();

  public dragging: boolean = false;
  public fileQueues$: Observable<FileQueue[]>;

  public static ngAcceptInputType_multiple: boolean | '';

  public constructor(
    private mediaUploaderService: MediaUploaderService,
    @Inject(API_END_POINT) public apiEndpoint: string,
    protected override changeDetectorRef: ChangeDetectorRef,
  ) {
    super(changeDetectorRef);
  }

  public ngOnInit(): void {
    this.fileQueues$ = this.mediaUploaderService.fileQueues$;

    const completeItem = (item: FileQueue, response: any) => {
      this.uploadCompleted.emit({ item, response });
    };

    this.mediaUploaderService.onCompleteItem = completeItem;
  }

  public handleDragover(): boolean {
    return false;
  }

  public handleDragEnter(): void {
    this.dragging = true;
  }

  public handleDragLeave(): void {
    this.dragging = false;
  }

  public handleDrop(event: DragEvent): void {
    event.preventDefault();
    this.dragging = false;
    this.markAsTouched();
    this.handleUploadedFiles(event.dataTransfer.files);
  }

  public handleInputChange(event: Event): void {
    const inputElement = event.target as HTMLInputElement;
    const files = inputElement.files;
    this.markAsTouched();
    this.handleUploadedFiles(files);
  }

  public remove(index: number): void {
    this.updateValue(this.value.filter((_, i) => i !== index));
  }

  private handleUploadedFiles(files: FileList) {
    if (!this.readonly) {
      const uploadedFiles: UploadedFile[] = [];
      const patterns = this.acceptableExt.split(',').map(value => new RegExp(value));
      for (let index = 0, length = files.length; index < length; index++) {
        const file = files.item(index);
        console.log(file.type);

        if (patterns.some(pattern => pattern.test(file.type))) {
          uploadedFiles.push({
            file,
            name: file.name,
            size: Number((file.size / 1024 / 1024).toFixed(2)),
            path: '',
            mimetype: file.type,
          });
          if (index === length - 1) {
            this.updateValue([...(this.value || []), ...uploadedFiles]);
            this.changeDetectorRef.detectChanges();
          }
        }
      }
    }
  }

  private addToQueue(files: File[]) {
    this.mediaUploaderService.addToQueue(files);
  }
}
