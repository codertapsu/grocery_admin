import './polyfills';

import { DOCUMENT } from '@angular/common';
import { enableProdMode, LOCALE_ID } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { API_END_POINT, ConfigProvider, LOCAL_STORAGE, THEME_PREFERENCE_KEY } from '@common';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

const defaultConfig: ConfigProvider = {
  apiEndpoint: '',
  themePreferenceKey: 'theme-preference',
};

fetch('./assets/configuration.json')
  .then<ConfigProvider, ConfigProvider>(
    response => {
      if (!response.ok) {
        return Promise.resolve(defaultConfig);
      }

      return response.json();
    },
    () => Promise.resolve(defaultConfig),
  )
  .then(configuration =>
    platformBrowserDynamic([
      {
        provide: API_END_POINT,
        useValue: configuration.apiEndpoint,
        multi: false,
      },
      {
        provide: THEME_PREFERENCE_KEY,
        useValue: configuration.themePreferenceKey,
        multi: false,
      },
    ]).bootstrapModule(AppModule, {
      providers: [
        {
          provide: LOCALE_ID,
          useValue: 'en-US',
        },
      ],
    }),
  )
  .then(ref => {
    const document = ref.injector.get(DOCUMENT);
    const localStorage = ref.injector.get(LOCAL_STORAGE);
    const themePreferenceKey = ref.injector.get(THEME_PREFERENCE_KEY);
    // Ensure Angular destroys itself on hot reloads.
    if ((document.defaultView as any)['ngRef']) {
      (document.defaultView as any)['ngRef'].destroy();
    }
    (document.defaultView as any)['ngRef'] = ref;

    const themeClassName =
      localStorage.getItem(themePreferenceKey) ||
      (document.defaultView.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light');
    console.log(themeClassName);

    if (themeClassName === 'dark') {
      document.body.classList.add('dark');
    }
  })
  .catch(err => console.error(err));
