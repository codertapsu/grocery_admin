'use strict';
class ExtendArray extends Array {
  remove(...a) {
    let L = a.length;
    let what;
    let ax;
    while (L && this.length) {
      what = a[--L];
      while ((ax = this.indexOf(what)) !== -1) {
        this.splice(ax, 1);
      }
    }
    return this;
  }
}
