import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

import { noop } from 'rxjs';

@Component({
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotFoundComponent {
  public constructor(private location: Location, private router: Router) {}

  public goBack(): void {
    const { navigationId } = this.location.getState() as { navigationId: number };
    if (navigationId > 1) {
      this.location.back();
    } else {
      this.router.navigate(['/']).then(noop);
    }
  }
}
