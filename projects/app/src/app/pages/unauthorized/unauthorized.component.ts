import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

import { noop } from 'rxjs';

@Component({
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnauthorizedComponent {
  public constructor(private location: Location, private router: Router) {}

  public goBack(): void {
    const { navigationId } = this.location.getState() as { navigationId: number };
    if (navigationId > 1) {
      this.location.back();
    } else {
      this.router.navigate(['/']).then(noop);
    }
  }
}
