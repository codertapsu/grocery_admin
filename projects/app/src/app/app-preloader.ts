import { Inject, Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { NAVIGATOR } from '@common';

import { delayedExecutor } from 'projects/app/src/app/utils';
import { Observable, of } from 'rxjs';

// http://wicg.github.io/netinfo/#effectiveconnectiontype-enum
type EffectiveConnectionType = '2g' | '3g' | '4g' | 'slow-2g';

type NetworkInformationAlias = NetworkInformation & {
  // http://wicg.github.io/netinfo/#effectivetype-attribute
  readonly effectiveType?: EffectiveConnectionType;
  // http://wicg.github.io/netinfo/#downlink-attribute
  readonly downlink?: number;
  // http://wicg.github.io/netinfo/#rtt-attribute
  readonly rtt?: number;
  // http://wicg.github.io/netinfo/#savedata-attribute
  readonly saveData?: boolean;
  // http://wicg.github.io/netinfo/#handling-changes-to-the-underlying-connection
  onchange?: EventListener;
};

@Injectable()
export class AppCustomPreloader implements PreloadingStrategy {
  public constructor(@Inject(NAVIGATOR) private navigator: Navigator) {}

  public preload(route: Route, fn: () => Observable<unknown>): Observable<unknown> {
    return this.loadWithDelay(route, fn);
  }

  private loadWithDelay(route: Route, fn: () => Observable<unknown>): Observable<unknown> {
    if (route.data && route.data['preload']) {
      const delay = route.data['loadAfterSeconds'] ? route.data['loadAfterSeconds'] : 0;
      return delayedExecutor(delay, fn);
    }
    return of(null);
  }

  /**
   * Currently it is not working on Firefox.
   */
  private loadBasedOnNetwork(route: Route, fn: () => Observable<unknown>): Observable<unknown> {
    const connection = this.navigator.connection as NetworkInformationAlias;
    if (connection.saveData) {
      return of(null);
    }
    const speed = connection.effectiveType;
    const slowConnections = ['slow-2g', '2g'];
    if (slowConnections.includes(speed)) {
      return of(null);
    }
    return fn();
  }
}
