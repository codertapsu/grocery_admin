import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

import { TypedFormGroup } from '@common';
import { CategoriesService } from '@modules/category/category.service';
import { Category } from '@modules/category/models/category.model';
import { OVERLAY_DATA, OverlayRef } from '@ui';

@Component({
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
  public form: TypedFormGroup<Category>;

  public constructor(
    private _categoryService: CategoriesService,
    private _overlayRef: OverlayRef<Category>,
    @Inject(OVERLAY_DATA) private _initialData: Category,
  ) {}

  public ngOnInit(): void {
    this.form = this.buildForm(this._initialData);
  }

  public submit(): void {
    this.form.markAsSubmitted();
    if (this.form.valid) {
      this._categoryService.create(this.form.getRawValue()).subscribe(() => {
        this._overlayRef.close();
      });
    }
  }

  private buildForm(category: Category): TypedFormGroup<Category> {
    return new TypedFormGroup<Category>({
      id: new UntypedFormControl(category.id),
      name: new UntypedFormControl(category.name),
      medias: new UntypedFormControl(category.medias || []),
      isFeature: new UntypedFormControl(category.isFeature),
    });
  }
}
