import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  createNgModuleRef,
  Injector,
  ViewContainerRef,
} from '@angular/core';

import { BehaviorSubject, combineLatest, from, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, startWith, switchMap } from 'rxjs/operators';

import { SortDirection } from '@core/constants/sort-direction.constant';
import { PaginationData } from '@core/models/pagination-data.model';
import { PaginationTable } from '@shared/abstractions';
import { NotificationService, OverlayService } from '@ui';

import { Order } from '../../models/order.model';
import { OrderService } from '../../order.service';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<Order> {
  public readonly search$ = new Subject<string>();
  public readonly sort$ = new BehaviorSubject<{
    by: string;
    direction: SortDirection;
  }>({
    by: 'createdAt',
    direction: SortDirection.DESC,
  });

  public constructor(
    private _overlayService: OverlayService,
    private _viewContainerRef: ViewContainerRef,
    private _injector: Injector,
    private _notificationService: NotificationService,
    private _orderService: OrderService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public createOrUpdate(data: Order = {} as Order): void {
    import('../form')
      .then(({ FormComponent, FormModule }) => {
        const ngModuleRef = createNgModuleRef(
          FormModule,
          Injector.create({
            parent: this._injector,
            providers: [
              {
                provide: OrderService,
                useValue: this._orderService,
              },
            ],
          }),
        );
        this._overlayService
          .open<Order>(FormComponent, {
            data,
            ngModuleRef,
            disableClose: true,
          })
          .afterClosed$.pipe(filter(closedResult => !!closedResult.result))
          .subscribe({
            next: () => {
              this.refresh();
              this._notificationService.success('Product created successfully');
            },
            error: () => {
              this._notificationService.error('Error creating product');
            },
          });
      })
      .catch(error => {
        this._notificationService.error(error.message || 'Error creating product');
      });
  }

  public edit(data: Order): void {
    from(import('../form'))
      .pipe(
        switchMap(
          ({ FormModule, FormComponent }) =>
            this._overlayService.open<Order['id']>(FormComponent, { data, disableClose: true }).afterClosed$,
        ),
        map(closed => closed.result),
        filter(Boolean),
      )
      .subscribe({
        next: productId => {
          this.refresh();
        },
        error: error => {
          console.log(error);
        },
        complete: () => {},
      });
  }

  public remove(item: Order): void {
    this._orderService.remove(item).subscribe(() => {
      this.refresh();
    });
  }

  protected getSource(): Observable<PaginationData<Order>> {
    return combineLatest([
      this.pagination$,
      this.search$.pipe(debounceTime(800), distinctUntilChanged(), startWith('')),
      this.sort$,
    ]).pipe(
      switchMap(([pagination, search, sort]) =>
        this._orderService.getFilteredPaginationData(
          pagination.index,
          pagination.size,
          search,
          sort.by,
          sort.direction,
        ),
      ),
    );
  }
}
