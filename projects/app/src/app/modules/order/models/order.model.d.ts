import { Order as OrderBase } from '@core/models/order.model';

export interface Order extends OrderBase {}
