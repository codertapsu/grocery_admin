import { NgModule } from '@angular/core';

import { OrderRoutingModule } from './order-routing.module';
import {OrderService} from './order.service';

@NgModule({
  declarations: [],
  imports: [OrderRoutingModule],
  providers: [OrderService]
})
export class OrderModule {}
