import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/list/list.module').then(m => m.ListModule),
  },
  {
    path: 'order-status',
    loadChildren: () => import('../order-status/order-status.module').then(m => m.OrderStatusModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderRoutingModule {}
