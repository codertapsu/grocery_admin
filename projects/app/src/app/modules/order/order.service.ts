import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Customer } from '@core/models/customer.model';
import { OrderStatus } from '@core/models/order-status.model';
import { PaymentStatus } from '@core/models/payment-status.model';
import { CustomersService } from '@core/services/customers.service';
import { OrdersService as OrderServiceBase } from '@core/services/orders.service';
import { PaymentsService } from '@core/services/payments.service';
import { SortDirection } from '@core/constants/sort-direction.constant';
import { Order } from './models/order.model';
import { PaginationData } from '@core/models/pagination-data.model';

@Injectable()
export class OrderService {
  public constructor(
    private _paymentService: PaymentsService,
    private _customerService: CustomersService,
    private _orderServiceBase: OrderServiceBase
  ) {}

  public getCustomerSelections(): Observable<Customer[]> {
    return this._customerService.getSelections();
  }

  public getOrderStatuses(): Observable<OrderStatus[]> {
    return this._orderServiceBase.getOrderStatuses();
  }

  public getById(id: number): Observable<OrderStatus> {
    return this._orderServiceBase.getById(id);
  }

  public getPaymentStatuses(): Observable<PaymentStatus[]> {
    return this._paymentService.getPaymentStatuses();
  }
  

  public getAll(): Observable<Order[]> {
    return this._orderServiceBase.getAll();
  }

  public getFilteredPaginationData(
    page: number,
    take: number,
    search: string,
    sortBy: string,
    sortDirection: SortDirection,
  ): Observable<PaginationData<Order>> {
    return this._orderServiceBase.getFilteredPaginationData({
      page,
      take,
      search,
      sortBy,
      sortDirection,
    });
  }

  public create(order: Order): Observable<Order> {
    const formData = new FormData();
    return this._orderServiceBase.createFormData(formData);
  }

  public update(order: Order): Observable<Order> {
    const formData = new FormData();
    return this._orderServiceBase.updateFormData(order.id, formData);
  }

  public remove(order: Order): Observable<unknown> {
    return this._orderServiceBase.remove(order.id);
  }
}
