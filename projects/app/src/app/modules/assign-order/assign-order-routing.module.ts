import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AssignOrderComponent } from './assign-order.component';

const routes: Routes = [
  {
    path: '',
    component: AssignOrderComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignOrderRoutingModule {}
