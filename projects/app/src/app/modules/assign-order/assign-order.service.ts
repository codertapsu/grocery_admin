import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { AssignOrder } from './assign-order.model';

@Injectable()
export class AssignOrderService {
  public constructor() {}

  public getAssignOrder(): Observable<AssignOrder[]> {
    return of([]);
  }
}
