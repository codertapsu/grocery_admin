import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MaterialModule, PaginatorModule } from '@ui';

import { AssignOrderRoutingModule } from './assign-order-routing.module';
import { AssignOrderComponent } from './assign-order.component';
import { AssignOrderService } from './assign-order.service';

@NgModule({
  declarations: [AssignOrderComponent],
  imports: [CommonModule, PaginatorModule, MaterialModule, AssignOrderRoutingModule],
  providers: [AssignOrderService],
})
export class AssignOrderModule {}
