import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { Observable, of } from 'rxjs';

import { PaginationTable } from '@shared/abstractions';

import { AssignOrder } from './assign-order.model';
import { PaginationData } from '@core/models/pagination-data.model';

@Component({
  templateUrl: './assign-order.component.html',
  styleUrls: ['./assign-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AssignOrderComponent extends PaginationTable<AssignOrder> implements OnInit {
  public constructor() {
    super();
  }

  public override ngOnInit(): void {
    super.ngOnInit();
  }

  public applyFilter(event: Event) {
    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  protected getSource(): Observable<PaginationData<AssignOrder>> {
    return of({
      data: [],
      meta: {
        page: 1,
        take: 10,
        itemCount: 0,
        pageCount: 0,
        hasPreviousPage: false,
        hasNextPage: false,
      },
    });
  }
}
