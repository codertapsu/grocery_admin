import { NgModule } from '@angular/core';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoriesService } from './category.service';

@NgModule({
  imports: [CategoryRoutingModule],
  providers: [CategoriesService]
})
export class CategoryModule {}
