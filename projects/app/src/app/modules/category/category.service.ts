import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { SortDirection } from '@core/constants/sort-direction.constant';
import { PaginationData } from '@core/models/pagination-data.model';
import { CategoriesService as CategoriesServiceBase } from '@core/services/categories.service';

import { Category } from './models/category.model';

@Injectable()
export class CategoriesService {
  public constructor(private _categoriesServiceBase: CategoriesServiceBase) {}

  public getAll(): Observable<Category[]> {
    return this._categoriesServiceBase.getAll();
  }

  public getFilteredPaginationData(
    page: number,
    take: number,
    search: string,
    sortBy: string,
    sortDirection: SortDirection,
  ): Observable<PaginationData<Category>> {
    return this._categoriesServiceBase.getFilteredPaginationData({
      page,
      take,
      search,
      sortBy,
      sortDirection,
    });
  }

  public create(category: Category): Observable<number> {
    const formData = new FormData();
    formData.append('name', category.name);
    category.medias.forEach(media => {
      formData.append('medias', media.file as Blob);
    });
    formData.append('isFeature', category.isFeature ? 'true' : 'false');
    return this._categoriesServiceBase.createFormData<number>(formData);
  }

  public update(category: Category): Observable<Category> {
    const formData = new FormData();
    formData.append('name', category.name);
    category.medias.forEach(media => {
      formData.append('medias', media.file as Blob);
    });
    formData.append('isFeature', category.isFeature ? 'true' : 'false');
    return this._categoriesServiceBase.updateFormData(category.id, formData);
  }

  public remove(category: Category): Observable<unknown> {
    return this._categoriesServiceBase.remove(category.id);
  }
}
