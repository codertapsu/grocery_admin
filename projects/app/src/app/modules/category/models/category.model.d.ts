import { Category as CategoryBase } from '@core/models/category.model';

export interface Category extends CategoryBase {}
