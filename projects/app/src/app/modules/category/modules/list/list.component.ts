import { ChangeDetectionStrategy, ChangeDetectorRef, Component, createNgModuleRef, Injector } from '@angular/core';

import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, startWith, switchMap } from 'rxjs/operators';

import { ANIMATIONS } from '@common';
import { SortDirection } from '@core/constants/sort-direction.constant';
import { PaginationData } from '@core/models/pagination-data.model';
import { PaginationTable } from '@shared/abstractions';
import { NotificationService, OverlayService, Pagination } from '@ui';

import { CategoriesService } from '../../category.service';
import { Category } from '../../models/category.model';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [ANIMATIONS],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<Category> {
  public readonly search$ = new Subject<string>();
  public readonly sort$ = new BehaviorSubject<{
    by: string;
    direction: SortDirection;
  }>({
    by: 'createdAt',
    direction: SortDirection.DESC,
  });

  public constructor(
    private _overlayService: OverlayService,
    private _injector: Injector,
    private _categoryService: CategoriesService,
    private _notificationService: NotificationService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public createOrUpdate(data: Category = {} as Category): void {
    import('../form')
      .then(({ FormComponent, FormModule }) => {
        const ngModuleRef = createNgModuleRef(
          FormModule,
          Injector.create({
            parent: this._injector,
            providers: [
              {
                provide: CategoriesService,
                useValue: this._categoryService,
              },
            ],
          }),
        );
        this._overlayService
          .open<Category['id']>(FormComponent, {
            data,
            ngModuleRef,
            disableClose: false,
          })
          .afterClosed$.pipe(filter(closed => !!closed.result))
          .subscribe({
            next: () => {
              this.refresh();
              const message = data.id ? 'Category updated' : 'Category created';
              this._notificationService.success(message);
            },
            error: () => {
              const message = data.id ? 'Error while updating category' : 'Error while creating category';
              this._notificationService.error(message);
            },
          });
      })
      .catch(error => {
        const message = data.id ? 'Error while updating category' : 'Error while creating category';
        this._notificationService.error(message);
      });
  }

  public remove(item: Category): void {
    this._categoryService.remove(item).subscribe({
      next: () => {
        this.refresh();
      },
      error: () => {
        this._notificationService.error('Error while removing category');
      },
    });
  }

  protected getSource(): Observable<PaginationData<Category>> {
    return combineLatest([
      this.pagination$,
      this.search$.pipe(debounceTime(800), distinctUntilChanged(), startWith('')),
      this.sort$,
    ]).pipe(
      switchMap(([pagination, search, sort]) =>
        this._categoryService.getFilteredPaginationData(
          pagination.index,
          pagination.size,
          search,
          sort.by,
          sort.direction,
        ),
      ),
    );
  }
}
