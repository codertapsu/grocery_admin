import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule, MediaUploaderModule } from '@ui';

import { FormComponent } from './form.component';

@NgModule({
  declarations: [FormComponent],
  imports: [CommonModule, ReactiveFormsModule, MaterialModule, MediaUploaderModule],
  bootstrap: [FormComponent],
})
export class FormModule {}
