import { Shipper as ShipperBase } from '@core/models/shipper.model';

export interface Shipper extends ShipperBase {}
