import { NgModule } from '@angular/core';

import { ShippersRoutingModule } from './shippers-routing.module';
import { ShippersService } from './shippers.service';

@NgModule({
  imports: [ShippersRoutingModule],
  providers: [ShippersService],
})
export class ShippersModule {}
