import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { SortDirection } from '@core/constants/sort-direction.constant';
import { PaginationData } from '@core/models/pagination-data.model';
import { ShippersService as ShippersServiceBase } from '@core/services/shippers.service';
import { Shipper } from './models/shipper.model';

@Injectable()
export class ShippersService {
  public constructor(private _shippersServiceBase: ShippersServiceBase) {}

  public getAll(): Observable<Shipper[]> {
    return this._shippersServiceBase.getAll();
  }

  public getFilteredPaginationData(
    page: number,
    take: number,
    search: string,
    sortBy: string,
    sortDirection: SortDirection,
  ): Observable<PaginationData<Shipper>> {
    return this._shippersServiceBase.getFilteredPaginationData({
      page,
      take,
      search,
      sortBy,
      sortDirection,
    });
  }

  public create(shipper: Shipper): Observable<number> {
    const formData = new FormData();
    formData.append('firstName', shipper.firstName);
    formData.append('lastName', shipper.lastName);
    formData.append('email', shipper.email);
    formData.append('phone', shipper.phone);
    formData.append('password', shipper.password);
    formData.append('avatar', shipper.avatar.file as Blob);
    formData.append('isActive', shipper.isActive ? 'true' : 'false');

    return this._shippersServiceBase.createFormData<number>(formData);
  }

  public update(shipper: Shipper): Observable<Shipper> {
    const formData = new FormData();
    formData.append('firstName', shipper.firstName);
    formData.append('lastName', shipper.lastName);
    formData.append('email', shipper.email);
    formData.append('phone', shipper.phone);
    formData.append('password', shipper.password);
    formData.append('avatar', shipper.avatar.file as Blob);
    formData.append('isActive', shipper.isActive ? 'true' : 'false');

    return this._shippersServiceBase.updateFormData(shipper.id, formData);
  }

  public remove(shipper: Shipper): Observable<unknown> {
    return this._shippersServiceBase.remove(shipper.id);
  }
}
