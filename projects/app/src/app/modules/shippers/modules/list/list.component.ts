import { ChangeDetectionStrategy, ChangeDetectorRef, Component, createNgModuleRef, Injector } from '@angular/core';

import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, startWith, switchMap } from 'rxjs/operators';

import { PaginationData } from '@core/models/pagination-data.model';
import { PaginationTable } from '@shared/abstractions';
import { NotificationService, OverlayService } from '@ui';

import { Shipper } from '../../models/shipper.model';
import { ShippersService } from '../../shippers.service';
import { SortDirection } from '@core/constants/sort-direction.constant';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<Shipper> {
  public readonly search$ = new Subject<string>();
  public readonly sort$ = new BehaviorSubject<{
    by: string;
    direction: SortDirection;
  }>({
    by: 'createdAt',
    direction: SortDirection.DESC,
  });

  public constructor(
    private _overlayService: OverlayService,
    private _injector: Injector,
    private _shippersService: ShippersService,
    private _notificationService: NotificationService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public createOrUpdate(data: Shipper = {} as Shipper): void {
    import('../form')
      .then(({ FormComponent, FormModule }) => {
        const ngModuleRef = createNgModuleRef(
          FormModule,
          Injector.create({
            parent: this._injector,
            providers: [
              {
                provide: ShippersService,
                useValue: this._shippersService,
              },
            ],
          }),
        );
        this._overlayService
          .open<Shipper['id']>(FormComponent, {
            data,
            ngModuleRef,
            disableClose: false,
          })
          .afterClosed$.pipe(filter(closed => !!closed.result))
          .subscribe({
            next: () => {
              this.refresh();
              const message = data.id ? 'Category updated' : 'Category created';
              this._notificationService.success(message);
            },
            error: () => {
              const message = data.id ? 'Error while updating category' : 'Error while creating category';
              this._notificationService.error(message);
            },
          });
      })
      .catch(error => {
        const message = data.id ? 'Error while updating category' : 'Error while creating category';
        this._notificationService.error(message);
      });
  }

  public remove(item: Shipper): void {
    this._shippersService.remove(item).subscribe({
      next: () => {
        this.refresh();
      },
      error: () => {
        this._notificationService.error('Error while removing category');
      },
    });
  }

  protected getSource(): Observable<PaginationData<Shipper>> {
    return combineLatest([
      this.pagination$,
      this.search$.pipe(debounceTime(800), distinctUntilChanged(), startWith('')),
      this.sort$,
    ]).pipe(
      switchMap(([pagination, search, sort]) =>
        this._shippersService.getFilteredPaginationData(
          pagination.index,
          pagination.size,
          search,
          sort.by,
          sort.direction,
        ),
      ),
    );
  }
}
