import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MediaViewerComponent } from '@shared/components/media-viewer/media-viewer.component';
import { PipesModule } from '@shared/pipes/pipes.module';
import { MaterialModule, PaginatorModule } from '@ui';

import { ListComponent } from './list.component';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    PaginatorModule,
    MaterialModule,
    PipesModule,
    FormsModule,
    MediaViewerComponent,
    RouterModule.forChild([
      {
        path: '',
        component: ListComponent,
      },
    ]),
  ],
})
export class ListModule {}
