import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

import { TypedFormGroup } from '@common';
import { ShippersService } from '@modules/shippers/shippers.service';
import { Shipper } from '@modules/shippers/models/shipper.model';
import { OVERLAY_DATA, OverlayRef } from '@ui';

@Component({
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
  public form: TypedFormGroup<Shipper>;

  public constructor(
    private _shipperService: ShippersService,
    private _overlayRef: OverlayRef<Shipper['id']>,
    @Inject(OVERLAY_DATA) private _initialData: Shipper,
  ) {}

  public ngOnInit(): void {
    this.form = this.buildForm(this._initialData);
  }

  public submit(): void {
    this.form.markAsSubmitted();
    if (this.form.valid) {
      this._shipperService.create(this.form.getRawValue()).subscribe(id => {
        this._overlayRef.close(id);
      });
    }
  }

  private buildForm(shipper: Shipper): TypedFormGroup<Shipper> {
    return new TypedFormGroup<Shipper>({
      id: new UntypedFormControl(shipper.id),
      name: new UntypedFormControl(shipper.name),
      firstName: new UntypedFormControl(shipper.firstName),
      lastName: new UntypedFormControl(shipper.lastName),
      email: new UntypedFormControl(shipper.email),
      phone: new UntypedFormControl(shipper.phone),
      password: new UntypedFormControl(shipper.password),
      avatar: new UntypedFormControl(shipper.avatar),
      isActive: new UntypedFormControl(shipper.isActive),
    });
  }
}
