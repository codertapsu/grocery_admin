import { NgModule } from '@angular/core';

import { VerificationRoutingModule } from './verification-routing.module';
import { VerificationComponent } from './verification.component';

@NgModule({
  declarations: [VerificationComponent],
  imports: [VerificationRoutingModule],
})
export class VerificationModule {}
