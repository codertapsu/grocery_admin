import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VerificationComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    // const abc = window.crypto.getRandomValues(new Uint8Array(10));
  }
}
