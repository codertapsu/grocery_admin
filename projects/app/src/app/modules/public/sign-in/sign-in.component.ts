import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { noop } from 'rxjs';

import { TypedFormGroup } from '@common';
import { AuthService } from '@security';
import { LoadingService } from '@shared/services/loading.service';
import { NotificationService } from '@ui';

import { SignIn } from '../models/sign-in.model';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignInComponent implements OnInit {
  public readonly form = new TypedFormGroup<SignIn>({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    rememberMe: new FormControl(false),
  });

  public constructor(
    private _authService: AuthService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _loadingService: LoadingService,
    private _notificationService: NotificationService,
  ) {}

  public ngOnInit(): void {
    if (this._authService.snapshot.user) {
      this._router.navigateByUrl('/').then(noop);
    }
  }

  public submitForm(): void {
    this.form.markAsSubmitted();
    if (this.form.valid) {
      this._loadingService.show();
      const { username, password } = this.form.getRawValue();
      this._authService.login(username, password).subscribe({
        next: authenticatedUser => {
          this._loadingService.hide();
          if (authenticatedUser.isTwoFactorAuthenticationEnabled && !authenticatedUser.id) {
            this._router.navigate(['/guest/otp-confirmation'], { queryParamsHandling: 'merge' }).then(noop);
          } else {
            this._router
              .navigateByUrl(this._activatedRoute.snapshot.queryParamMap.get('returnUrl') || '/')
              .then(() => this._notificationService.success('Success', 'Welcome back!'));
          }
        },
        error: err => {
          this._loadingService.hide();
          this._notificationService.error(err.message);
        },
      });
    }
  }
}
