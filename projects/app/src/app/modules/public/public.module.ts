import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { InputModule, MaterialModule } from '@ui';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { OtpConfirmationComponent } from './otp-confirmation/otp-confirmation.component';

@NgModule({
  declarations: [SignInComponent, PublicComponent, ResetPasswordComponent, ForgotPasswordComponent, OtpConfirmationComponent],
  imports: [CommonModule, ReactiveFormsModule, MaterialModule, InputModule, PublicRoutingModule],
})
export class PublicModule {}
