import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TypedFormGroup } from '@common';
import { AuthService } from '@security';
import { LoadingService } from '@shared/services/loading.service';
import { NotificationService } from '@ui';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForgotPasswordComponent implements OnInit {
  public readonly form = new TypedFormGroup<{ email: string }>({
    email: new FormControl('', [Validators.required]),
  });

  public hasOtpValidation = false;

  public constructor(
    private _authService: AuthService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _loadingService: LoadingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _notificationService: NotificationService,
  ) {}

  ngOnInit(): void {}

  public submitForm(): void {
    this.form.markAsSubmitted();
    if (this.form.valid) {
      this._loadingService.show();
      const { email } = this.form.getRawValue();
      this._authService.forgotPassword(email).subscribe({
        next: () => {
          this._loadingService.hide();
          // const returnUrl = this._activatedRoute.snapshot.queryParamMap.get('returnUrl') || '/';
          // this._router
          //   .navigateByUrl(returnUrl)
          //   .then(() => this._notificationService.success('Success', 'Welcome back!'));
        },
        error: err => {
          this._loadingService.hide();
          this._notificationService.error(err.message);
        },
      });
    }
  }
}
