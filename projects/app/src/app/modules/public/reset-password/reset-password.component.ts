import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { map } from 'rxjs/operators';

import { TypedFormGroup } from '@common';
import { AuthService } from '@security';
import { LoadingService } from '@shared/services/loading.service';
import { NotificationService } from '@ui';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResetPasswordComponent implements OnInit {
  public readonly form = new TypedFormGroup<{
    token: string;
    password: string;
    confirmPassword: string;
  }>(
    {
      token: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      confirmPassword: new FormControl('', [Validators.required]),
    },
    [
      formGroup => {
        const { password, confirmPassword } = formGroup.getRawValue();
        return password === confirmPassword ? null : { confirmPassword: true };
      },
    ],
  );

  public hasOtpValidation = false;

  public constructor(
    private _authService: AuthService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _loadingService: LoadingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _notificationService: NotificationService,
  ) {}

  public ngOnInit(): void {
    this._activatedRoute.queryParamMap.pipe(map(queryParamMap => queryParamMap.get('token'))).subscribe(token => {
      this.form.patchValue({ token });
      this._changeDetectorRef.markForCheck();
    });
  }

  public submitForm(): void {
    this.form.markAsSubmitted();
    if (this.form.valid) {
      this._loadingService.show();
      const { password, token } = this.form.getRawValue();
      this._authService.resetPassword(token, password).subscribe({
        next: () => {
          this._loadingService.hide();
          // const returnUrl = this._activatedRoute.snapshot.queryParamMap.get('returnUrl') || '/';
          // this._router
          //   .navigateByUrl(returnUrl)
          //   .then(() => this._notificationService.success('Success', 'Welcome back!'));
        },
        error: err => {
          this._loadingService.hide();
          this._notificationService.error(err.message);
        },
      });
    }
  }
}
