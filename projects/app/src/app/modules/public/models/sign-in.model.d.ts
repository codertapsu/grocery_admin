export interface SignIn {
  username: string;
  password: string;
  rememberMe: boolean;
}
