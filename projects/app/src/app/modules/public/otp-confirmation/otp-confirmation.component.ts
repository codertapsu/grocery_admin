import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TypedFormGroup } from '@common';
import { AuthService } from '@security';
import { LoadingService } from '@shared/services/loading.service';
import { NotificationService } from '@ui';

@Component({
  selector: 'app-otp-confirmation',
  templateUrl: './otp-confirmation.component.html',
  styleUrls: ['./otp-confirmation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OtpConfirmationComponent implements OnInit {
  public readonly form = new TypedFormGroup<{ otp: string }>({
    otp: new FormControl('', [Validators.required]),
  });

  public constructor(
    private _authService: AuthService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _loadingService: LoadingService,
    private _changeDetectorRef: ChangeDetectorRef,
    private _notificationService: NotificationService,
  ) {}

  ngOnInit(): void {}

  public submitForm(): void {
    this.form.markAsSubmitted();
    if (this.form.valid) {
      this._loadingService.show();
      const { otp } = this.form.getRawValue();
      this._authService.authenticate2fa(otp).subscribe({
        next: () => {
          this._loadingService.hide();
          this._router
            .navigateByUrl(this._activatedRoute.snapshot.queryParamMap.get('returnUrl') || '/')
            .then(() => this._notificationService.success('Success', 'Welcome back!'));
        },
        error: err => {
          this._loadingService.hide();
          this._notificationService.error(err.message);
        },
      });
    }
  }
}
