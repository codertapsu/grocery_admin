import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { OrderStatus } from '@core/models/order-status.model';
import { OrdersService } from '@core/services/orders.service';

@Injectable()
export class OrderStatusService {
  public constructor(private order: OrdersService) {}

  public getAll(): Observable<OrderStatus[]> {
    return this.order.getOrderStatuses();
  }
}
