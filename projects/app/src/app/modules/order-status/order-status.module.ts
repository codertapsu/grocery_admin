import { NgModule } from '@angular/core';

import { OrderStatusRoutingModule } from './order-status-routing.module';
import { OrderStatusService } from './order-status.service';

@NgModule({
  imports: [OrderStatusRoutingModule],
  providers: [OrderStatusService],
})
export class OrderStatusModule {}
