import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { Observable, of } from 'rxjs';

import { PaginationData } from '@core/models/pagination-data.model';
import { PaginationTable } from '@shared/abstractions';
import { OverlayService } from '@ui';

import { OrderStatus } from '../../models/order-status.model';
import { OrderStatusService } from '../../order-status.service';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<OrderStatus> implements OnInit {
  public constructor(
    private overlayService: OverlayService,
    private orderStatusService: OrderStatusService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public override ngOnInit(): void {
    super.ngOnInit();
  }

  public applyFilter(event: Event) {
    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  public editOrderStatus(orderStatus: OrderStatus): void {}

  protected getSource(): Observable<PaginationData<OrderStatus>> {
    return of({
      data: [],
      meta: {
        page: 1,
        take: 10,
        itemCount: 0,
        pageCount: 0,
        hasPreviousPage: false,
        hasNextPage: false,
      },
    });
  }
}
