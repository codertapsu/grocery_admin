import { OrderStatus as OrderStatusBase } from '@core/models/order-status.model';

export interface OrderStatus extends OrderStatusBase {}
