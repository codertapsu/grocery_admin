import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Category } from '@core/models/category.model';
import { Currency } from '@core/models/currency.model';
import { CategoriesService } from '@core/services/categories.service';
import { ProductsService as ProductServiceBase } from '@core/services/products.service';
import { WellKnownService } from '@core/services/well-known.service';

import { Product } from './models/product.model';
import { SortDirection } from '@core/constants/sort-direction.constant';
import { PaginationData } from '@core/models/pagination-data.model';

@Injectable()
export class ProductService {
  public constructor(
    private _productServiceBase: ProductServiceBase,
    private _categoryService: CategoriesService,
    private _wellKnownService: WellKnownService,
  ) {}

  public getAll(): Observable<Product[]> {
    return this._productServiceBase.getAll();
  }

  public getFilteredPaginationData(
    page: number,
    take: number,
    search: string,
    sortBy: string,
    sortDirection: SortDirection,
  ): Observable<PaginationData<Product>> {
    return this._productServiceBase.getFilteredPaginationData({
      page,
      take,
      search,
      sortBy,
      sortDirection,
    });
  }

  public create(product: Product): Observable<Product> {
    const formData = new FormData();
    formData.append('name', product.name);
    product.medias.forEach(media => {
      formData.append('medias', media.file as Blob);
    });
    formData.append('price', String(product.price));
    formData.append('discountPrice', String(product.discountPrice));
    formData.append('currencyId', String(product.currencyId));
    formData.append('stock', String(product.stock));
    formData.append('quantity', String(product.quantity));
    formData.append('unit', product.unit);
    product.categoryIds.forEach(categoryId => {
      formData.append('categoryIds', String(categoryId));
    });
    formData.append('isFeature', product.isFeature ? 'true' : 'false');
    formData.append('isMostPopular', product.isMostPopular ? 'true' : 'false');
    formData.append('isTopDeal', product.isTopDeal ? 'true' : 'false');
    return this._productServiceBase.insert(formData);
  }

  public remove(product: Product): Observable<unknown> {
    return this._productServiceBase.remove(product.id);
  }

  public getCategorySelections(): Observable<Category[]> {
    return this._categoryService.getSelections();
  }

  public getCurrencySelections(): Observable<Currency[]> {
    return this._wellKnownService.getCurrencies();
  }
}
