import { NgModule } from '@angular/core';

import { ProductRoutingModule } from './product-routing.module';
import { ProductService } from './product.service';

@NgModule({
  providers: [ProductService],
  imports: [ProductRoutingModule],
})
export class ProductModule {}
