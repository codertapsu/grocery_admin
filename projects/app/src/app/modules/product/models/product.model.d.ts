import { Product as ProductBase } from '@core/models/product.model';

export interface Product extends ProductBase {}
