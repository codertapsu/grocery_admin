import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { TypedFormGroup } from '@common';
import { Category } from '@core/models/category.model';
import { Currency } from '@core/models/currency.model';
import { NotificationService, OVERLAY_DATA, OverlayRef } from '@ui';

import { Product } from '../../models/product.model';
import { ProductService } from '../../product.service';

@Component({
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
  public categories$: Observable<Category[]>;
  public currencies$: Observable<Currency[]>;
  public form: TypedFormGroup<Product>;
  public signature: any[] = [];

  public constructor(
    private overlayRef: OverlayRef<unknown>,
    private productService: ProductService,
    private notificationService: NotificationService,
    @Inject(OVERLAY_DATA) private initialData: Product,
  ) {}

  public ngOnInit(): void {
    this.form = this.buildForm(this.initialData);

    this.categories$ = this.productService.getCategorySelections().pipe(
      tap({
        error: error => {
          this.notificationService.error(error.message, error.statusText);
        },
      }),
    );
    this.currencies$ = this.productService.getCurrencySelections().pipe(
      tap({
        error: error => {
          this.notificationService.error(error.message, error.statusText);
        },
      }),
    );
  }

  public close(): void {
    this.overlayRef.close();
  }

  public createCategory(): void {}

  public submitForm(): void {
    this.form.markAsSubmitted();
    this.form.markAllAsTouched();
    if (this.form.valid) {
      const product = this.form.getRawValue();
      this.productService.create(product).subscribe(id => {
        this.overlayRef.close(id);
      });
    }
  }

  private buildForm(product: Product): TypedFormGroup<Product> {
    return new TypedFormGroup<Product>({
      id: new UntypedFormControl(product.id),
      name: new UntypedFormControl(product.name, [Validators.required]),
      medias: new UntypedFormControl(product.medias || [], [Validators.required, Validators.minLength(1)]),
      price: new UntypedFormControl(product.price, [Validators.required]),
      discountPrice: new UntypedFormControl(product.discountPrice),
      currencyId: new UntypedFormControl(product.currencyId, { nonNullable: true, validators: [Validators.required] }),
      description: new UntypedFormControl(product.description),
      stock: new UntypedFormControl(product.stock, [Validators.required]),
      quantity: new UntypedFormControl(product.quantity, [Validators.required]),
      unit: new UntypedFormControl(product.unit, [Validators.required]),
      categoryIds: new UntypedFormControl(product.categoryIds, [Validators.required]),
      isFeature: new UntypedFormControl(product.isFeature),
      isMostPopular: new UntypedFormControl(product.isMostPopular),
      isTopDeal: new UntypedFormControl(product.isTopDeal),
    });
  }
}
