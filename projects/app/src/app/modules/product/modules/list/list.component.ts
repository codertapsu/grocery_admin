import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  createNgModuleRef,
  Injector,
  ViewContainerRef,
} from '@angular/core';

import { BehaviorSubject, combineLatest, from, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, startWith, switchMap } from 'rxjs/operators';

import { SortDirection } from '@core/constants/sort-direction.constant';
import { PaginationData } from '@core/models/pagination-data.model';
import { PaginationTable } from '@shared/abstractions';
import { NotificationService, OverlayService } from '@ui';

import { Product } from '../../models/product.model';
import { ProductService } from '../../product.service';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<Product> {
  public readonly search$ = new Subject<string>();
  public readonly sort$ = new BehaviorSubject<{
    by: string;
    direction: SortDirection;
  }>({
    by: 'createdAt',
    direction: SortDirection.DESC,
  });

  public constructor(
    private _overlayService: OverlayService,
    private _viewContainerRef: ViewContainerRef,
    private _injector: Injector,
    private _notificationService: NotificationService,
    private _productService: ProductService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public createOrUpdate(data: Product = {} as Product): void {
    import('../form')
      .then(({ FormComponent, FormModule }) => {
        const ngModuleRef = createNgModuleRef(
          FormModule,
          Injector.create({
            parent: this._injector,
            providers: [
              {
                provide: ProductService,
                useValue: this._productService,
              },
            ],
          }),
        );
        this._overlayService
          .open<Product>(FormComponent, {
            data,
            ngModuleRef,
            disableClose: true,
          })
          .afterClosed$.pipe(filter(closedResult => !!closedResult.result))
          .subscribe({
            next: () => {
              this.refresh();
              this._notificationService.success('Product created successfully');
            },
            error: () => {
              this._notificationService.error('Error creating product');
            },
          });
      })
      .catch(error => {
        this._notificationService.error(error.message || 'Error creating product');
      });
  }

  public edit(data: Product): void {
    from(import('../form'))
      .pipe(
        switchMap(
          ({ FormModule, FormComponent }) =>
            this._overlayService.open<Product['id']>(FormComponent, { data, disableClose: true }).afterClosed$,
        ),
        map(closed => closed.result),
        filter(Boolean),
      )
      .subscribe({
        next: productId => {
          this.refresh();
        },
        error: error => {
          console.log(error);
        },
        complete: () => {},
      });
  }

  public remove(item: Product): void {
    this._productService.remove(item).subscribe(() => {
      this.refresh();
    });
  }

  protected getSource(): Observable<PaginationData<Product>> {
    return combineLatest([
      this.pagination$,
      this.search$.pipe(debounceTime(800), distinctUntilChanged(), startWith('')),
      this.sort$,
    ]).pipe(
      switchMap(([pagination, search, sort]) =>
        this._productService.getFilteredPaginationData(
          pagination.index,
          pagination.size,
          search,
          sort.by,
          sort.direction,
        ),
      ),
    );
  }
}
