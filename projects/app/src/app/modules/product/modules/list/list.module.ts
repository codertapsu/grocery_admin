import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MediaViewerComponent } from '@shared/components/media-viewer/media-viewer.component';

import { DirectivesModule } from '@shared/diretives/directives.module';
import { PipesModule } from '@shared/pipes/pipes.module';
import { ConfirmationModule, MaterialModule, PaginatorModule, SignaturePadModule, TooltipModule } from '@ui';

import { ListComponent } from './list.component';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    MaterialModule,
    PaginatorModule,
    TooltipModule,
    DirectivesModule,
    PipesModule,
    ConfirmationModule,
    SignaturePadModule,
    MediaViewerComponent,
    RouterModule.forChild([
      {
        path: '',
        component: ListComponent,
      },
    ]),
  ],
})
export class ListModule {}
