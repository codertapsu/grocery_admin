import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserRole } from '@core/constants/user-role.constant';

import { RoleGuard } from '@core/guards/role.guard';

import { PrivateComponent } from './private.component';

const routes: Routes = [
  {
    path: '',
    component: PrivateComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'assign-orders',
        loadChildren: () => import('@modules/assign-order/assign-order.module').then(m => m.AssignOrderModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'banner-management',
        loadChildren: () =>
          import('@modules/banners-management/banners-management.module').then(m => m.BannersManagementModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'categories',
        loadChildren: () => import('@modules/category/category.module').then(m => m.CategoryModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'coupons',
        loadChildren: () => import('@modules/coupon/coupon.module').then(m => m.CouponModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'customer-service',
        loadChildren: () =>
          import('@modules/customer-service/customer-service.module').then(m => m.CustomerServiceModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'dashboard',
        loadChildren: () => import('@modules/dashboard/dashboard.module').then(m => m.DashboardModule),
        data: {
          roles: [UserRole.SystemUser, UserRole.StoreAdmin, UserRole.StoreModerator],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'orders',
        loadChildren: () => import('@modules/order/order.module').then(m => m.OrderModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'partners',
        loadChildren: () => import('@modules/partner/partner.module').then(m => m.PartnerModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'products',
        loadChildren: () => import('@modules/product/product.module').then(m => m.ProductModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'shippers',
        loadChildren: () => import('@modules/shippers/shippers.module').then(m => m.ShippersModule),
        data: {
          roles: [UserRole.SystemAdmin, UserRole.SystemModerator, UserRole.StoreAdmin, UserRole.StoreModerator],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'users',
        loadChildren: () => import('@modules/users/users.module').then(m => m.UsersModule),
        data: {
          roles: [UserRole.SystemAdmin, UserRole.SystemModerator, UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'profile',
        loadChildren: () => import('@modules/profile/profile.module').then(m => m.ProfileModule),
        data: {
          roles: [UserRole.SystemAdmin, UserRole.SystemModerator, UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'zip-code',
        loadChildren: () => import('@modules/zip-code/zip-code.module').then(m => m.ZipCodeModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
      {
        path: 'settings',
        loadChildren: () => import('@modules/settings/settings.module').then(m => m.SettingsModule),
        data: {
          roles: [UserRole.SystemUser],
        },
        canLoad: [RoleGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrivateRoutingModule {}
