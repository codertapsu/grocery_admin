import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AvatarModule, MaterialModule, ProgressIndicatorModule } from '@ui';

import { SidebarMenuComponent } from './components/sidebar-menu/sidebar-menu.component';
import { PrivateRoutingModule } from './private-routing.module';
import { PrivateComponent } from './private.component';
import { BroadCastService } from './services/broad-cast.service';
import { GeoLocationPositionService } from './services/geo-location-position.service';
import { FabComponent } from './components/fab/fab.component';
import { DirectivesModule } from '@shared/diretives/directives.module';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [PrivateComponent, SidebarMenuComponent, FabComponent, BreadcrumbComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    AvatarModule,
    ProgressIndicatorModule,
    DirectivesModule,
    PrivateRoutingModule,
  ],
  providers: [BroadCastService, GeoLocationPositionService],
})
export class PrivateModule {}
