import { Injectable, OnDestroy } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable()
export class BroadCastService implements OnDestroy {
  private _registeredChannelsMap = new Map<string, BroadcastChannel>();

  public ngOnDestroy(): void {
    this._registeredChannelsMap.forEach(channel => {
      channel.close();
    });
  }

  public listen<R>(channelName: string): Observable<R> {
    const channel = this.register(channelName);
    return new Observable<R>(observer => {
      channel.onmessage = (event: MessageEvent) => {
        observer.next(event.data as R);
      };
    });
  }

  public close(channelName: string): void {
    const channel = this._registeredChannelsMap.get(channelName);
    if (channel) {
      channel.close();
      this._registeredChannelsMap.delete(channelName);
    }
  }

  public send<R>(channelName: string, data: R): void {
    if (this._registeredChannelsMap.has(channelName)) {
      this._registeredChannelsMap.get(channelName).postMessage(data);
    }
  }

  private register(channelName: string): BroadcastChannel {
    if (this._registeredChannelsMap.has(channelName)) {
      return this._registeredChannelsMap.get(channelName);
    }
    const channel = new BroadcastChannel(channelName);
    this._registeredChannelsMap.set(channelName, channel);

    return channel;
  }
}
