import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { NAVIGATOR } from '@common';

@Injectable()
export class GeoLocationPositionService {
  private readonly _coords$: Observable<GeolocationCoordinates>;

  get coords$() {
    return this._coords$;
  }

  public constructor(@Inject(NAVIGATOR) private navigator: Navigator) {
    this._coords$ = new Observable<GeolocationCoordinates>(observer => {
      if (this.navigator.geolocation) {
        this.navigator.geolocation.getCurrentPosition(
          position => {
            observer.next(position.coords);
            observer.complete();
          },
          error => {
            let title = 'Error occurred';
            let message = 'An unknown error occurred.';
            switch (error.code) {
              case error.PERMISSION_DENIED: {
                title = 'Denied';
                message = 'User denied the request for Geolocation.';
                break;
              }
              case error.POSITION_UNAVAILABLE: {
                title = 'Unavailable';
                message = 'Location information is unavailable.';
                break;
              }
              case error.TIMEOUT: {
                title = 'Timed out';
                message = 'The request to get user location timed out.';
                break;
              }
            }
            observer.error({ title, message });
            observer.complete();
          },
        );
      } else {
        observer.error({ title: 'Unsupported', message: 'Geolocation is not supported by this browser.' });
        observer.complete();
      }
    });
  }
}
