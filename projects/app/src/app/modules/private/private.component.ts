import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  TemplateRef,
  ViewContainerRef,
} from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';

import { noop } from 'rxjs';

import { WINDOW } from '@common';
import { AuthenticatedUser, AuthService } from '@security';
import { fader } from '@shared/animations';
import { SupportedLanguages } from '@shared/constants/supported-languages.constant';
import { LocaleService } from '@shared/services/locale.service';
import { PullToRefreshService } from '@shared/services/pull-to-refresh.service';
import { NotificationService, OverlayService } from '@ui';

import { SidebarMenuComponent } from './components/sidebar-menu/sidebar-menu.component';

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [fader],
})
export class PrivateComponent implements OnInit, AfterViewChecked {
  public pullTop = -40;
  public pullPercent = 0;
  public currentLocale: string;
  public supportedLanguages: any = [];
  public authenticatedUser: AuthenticatedUser;

  public constructor(
    private _overlayService: OverlayService,
    private _viewContainerRef: ViewContainerRef,
    private _changeDetectorRef: ChangeDetectorRef,
    private _router: Router,
    private _authService: AuthService,
    private _notificationService: NotificationService,
    private _pullToRefreshService: PullToRefreshService,
    private _localeService: LocaleService,
    @Inject(WINDOW) private _window: Window,
  ) {}

  public ngOnInit(): void {
    this._authService.authUser$.subscribe(user => {
      this.authenticatedUser = {
        avatar: user?.avatar || {
          name: '',
          path: 'assets/icons/avatar_default.jpg',
          mimetype: 'image/jpeg',
          size: 0,
        },
        id: user?.id,
        email: user?.email,
        firstName: user?.firstName,
        lastName: user?.lastName,
        name: user?.name || 'Guest',
        phone: user?.phone || '',
        username: user?.username || 'guest',
        isActive: user?.isActive,
        isTwoFactorAuthenticationEnabled: user?.isTwoFactorAuthenticationEnabled,
        isEmailConfirmed: user?.isEmailConfirmed,
        isPhoneConfirmed: user?.isPhoneConfirmed,
        rbac: user?.rbac || [],
      };
      this._changeDetectorRef.markForCheck();
    });

    this._localeService.localeChanged$.subscribe(currentLocale => {
      this.currentLocale = currentLocale;
      const languageNames = new Intl.DisplayNames([currentLocale], { type: 'language' });
      const regionNames = new Intl.DisplayNames([currentLocale], { type: 'region' });
      const currencyNames = new Intl.DisplayNames([currentLocale], { type: 'currency' });
      this.supportedLanguages = SupportedLanguages.map(language => ({
        locale: language.locale,
        name: languageNames.of(language.locale),
        region: regionNames.of(language.regionCode),
        currency: currencyNames.of(language.currencyCode),
      }));
    });

    this._pullToRefreshService.onPulling$.subscribe(delta => {
      const pullHeight = Math.max(this._window.innerHeight / 4, 150);
      this.pullPercent = Math.floor((Math.min(delta, pullHeight) / pullHeight) * 100);
      // 120 = 100% =>
      this.pullTop = (this.pullPercent / 120) * 100 - 40;
      this._changeDetectorRef.detectChanges();
    });
    this._pullToRefreshService.onPulled$.subscribe(delta => {
      this.pullPercent = 0;
      this.pullTop = -40;
      this._changeDetectorRef.detectChanges();
    });
  }

  public ngAfterViewChecked(): void {
    // console.log('AfterViewChecked');
  }

  public openMenu(event: Event): void {
    event.stopPropagation();
    this._overlayService.open(SidebarMenuComponent).afterClosed$.subscribe(noop);
  }

  public closeMenu(): void {}

  public registerLocale(locale: string): void {
    this._localeService.registerLocale(locale);
  }

  public openProfile(event: Event, templateRef: TemplateRef<HTMLElement>): void {
    this._overlayService.openPopover(event.target as HTMLElement, templateRef).afterClosed$.subscribe(noop);
  }

  public logout(): void {
    this._authService.logout().subscribe(() => this._router.navigateByUrl('/guest/sign-in').then(noop));
  }

  public trackLanguage(index: number, language: any): string {
    return language.locale;
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet.isActivated && outlet.activatedRoute.routeConfig.path;
  }
}
