import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MenuItem } from '@shared/models/menu-item.model';

@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidebarMenuComponent implements OnInit {
  public readonly menuItems: MenuItem[] = [
    {
      name: 'Dashboard',
      routerLink: '/dashboard',
    },
    {
      name: 'Categories',
      routerLink: '/categories',
    },
    {
      name: 'Products',
      routerLink: '/products',
    },
    {
      name: 'Coupons',
      routerLink: '/coupons',
    },
    {
      name: 'Orders',
      routerLink: '/orders',
    },
    {
      name: 'Users',
      routerLink: '/users',
    },
    {
      name: 'Shippers',
      routerLink: '/shippers',
    },
    {
      name: 'Settings',
      routerLink: '/settings',
      children: [
        {
          name: 'Coupon type',
          routerLink: '/settings/coupon-type',
        },
        {
          name: 'Payment',
          routerLink: '/settings/payment',
        },
      ],
    },
  ];

  public ngOnInit(): void {}
}
