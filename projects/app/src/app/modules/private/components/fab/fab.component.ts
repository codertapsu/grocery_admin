import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  NgZone,
  OnDestroy,
  Renderer2,
} from '@angular/core';

import { ElementDraggable, WINDOW } from '@common';

@Component({
  selector: 'app-fab',
  templateUrl: './fab.component.html',
  styleUrls: ['./fab.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FabComponent implements AfterViewInit, OnDestroy {
  private _elementDraggable: ElementDraggable;
  private readonly _gap = 20;

  public constructor(
    private elementRef: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    private ngZone: NgZone,
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window: Window,
  ) {}

  public ngAfterViewInit(): void {
    this.ngZone.runOutsideAngular(() => {
      this._elementDraggable = new ElementDraggable(
        this.document,
        this.elementRef.nativeElement,
        this._gap,
        this.renderer,
        this.window,
      );
    });
  }

  public ngOnDestroy(): void {
    if (this._elementDraggable) {
      this._elementDraggable.destroy();
    }
  }
}
