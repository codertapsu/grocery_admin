import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BrowserWindow, WINDOW } from '@common';
import { UserProfile } from '@modules/user-profile/user-profile.model';
import { AuthService } from '@security';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent implements OnInit {
  public form: FormGroup;
  public twoFactorImg$: Observable<string>;
  public is2FaEnabled = false;

  public constructor(
    private _changeDetectorRef: ChangeDetectorRef,
    private _authService: AuthService,
    @Inject(WINDOW)
    private _window: BrowserWindow,
  ) {}

  public ngOnInit(): void {
    this.twoFactorImg$ = this._authService.generate2FA().pipe(map(blob => this._window.URL.createObjectURL(blob)));
    this._authService.authUser$.subscribe(userProfile => {
      this.form = this.buildForm(userProfile);
      console.log(userProfile);
      
      this.is2FaEnabled = userProfile.isTwoFactorAuthenticationEnabled;
    });
  }

  public turn2FaOn(otpCode: string): void {
    this._authService.turn2FaOn(otpCode).subscribe(() => {});
  }

  public submitForm(): void {
    // this.overlayRef.close(this.form.getRawValue());
  }

  private buildForm(userProfile: UserProfile): FormGroup {
    return new FormGroup({
      id: new FormControl(userProfile.id),
      name: new FormControl(userProfile.name),
      email: new FormControl(userProfile.email),
      phone: new FormControl(userProfile.phone),
      password: new FormControl(userProfile.password),
      isActive: new FormControl(userProfile.isActive),
      medias: new FormControl(userProfile.avatar ? [userProfile.avatar] : []),
    });
  }
}
