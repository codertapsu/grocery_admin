import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PipesModule } from '@shared/pipes/pipes.module';
import { MaterialModule, MediaUploaderModule } from '@ui';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ProfileService } from './profile.service';

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
    MaterialModule,
    MediaUploaderModule,
    ProfileRoutingModule,
  ],
  providers: [ProfileService],
})
export class ProfileModule {}
