import { ZipCode as ZipCodeBase } from '@core/models/zip-code.model';

export interface ZipCode extends ZipCodeBase {}
