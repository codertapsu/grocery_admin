import { NgModule } from '@angular/core';

import { ZipCodeRoutingModule } from './zip-code-routing.module';
import { ZipCodeService } from './zip-code.service';

@NgModule({
  imports: [ZipCodeRoutingModule],
  providers: [ZipCodeService],
})
export class ZipCodeModule {}
