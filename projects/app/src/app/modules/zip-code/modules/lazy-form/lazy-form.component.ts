import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { OVERLAY_DATA, OverlayRef } from '@ui';

import { ZipCode } from '../../models/zip-code.model';

@Component({
  templateUrl: './lazy-form.component.html',
  styleUrls: ['./lazy-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LazyFormComponent implements OnInit {
  public form: FormGroup;

  public constructor(private overlayRef: OverlayRef<ZipCode>, @Inject(OVERLAY_DATA) private initialData: ZipCode) {}

  public ngOnInit(): void {
    this.form = this.buildForm(this.initialData);
  }

  public submitForm(): void {
    this.overlayRef.close(this.form.getRawValue());
  }

  private buildForm(zipCode: ZipCode): FormGroup {
    return new FormGroup({
      id: new FormControl(zipCode.id),
      name: new FormControl(zipCode.name),
      country: new FormControl(zipCode.country),
      state: new FormControl(zipCode.state),
      city: new FormControl(zipCode.city),
    });
  }
}
