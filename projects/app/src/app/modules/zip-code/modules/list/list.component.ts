import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { Observable, of } from 'rxjs';

import { PaginationTable } from '@shared/abstractions';
import { OverlayService } from '@ui';

import { ZipCode } from '../../models/zip-code.model';
import { ZipCodeService } from '../../zip-code.service';
import { LazyFormComponent } from '../lazy-form';
import { PaginationData } from '@core/models/pagination-data.model';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<ZipCode> implements OnInit {
  public constructor(
    private overlayService: OverlayService,
    private zipCodeService: ZipCodeService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public override ngOnInit(): void {
    super.ngOnInit();
  }

  public changePage(event: unknown): void {
    console.log(event);
  }

  public applyFilter(event: Event) {
    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  public createItem(): void {
    // this.overlayService.open<unknown, unknown>(LazyFormComponent, {}).afterClosed$.subscribe(data => {
    //   this.changeDetectorRef.detectChanges();
    // });
  }

  public editItem(item: ZipCode): void {
    // this.overlayService.open(LazyFormComponent, item);
  }

  protected getSource(): Observable<PaginationData<ZipCode>> {
    return of({
      data: [],
      meta: {
        page: 1,
        take: 10,
        itemCount: 0,
        pageCount: 0,
        hasPreviousPage: false,
        hasNextPage: false,
      },
    });
  }
}
