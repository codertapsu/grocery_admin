import { User } from '@core/models/user.model';

export interface Partner extends User {}
