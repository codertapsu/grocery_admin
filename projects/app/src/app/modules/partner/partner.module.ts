import { NgModule } from '@angular/core';

import { PartnerRoutingModule } from './partner-routing.module';
import { PartnerService } from './partner.service';

@NgModule({
  imports: [PartnerRoutingModule],
  providers: [PartnerService],
})
export class PartnerModule {}
