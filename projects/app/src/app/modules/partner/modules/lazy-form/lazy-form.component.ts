import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { OVERLAY_DATA, OverlayRef } from '@ui';

import { Partner } from '../../models/partner.model';

@Component({
  templateUrl: './lazy-form.component.html',
  styleUrls: ['./lazy-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LazyFormComponent implements OnInit {
  public form: UntypedFormGroup;

  public constructor(private overlayRef: OverlayRef<Partner>, @Inject(OVERLAY_DATA) private initialData: Partner) {}

  public ngOnInit(): void {
    this.form = this.buildForm(this.initialData);
  }

  public submitForm(): void {
    this.overlayRef.close(this.form.getRawValue());
  }

  private buildForm(partner: Partner): UntypedFormGroup {
    return new UntypedFormGroup({
      id: new UntypedFormControl(partner.id),
      name: new UntypedFormControl(partner.name),
      email: new UntypedFormControl(partner.email),
      phone: new UntypedFormControl(partner.phone),
      // password: new UntypedFormControl(partner.password),
      // isActive: new UntypedFormControl(partner.isActive),
      medias: new UntypedFormControl(partner.avatar ? [partner.avatar] : []),
    });
  }
}
