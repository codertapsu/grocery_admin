import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { Observable, of } from 'rxjs';

import { PaginationTable } from '@shared/abstractions';
import { OverlayService } from '@ui';

import { LazyFormComponent } from '../lazy-form';
import { PartnerService } from '../../partner.service';
import { Partner } from '../../models/partner.model';
import { PaginationData } from '@core/models/pagination-data.model';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<Partner> implements OnInit {
  public constructor(
    private overlayService: OverlayService,
    private partnerService: PartnerService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public override ngOnInit(): void {
    super.ngOnInit();
  }

  public applyFilter(event: Event) {
    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  public createItem(): void {
    // this.overlayService.open<unknown, unknown>(LazyFormComponent, {}).afterClosed$.subscribe(data => {
    //   this.changeDetectorRef.detectChanges();
    // });
  }

  public editItem(item: Partner): void {
    // this.overlayService.open(LazyFormComponent, item);
  }

  protected getSource(): Observable<PaginationData<Partner>> {
    return of({
      data: [],
      meta: {
        page: 1,
        take: 10,
        itemCount: 0,
        pageCount: 0,
        hasPreviousPage: false,
        hasNextPage: false,
      },
    });
  }
}
