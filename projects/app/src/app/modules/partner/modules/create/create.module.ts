import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule, MediaUploaderModule } from '@ui';

import { CreateComponent } from './create.component';

@NgModule({
  declarations: [CreateComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    MediaUploaderModule,
    RouterModule.forChild([
      {
        path: '',
        component: CreateComponent,
      },
    ]),
  ],
})
export class CreateModule {}
