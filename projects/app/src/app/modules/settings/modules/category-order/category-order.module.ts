import { NgModule } from '@angular/core';

import { CategoryOrderRoutingModule } from './category-order-routing.module';
import { CategoryOrderComponent } from './category-order.component';


@NgModule({
  declarations: [
    CategoryOrderComponent
  ],
  imports: [,
    CategoryOrderRoutingModule
  ]
})
export class CategoryOrderModule { }
