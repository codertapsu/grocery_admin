import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-category-order',
  templateUrl: './category-order.component.html',
  styleUrls: ['./category-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryOrderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
