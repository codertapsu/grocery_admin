import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '@shared/diretives/directives.module';
import { PipesModule } from '@shared/pipes/pipes.module';
import { ConfirmationModule, DialogModule, MaterialModule, PaginatorModule } from '@ui';

import { CouponTypeRoutingModule } from './coupon-type-routing.module';
import { CouponTypeComponent } from './coupon-type.component';
import { CouponTypeService } from './coupon-type.service';

@NgModule({
  declarations: [CouponTypeComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    PaginatorModule,
    DirectivesModule,
    ConfirmationModule,
    DialogModule,
    PipesModule,
    CouponTypeRoutingModule,
  ],
  providers: [CouponTypeService],
})
export class CouponTypeModule {}
