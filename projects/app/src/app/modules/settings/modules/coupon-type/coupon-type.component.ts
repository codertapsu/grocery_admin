import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { filter, Observable, of, switchMap, take } from 'rxjs';

import { PaginationData } from '@core/models/pagination-data.model';
import { PaginationTable } from '@shared/abstractions/pagination-table.abstraction';
import { DialogService } from '@ui';

import { CouponType } from './coupon-type.model';
import { CouponTypeService } from './coupon-type.service';

@Component({
  templateUrl: './coupon-type.component.html',
  styleUrls: ['./coupon-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CouponTypeComponent extends PaginationTable<CouponType> implements OnInit {
  public constructor(
    private couponTypeService: CouponTypeService,
    private dialogService: DialogService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public add(): void {
    this.dialogService
      .prompt('Enter coupon type name', '')
      .pipe(
        take(1),
        filter(Boolean),
        switchMap(name => this.couponTypeService.create(name)),
      )
      .subscribe(couponType => {
        console.log(couponType);
        this.refresh();
      });
  }

  public edit(couponType: CouponType): void {
    // this.overlayService.show(CouponTypeEditComponent, { couponType });
  }

  public remove(couponType: CouponType): void {
    // this.overlayService.show(CouponTypeRemoveComponent, { couponType });
  }

  protected getSource(): Observable<PaginationData<CouponType>> {
    return of({
      data: [],
      meta: {
        page: 1,
        take: 10,
        itemCount: 0,
        pageCount: 0,
        hasPreviousPage: false,
        hasNextPage: false,
      },
    });
  }
}
