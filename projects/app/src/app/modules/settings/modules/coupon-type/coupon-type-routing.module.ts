import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CouponTypeComponent } from './coupon-type.component';

const routes: Routes = [{ path: '', component: CouponTypeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CouponTypeRoutingModule {}
