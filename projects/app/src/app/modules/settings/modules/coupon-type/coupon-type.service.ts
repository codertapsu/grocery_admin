import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { CouponTypesService as CouponTypeServiceBase } from '@core/services/coupon-types.service';

import { CouponType } from './coupon-type.model';

@Injectable()
export class CouponTypeService {
  public constructor(private couponTypeServiceBase: CouponTypeServiceBase) {}

  public create(name: string): Observable<CouponType> {
    return this.couponTypeServiceBase.create({ name });
  }

  public update(): void {
    // TODO
  }

  public delete(): void {
    // TODO
  }

  public getAll(): Observable<CouponType[]> {
    return this.couponTypeServiceBase.getAll();
  }
}
