import { CouponType as CouponTypeBase } from '@core/models/coupon-type.model';

export interface CouponType extends CouponTypeBase {}
