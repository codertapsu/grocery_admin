import { NgModule } from '@angular/core';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsService } from './settings.service';

@NgModule({
  imports: [SettingsRoutingModule],
  providers: [SettingsService],
})
export class SettingsModule {}
