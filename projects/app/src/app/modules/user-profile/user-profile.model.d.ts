import { User } from '@core/models/user.model';

export interface UserProfile extends User {}
