import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { PipesModule } from '@shared/pipes/pipes.module';

import { MaterialModule, MediaUploaderModule } from '@ui';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';
import { UserProfileService } from './user-profile.service';

@NgModule({
  declarations: [UserProfileComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PipesModule,
    MaterialModule,
    MediaUploaderModule,
    UserProfileRoutingModule,
  ],
  exports: [UserProfileComponent],
  bootstrap: [UserProfileComponent],
  providers: [UserProfileService],
})
export class UserProfileModule {}
