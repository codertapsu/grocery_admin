import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { OVERLAY_DATA, OverlayRef } from '@ui';
import { AuthService } from '@security';

import { UserProfile } from './user-profile.model';
import { BrowserWindow, WINDOW } from '@common';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfileComponent implements OnInit {
  public form: FormGroup;
  public twoFactorImg: string;

  public constructor(
    // private overlayRef: OverlayRef<UserProfile>,
    // @Inject(OVERLAY_DATA) private initialData: UserProfile,
    private _activatedRoute: ActivatedRoute,
    private _changeDetectorRef: ChangeDetectorRef,
    private _authService: AuthService,
    @Inject(WINDOW) private _window: BrowserWindow,
  ) {}

  public ngOnInit(): void {
    this._authService.generate2FA().subscribe(blob => {
      this.twoFactorImg = this._window.URL.createObjectURL(blob);
      this._changeDetectorRef.markForCheck();
    });
    console.log(this._activatedRoute.snapshot);
    this._authService.authUser$.subscribe(userProfile => {
      this.form = this.buildForm(userProfile);
    });

    // this.form = this.buildForm(this.initialData);
  }

  public submitForm(): void {
    // this.overlayRef.close(this.form.getRawValue());
  }

  private buildForm(userProfile: UserProfile): FormGroup {
    return new FormGroup({
      id: new FormControl(userProfile.id),
      name: new FormControl(userProfile.name),
      email: new FormControl(userProfile.email),
      phone: new FormControl(userProfile.phone),
      password: new FormControl(userProfile.password),
      isActive: new FormControl(userProfile.isActive),
      medias: new FormControl(userProfile.avatar ? [userProfile.avatar] : []),
    });
  }
}
