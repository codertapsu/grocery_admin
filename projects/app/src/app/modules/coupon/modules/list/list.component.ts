import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  LOCALE_ID,
  NgZone,
  OnInit,
  ViewContainerRef,
} from '@angular/core';

import { filter } from 'rxjs/operators';

import { BrowserWindow, MEDIA_DEVICES, WINDOW } from '@common';
import { Category } from '@core/models/category.model';
import { CameraService } from '@shared/modules/camera/camera.service';
import { LocaleId } from '@shared/providers/locale.provider';
import { LocaleService } from '@shared/services/locale.service';
import { OverlayService } from '@ui';

import { CouponService } from '../../coupon.service';
import { Coupon } from '../../models/coupon.model';
import { LazyFormComponent } from '../lazy-form';
import { PlatformService } from '@shared/services/platform.service';
import { PullToRefreshService } from '@shared/services/pull-to-refresh.service';
import { BroadCastService } from '@modules/private/services/broad-cast.service';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent implements OnInit, AfterViewChecked {
  public categories: Category[] = [];
  public coupons = new Array<Coupon>();
  public today = new Date();
  public counter = 0;
  public devices: any[];
  public pics: string[] = [];

  public constructor(
    private overlayService: OverlayService,
    private couponService: CouponService,
    private changeDetectorRef: ChangeDetectorRef,
    private broadCastService: BroadCastService,
    private localeService: LocaleService,
    private viewContainerRef: ViewContainerRef,
    private injector: Injector,
    private ngZone: NgZone,
    private cameraService: CameraService,
    private pullToRefreshService: PullToRefreshService,
    private platformService: PlatformService,
    @Inject(WINDOW) private window: BrowserWindow,
    @Inject(MEDIA_DEVICES) private mediaDevices: MediaDevices,
    @Inject(LOCALE_ID) private localeId: LocaleId,
  ) {}

  public ngOnInit() {
    this.couponService.getCategorySelections().subscribe({
      next: data => {
        this.categories = data;
      },
      error: (err: unknown) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });

    this.pullToRefreshService.onPulling$.subscribe(delta => {});
    this.pullToRefreshService.onPulled$.subscribe(delta => {});

    this.broadCastService.listen<number>('counter').subscribe(data => {
      this.counter = data;
      this.changeDetectorRef.detectChanges();
    });

    this.mediaDevices.enumerateDevices().then(devices => {
      // console.log(devices);
      this.devices = devices;
    });

    for (let i = 0; i < 50; i++) {
      // img.src = "https://cdn.liverez.com/3/Images/4x3.jpg";
      this.pics.push('https://loremflickr.com/320/240?random=' + i);
    }
    // console.log(this.pics);
  }

  public ngAfterViewChecked(): void {}

  public openCamera(): void {
    this.cameraService
      .open()
      .pipe(filter(Boolean))
      .subscribe(data => {
        // console.log(data);
      });
  }

  public increase(): void {
    this.counter++;
    this.broadCastService.send('counter', this.counter);
  }

  public testRaf(event: MouseEvent) {
    // let id = NaN;
    // if (id) {
    //   this.window.cancelAnimationFrame(id);
    //   console.log('cancelAnimationFrame');
    //   return;
    // }
    if (this.counter > 10) {
      // id = this.window.requestAnimationFrame(() => {
      //   queueMicrotask(this.testRaf.bind(this));
      //   return;
      // });
    }
    // console.log('publishMessage');
    // console.log(event.screenX);
  }

  public trackDataTable(index: number, item: Coupon): number {
    return item.id;
  }

  public changePage(event: unknown): void {
    // console.log(event);
  }

  public applyFilter(event: Event) {
    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  public createCoupon(): void {
    this.overlayService.open(LazyFormComponent).afterClosed$.subscribe(data => {
      // const coupon = data.result;
      // const category = this.categories.find(c => c.id === coupon.categoryId);
      // const mock = Array.from({ length: 1000 }).map(() => ({
      //   id: new Date().getTime(),
      //   name: coupon.name + '-' + Math.random().toFixed(2),
      //   createdAt: null,
      //   updatedAt: null,
      //   categoryId: coupon.categoryId,
      //   category,
      //   endDate: coupon.endDate,
      //   minimumOrder: coupon.minimumOrder,
      //   quantity: coupon.quantity,
      //   startDate: coupon.startDate,
      //   type: coupon.type,
      //   value: coupon.value,
      // }));

      // this.coupons.push(...mock);
      this.changeDetectorRef.detectChanges();
    });
  }

  public editCoupon(coupon: Coupon): void {
    // this.overlayService.open(LazyFormComponent, coupon);
  }

  public remove(): void {
    console.log('remove');
  }
}
