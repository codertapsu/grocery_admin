import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from '@shared/diretives/directives.module';
import { CameraModule } from '@shared/modules/camera/camera.module';
import { PipesModule } from '@shared/pipes/pipes.module';

import { ButtonModule, ClockModule, ConfirmationModule, MaterialModule, PaginatorModule } from '@ui';
import { LazyFormModule } from '../lazy-form';

import { ListComponent } from './list.component';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    PaginatorModule,
    MaterialModule,
    LazyFormModule,
    PipesModule,
    DirectivesModule,
    ConfirmationModule,
    CameraModule,
    ButtonModule,
    ClockModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListComponent,
      },
    ]),
  ],
})
export class ListModule {}
