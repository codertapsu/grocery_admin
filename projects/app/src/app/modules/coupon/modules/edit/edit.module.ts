import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EditComponent } from './edit.component';

@NgModule({
  declarations: [EditComponent],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: EditComponent,
      },
    ]),
  ],
})
export class EditModule {}
