import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { combineLatest, map, Observable, startWith } from 'rxjs';

import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { CouponService } from '../../coupon.service';
import { OverlayRef, OVERLAY_DATA } from '@ui';
import { Coupon } from '../../models/coupon.model';

@UntilDestroy()
@Component({
  templateUrl: './lazy-form.component.html',
  styleUrls: ['./lazy-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LazyFormComponent implements OnInit {
  public readonly couponTypes$ = this.couponService.getCouponTypes();
  public readonly categories$ = this.couponService.getCategorySelections();

  public form: UntypedFormGroup;

  public constructor(
    private couponService: CouponService,
    private overlayRef: OverlayRef<Coupon>,
    @Inject(OVERLAY_DATA) private passedData: unknown,
  ) {}

  public ngOnInit(): void {
    console.log('passedData', this.passedData);
    
    this.form = this.buildForm();
  }

  public submitForm(): void {
    this.overlayRef.close(this.form.getRawValue());
  }

  private buildForm(): UntypedFormGroup {
    return new UntypedFormGroup({
      id: new UntypedFormControl(),
      name: new UntypedFormControl(),
      type: new UntypedFormControl(),
      value: new UntypedFormControl([]),
      categoryId: new UntypedFormControl(),
      startDate: new UntypedFormControl(),
      endDate: new UntypedFormControl(),
      quantity: new UntypedFormControl(),
      minimumOrderValue: new UntypedFormControl(),
      currencyUnit: new UntypedFormControl(),
    });
  }
}
