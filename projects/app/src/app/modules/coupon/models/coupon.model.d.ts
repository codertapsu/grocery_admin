import { Category } from '@core/models/category.model';
import { Coupon as CouponBase } from '@core/models/coupon.model';

export interface Coupon extends CouponBase {
  category: Category;
}
