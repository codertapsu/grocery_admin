import { NgModule } from '@angular/core';

import { CouponRoutingModule } from './coupon-routing.module';
import { CouponService } from './coupon.service';

@NgModule({
  imports: [CouponRoutingModule],
  providers: [CouponService],
})
export class CouponModule {}
