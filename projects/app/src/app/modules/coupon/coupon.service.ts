import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Category } from '@core/models/category.model';
import { CouponType } from '@core/models/coupon-type.model';
import { CategoriesService } from '@core/services/categories.service';
import { CouponsService as CoreCouponService } from '@core/services/coupons.service';
import { WellKnownService } from '@core/services/well-known.service';

@Injectable()
export class CouponService {
  public constructor(
    private coreCouponService: CoreCouponService,
    private categoryService: CategoriesService,
    private wellKnownService: WellKnownService,
  ) {}

  public getCouponTypes(): Observable<CouponType[]> {
    return this.wellKnownService.getCouponTypes();
  }

  public getCategorySelections(): Observable<Category[]> {
    return this.categoryService.getSelections();
  }
}
