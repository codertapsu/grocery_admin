import { NgModule } from '@angular/core';

import { CustomerServiceRoutingModule } from './customer-service-routing.module';
import { CustomerServiceService } from './customer-service.service';

@NgModule({
  imports: [CustomerServiceRoutingModule],
  providers: [CustomerServiceService],
})
export class CustomerServiceModule {}
