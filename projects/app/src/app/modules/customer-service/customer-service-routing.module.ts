import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'common-asked-questions',
  },
  {
    path: 'common-asked-questions',
    loadChildren: () =>
      import('./modules/common-asked-questions/common-asked-questions.module').then(m => m.CommonAskedQuestionsModule),
  },
  {
    path: 'inquiry',
    loadChildren: () => import('./modules/inquiry/inquiry.module').then(m => m.InquiryModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerServiceRoutingModule {}
