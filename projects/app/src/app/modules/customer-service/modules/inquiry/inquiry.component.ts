import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InquiryComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
