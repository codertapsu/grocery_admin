import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CommonAskedQuestionsComponent } from './common-asked-questions.component';

const routes: Routes = [
  {
    path: '',
    component: CommonAskedQuestionsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommonAskedQuestionsRoutingModule {}
