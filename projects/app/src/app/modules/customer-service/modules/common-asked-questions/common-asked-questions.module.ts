import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CommonAskedQuestionsRoutingModule } from './common-asked-questions-routing.module';
import { CommonAskedQuestionsComponent } from './common-asked-questions.component';

@NgModule({
  declarations: [CommonAskedQuestionsComponent],
  imports: [CommonModule, CommonAskedQuestionsRoutingModule],
})
export class CommonAskedQuestionsModule {}
