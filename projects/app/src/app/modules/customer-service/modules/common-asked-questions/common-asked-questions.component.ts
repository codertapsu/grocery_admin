import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  templateUrl: './common-asked-questions.component.html',
  styleUrls: ['./common-asked-questions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommonAskedQuestionsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
