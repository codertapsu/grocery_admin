export interface Stat {
  title: string;
  stat: number;
  percentage: number;
  color: string;
}
