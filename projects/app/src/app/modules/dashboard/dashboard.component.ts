import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Order } from '@core/models/order.model';

import { Observable } from 'rxjs';

import { DashboardService } from './dashboard.service';
import { Stat } from './models/stat.model';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {
  public readonly displayedOrderColumns: (keyof Order | 'action')[] = ['id', 'recipientName', 'updatedAt', 'action'];

  public latestOrders$: Observable<Order[]>;
  public stats$: Observable<Array<Stat>>;
  public earningData$: Observable<any>;

  public constructor(private dashboardService: DashboardService) {}

  public ngOnInit(): void {
    this.stats$ = this.dashboardService.getStats();
    this.earningData$ = this.dashboardService.getDataForACountry('IN');
    this.latestOrders$ = this.dashboardService.getSomeLatestOrder();
  }
}
