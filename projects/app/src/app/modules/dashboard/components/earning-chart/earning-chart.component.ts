import { Component, OnInit, ChangeDetectionStrategy, ViewChild, Input } from '@angular/core';
import { ChartComponent } from 'ng-apexcharts';

@Component({
  selector: 'app-earning-chart',
  templateUrl: './earning-chart.component.html',
  styleUrls: ['./earning-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EarningChartComponent implements OnInit {
  @ViewChild('chart') chart: ChartComponent;

  public chartOptions: any;
  @Input() public countryData: any[];
  constructor() {}

  ngOnInit() {
    this.chartOptions = {
      series: [
        {
          name: 'Confirmed',
          data: this.generateDayWiseTimeSeries(this.countryData, 'Confirmed'),
        },
        {
          name: 'Recovered',
          data: this.generateDayWiseTimeSeries(this.countryData, 'Recovered'),
        },
        {
          name: 'Deaths',
          data: this.generateDayWiseTimeSeries(this.countryData, 'Deaths'),
        },
      ],
      chart: {
        type: 'area',
        height: 350,
        stacked: true,
        events: {
          selection: function (chart: any, e: any) {
            console.log(new Date(e.xaxis.min));
          },
        },
      },
      colors: ['#FD4E71', '#6DD400', '#9475FF'],
      dataLabels: {
        enabled: false,
      },
      fill: {
        type: 'gradient',
        gradient: {
          opacityFrom: 0,
          opacityTo: 0.1,
        },
      },
      legend: {
        position: 'top',
        horizontalAlign: 'left',
      },
      xaxis: {
        type: 'datetime',
      },
    };
  }

  private generateDayWiseTimeSeries(items: any[], key: any) {
    const stats = items.map(item => [new Date(item.Date).getTime(), item[key]]);
    if (['Recovered', 'Deaths'].indexOf(key) > -1) {
      // console.log(key, stats);
    }
    return stats.splice(0, 50);
  }
}
