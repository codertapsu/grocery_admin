import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Stat } from '../../models/stat.model';

@Component({
  selector: 'app-stat-card',
  templateUrl: './stat-card.component.html',
  styleUrls: ['./stat-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatCardComponent {
  @Input() public stat: Stat;
}
