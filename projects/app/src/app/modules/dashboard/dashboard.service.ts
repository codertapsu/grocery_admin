import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from '@core/models/order.model';
import { OrdersService } from '@core/services/orders.service';
import { Observable, of } from 'rxjs';
import { Stat } from './models/stat.model';

@Injectable()
export class DashboardService {
  public constructor(private httpClient: HttpClient, private orderService: OrdersService) {}

  public getStats(): Observable<Array<Stat>> {
    return of([
      {
        title: 'Total Products',
        stat: 34567,
        percentage: 10.23,
        color: '#F9345E',
      },
      {
        title: 'Total Products',
        stat: 34567,
        percentage: 10.23,
        color: '#F9345E',
      },
      {
        title: 'Total Products',
        stat: 34567,
        percentage: 10.23,
        color: '#F9345E',
      },
      {
        title: 'Total Products',
        stat: 34567,
        percentage: 10.23,
        color: '#F9345E',
      },
    ]);
  }

  public getDataForACountry(countryCode: string) {
    return this.httpClient.get(`https://api.covid19api.com/total/dayone/country/${countryCode}`);
  }

  public getSomeLatestOrder(): Observable<Order[]> {
    return of([]);
  }
}
