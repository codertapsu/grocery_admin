import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '@ui';

import { NgApexchartsModule } from 'ng-apexcharts';

import { EarningChartComponent } from './components/earning-chart/earning-chart.component';
import { StatCardComponent } from './components/stat-card/stat-card.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';

@NgModule({
  declarations: [DashboardComponent, StatCardComponent, EarningChartComponent],
  imports: [CommonModule, NgApexchartsModule, MaterialModule, DashboardRoutingModule],
  providers: [DashboardService],
})
export class DashboardModule {}
