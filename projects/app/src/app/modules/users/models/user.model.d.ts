import { User as UserBase } from '@core/models/user.model';

export interface User extends UserBase {}
