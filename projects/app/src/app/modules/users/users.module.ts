import { NgModule } from '@angular/core';

import { UsersRoutingModule } from './users-routing.module';
import { UserService } from './users.service';

@NgModule({
  imports: [UsersRoutingModule],
  providers: [UserService],
})
export class UsersModule {}
