import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { SortDirection } from '@core/constants/sort-direction.constant';
import { PaginationData } from '@core/models/pagination-data.model';
import { UsersService as UsersServiceBase } from '@core/services/users.service';

import { User } from './models/user.model';

@Injectable()
export class UserService {
  public constructor(private _usersServiceBase: UsersServiceBase) {}

  public getAll(): Observable<User[]> {
    return this._usersServiceBase.getAll();
  }

  public getFilteredPaginationData(
    page: number,
    take: number,
    search: string,
    sortBy: string,
    sortDirection: SortDirection,
  ): Observable<PaginationData<User>> {
    return this._usersServiceBase.getFilteredPaginationData({
      page,
      take,
      search,
      sortBy,
      sortDirection,
    });
  }

  public create(data: User): Observable<User> {
    const formData = new FormData();
    formData.append('name', data.name);
    return this._usersServiceBase.createFormData(formData);
  }

  public update(data: User): Observable<User> {
    const formData = new FormData();
    formData.append('name', data.name);
    return this._usersServiceBase.updateFormData(data.id, formData);
  }

  public remove(data: User): Observable<unknown> {
    return this._usersServiceBase.remove(data.id);
  }
}
