import { ChangeDetectionStrategy, ChangeDetectorRef, Component, createNgModuleRef, Injector } from '@angular/core';

import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, startWith, switchMap } from 'rxjs/operators';

import { ANIMATIONS } from '@common';
import { SortDirection } from '@core/constants/sort-direction.constant';
import { PaginationData } from '@core/models/pagination-data.model';
import { PaginationTable } from '@shared/abstractions';
import { NotificationService, OverlayService } from '@ui';

import { User } from '../../models/user.model';
import { UserService } from '../../users.service';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [ANIMATIONS],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<User> {
  public readonly search$ = new Subject<string>();
  public readonly sort$ = new BehaviorSubject<{
    by: string;
    direction: SortDirection;
  }>({
    by: 'createdAt',
    direction: SortDirection.DESC,
  });

  public constructor(
    private _overlayService: OverlayService,
    private _injector: Injector,
    private _userService: UserService,
    private _notificationService: NotificationService,
    private _changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public createOrUpdate(data: User = {} as User): void {
    import('../form')
      .then(({ FormComponent, FormModule }) => {
        const ngModuleRef = createNgModuleRef(
          FormModule,
          Injector.create({
            parent: this._injector,
            providers: [
              {
                provide: UserService,
                useValue: this._userService,
              },
            ],
          }),
        );
        this._overlayService
          .open<User>(FormComponent, {
            data,
            ngModuleRef,
            disableClose: false,
          })
          .afterClosed$.pipe(filter(closedResult => !!closedResult.result))
          .subscribe({
            next: () => {
              this.refresh();
              this._notificationService.success('User created successfully');
            },
            error: () => {
              this._notificationService.error('Error while creating user');
            },
          });
      })
      .catch(error => {
        this._notificationService.error(error.message || 'Error while creating user');
      });
  }

  public remove(item: User): void {
    this._userService.remove(item).subscribe({
      next: () => {
        this.refresh();
      },
      error: () => {
        this._notificationService.error('Error while removing User');
      },
    });
  }

  protected getSource(): Observable<PaginationData<User>> {
    return combineLatest([
      this.pagination$,
      this.search$.pipe(debounceTime(800), distinctUntilChanged(), startWith('')),
      this.sort$,
    ]).pipe(
      switchMap(([pagination, search, sort]) =>
        this._userService.getFilteredPaginationData(
          pagination.index,
          pagination.size,
          search,
          sort.by,
          sort.direction,
        ),
      ),
    );
  }
}
