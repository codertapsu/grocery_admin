import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MediaViewerComponent } from '@shared/components/media-viewer/media-viewer.component';
import { DirectivesModule } from '@shared/diretives/directives.module';
import { PipesModule } from '@shared/pipes/pipes.module';
import { ConfirmationModule, MaterialModule, PaginatorModule, TooltipModule } from '@ui';

import { ListComponent } from './list.component';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    ConfirmationModule,
    DirectivesModule,
    FormsModule,
    MaterialModule,
    MediaViewerComponent,
    PaginatorModule,
    PipesModule,
    TooltipModule,
    RouterModule.forChild([
      {
        path: '',
        component: ListComponent,
      },
    ]),
  ],
})
export class ListModule {}
