import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

import { TypedFormGroup } from '@common';
import { OVERLAY_DATA, OverlayRef } from '@ui';

import { User } from '../../models/user.model';
import { UserService } from '../../users.service';

@Component({
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit {
  public form: TypedFormGroup<User>;

  public constructor(
    private _userService: UserService,
    private _overlayRef: OverlayRef<User>,
    @Inject(OVERLAY_DATA) private _initialData: User,
  ) {}

  public ngOnInit(): void {
    this.form = this.buildForm(this._initialData);
  }

  public submit(): void {
    this.form.markAsSubmitted();
    if (this.form.valid) {
      this._userService.create(this.form.getRawValue()).subscribe(() => {
        this._overlayRef.close();
      });
    }
  }

  private buildForm(user: User): TypedFormGroup<User> {
    return new TypedFormGroup<User>({
      id: new UntypedFormControl(user.id),
      name: new UntypedFormControl(user.name),
      firstName: new UntypedFormControl(user.firstName),
      lastName: new UntypedFormControl(user.lastName),
      email: new UntypedFormControl(user.email),
      phone: new UntypedFormControl(user.phone),
      password: new UntypedFormControl(user.password),
      avatar: new UntypedFormControl(user.avatar),
      isActive: new UntypedFormControl(user.isActive),
    });
  }
}
