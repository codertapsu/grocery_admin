import { NgModule } from '@angular/core';

import { BannersManagementRoutingModule } from './banners-management-routing.module';
import { BannersManagementService } from './banners-management.service';

@NgModule({
  imports: [BannersManagementRoutingModule],
  providers: [BannersManagementService],
})
export class BannersManagementModule {}
