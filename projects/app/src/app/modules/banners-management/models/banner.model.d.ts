import { Banner as BannerBase } from '@core/models/banner.model';

export interface Banner extends BannerBase {}
