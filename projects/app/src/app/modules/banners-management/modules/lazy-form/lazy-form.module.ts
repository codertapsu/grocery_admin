import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule, MediaUploaderModule } from '@ui';

import { LazyFormComponent } from './lazy-form.component';

@NgModule({
  declarations: [LazyFormComponent],
  imports: [CommonModule, ReactiveFormsModule, MaterialModule, MediaUploaderModule],
  bootstrap: [LazyFormComponent],
})
export class LazyFormModule {}
