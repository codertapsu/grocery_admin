import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { OverlayRef, OVERLAY_DATA } from '@ui';
import { Banner } from '../../models/banner.model';

@Component({
  templateUrl: './lazy-form.component.html',
  styleUrls: ['./lazy-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LazyFormComponent implements OnInit {
  public form: UntypedFormGroup;

  public constructor(private overlayRef: OverlayRef<Banner>, @Inject(OVERLAY_DATA) private initialData: Banner) {}

  public ngOnInit(): void {
    this.form = this.buildForm(this.initialData);
  }

  public submitForm(): void {
    this.overlayRef.close(this.form.getRawValue());
  }

  private buildForm(banner: Banner): UntypedFormGroup {
    return new UntypedFormGroup({
      id: new UntypedFormControl(banner.id),
      name: new UntypedFormControl(banner.name),
      medias: new UntypedFormControl(banner.medias || []),
    });
  }
}
