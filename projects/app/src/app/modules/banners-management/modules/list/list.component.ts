import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewContainerRef,
} from '@angular/core';

import { Observable, of } from 'rxjs';

import { PaginationTable } from '@shared/abstractions';
import { OverlayService } from '@ui';

import { BannersManagementService } from '../../banners-management.service';
import { Banner } from '../../models/banner.model';
import { LazyFormComponent } from '../lazy-form';
import { PaginationData } from '@core/models/pagination-data.model';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent extends PaginationTable<Banner> implements OnInit {
  public constructor(
    private overlayService: OverlayService,
    private viewContainerRef: ViewContainerRef,
    private bannersManagementService: BannersManagementService,
    private changeDetectorRef: ChangeDetectorRef,
  ) {
    super();
  }

  public override ngOnInit(): void {
    super.ngOnInit();
  }

  public applyFilter(event: Event) {
    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  public createCoupon(): void {
    this.overlayService.open(LazyFormComponent).afterClosed$.subscribe(data => {
      this.changeDetectorRef.detectChanges();
    });
  }

  public editCoupon(item: Banner): void {
    // this.overlayService.open(LazyFormComponent, item);
  }

  protected getSource(): Observable<PaginationData<Banner>> {
    return of({
      data: [],
      meta: {
        page: 1,
        take: 10,
        itemCount: 0,
        pageCount: 0,
        hasPreviousPage: false,
        hasNextPage: false,
      },
    });
  }
}
