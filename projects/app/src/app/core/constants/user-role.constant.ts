export enum UserRole {
  SystemAdmin = 'SystemAdmin',
  SystemModerator = 'SystemModerator',
  SystemUser = 'SystemUser',
  StoreAdmin = 'StoreAdmin',
  StoreModerator = 'StoreModerator',
  StoreUser = 'StoreUser',
  Shipper = 'Shipper',
}
