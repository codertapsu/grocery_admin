import { PaymentStatus } from '../models/payment-status.model';

export const PAYMENT_STATUSES: PaymentStatus[] = [
  {
    id: 1,
    name: 'Waiting for Customer',
    uuid: null,
    createdAt: '2022-04-29T13:47:09.280Z',
    updatedAt: '2022-04-29T13:47:09.280Z',
  },
  {
    id: 2,
    name: 'Not Paid',
    uuid: null,
    createdAt: '2022-04-29T13:47:09.280Z',
    updatedAt: '2022-04-29T13:47:09.280Z',
  },
  {
    id: 3,
    name: 'Paid',
    uuid: null,
    createdAt: '2022-04-29T13:47:09.280Z',
    updatedAt: '2022-04-29T13:47:09.280Z',
  },
];
