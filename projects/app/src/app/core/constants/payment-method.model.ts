export enum PaymentMethod {
  Cash,
  CreditCard,
  DebitCard,
  Check,
  Paypal,
  Bitcoin,
  Applepay,
  Googlepay,
  Stripe,
  Other,
}
