import { OrderStatus } from '../models/order-status.model';

export const ORDER_STATUSES: OrderStatus[] = [
  {
    id: 1,
    name: 'Order Received',
    uuid: null,
    createdAt: '2022-04-29T13:47:09.280Z',
    updatedAt: '2022-04-29T13:47:09.280Z',
  },
  {
    id: 2,
    name: 'Preparing',
    uuid: null,
    createdAt: '2022-04-29T13:47:09.280Z',
    updatedAt: '2022-04-29T13:47:09.280Z',
  },
  {
    id: 3,
    name: 'Ready',
    uuid: null,
    createdAt: '2022-04-29T13:47:09.280Z',
    updatedAt: '2022-04-29T13:47:09.280Z',
  },
  {
    id: 4,
    name: 'On the Way',
    uuid: null,
    createdAt: '2022-04-29T13:47:09.280Z',
    updatedAt: '2022-04-29T13:47:09.280Z',
  },
  {
    id: 5,
    name: 'Delivered',
    uuid: null,
    createdAt: '2022-04-29T13:47:09.280Z',
    updatedAt: '2022-04-29T13:47:09.280Z',
  },
];
