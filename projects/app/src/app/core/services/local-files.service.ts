import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { API_END_POINT } from '@common';

import { LocalFile } from '../models/local-file.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class LocalFilesService extends BaseService<LocalFile> {
  public constructor(protected override httpClient: HttpClient, @Inject(API_END_POINT) private apiEndpoint: string) {
    super(httpClient);
  }

  protected defineRepositoryEndpoint(): string {
    return `${this.apiEndpoint}/api/local-files`;
  }
}
