import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { ORDER_STATUSES } from '../constants/order-statuses.const';
import { OrderStatus } from '../models/order-status.model';
import { Order } from '../models/order.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class OrdersService extends BaseService<Order> {
  public constructor(protected override httpClient: HttpClient) {
    super(httpClient);
  }

  public getOrderStatuses(): Observable<OrderStatus[]> {
    return of(ORDER_STATUSES);
  }

  protected defineRepositoryEndpoint(): string {
    return `/api/orders`;
  }
}
