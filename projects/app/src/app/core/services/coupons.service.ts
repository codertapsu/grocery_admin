import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Coupon } from '../models/coupon.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class CouponsService extends BaseService<Coupon> {
  public constructor(protected override httpClient: HttpClient) {
    super(httpClient);
  }

  protected defineRepositoryEndpoint(): string {
    return `/api/coupons`;
  }
}
