import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { GenericEntity } from '@common';

import { PaginationData } from '../models/pagination-data.model';
import { PaginationOptions } from '../models/pagination-options.model';

export abstract class BaseService<T extends GenericEntity> {
  get repositoryEndpoint(): string {
    return this.defineRepositoryEndpoint();
  }

  protected constructor(protected httpClient: HttpClient) {}

  public createFormData<K = T>(formData: FormData): Observable<K> {
    return this.httpClient.post<K>(this.repositoryEndpoint, formData);
  }

  public updateFormData<K = T>(id: T['id'], formData: FormData): Observable<K> {
    return this.httpClient.put<K>(`${this.repositoryEndpoint}/${id}`, formData);
  }

  public create<K = T>(item: K): Observable<K> {
    return this.httpClient.post<K>(this.repositoryEndpoint, item);
  }

  public update<K = T>(item: K): Observable<K> {
    return this.httpClient.put<K>(this.repositoryEndpoint, item);
  }

  public remove<K = T>(id: T['id']): Observable<K> {
    return this.httpClient.delete<K>(this.repositoryEndpoint + '/' + id);
  }

  public getById(id: number): Observable<T> {
    return this.httpClient.get<T>(this.repositoryEndpoint + '/' + id);
  }

  public getFilteredPaginationData(paginationOptions: PaginationOptions): Observable<PaginationData<T>> {
    return this.httpClient.get<PaginationData<T>>(this.repositoryEndpoint + '/query', {
      params: {
        search: paginationOptions.search,
        sortBy: paginationOptions.sortBy,
        sortDirection: paginationOptions.sortDirection,
        take: paginationOptions.take.toString(),
        page: paginationOptions.page.toString(),
      },
    });
  }

  public getAll(): Observable<T[]> {
    return this.httpClient.get<T[]>(this.repositoryEndpoint);
  }

  protected abstract defineRepositoryEndpoint(): string;
}
