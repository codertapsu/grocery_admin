import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { API_END_POINT } from '@common';
import { Observable, of } from 'rxjs';

import { User } from '../models/user.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class UsersService extends BaseService<User> {
  public constructor(protected override httpClient: HttpClient, @Inject(API_END_POINT) private apiEndpoint: string) {
    super(httpClient);
  }

  public getSelections(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${this.repositoryEndpoint}/selections`);
  }

  protected defineRepositoryEndpoint(): string {
    return `${this.apiEndpoint}/api/users`;
  }
}
