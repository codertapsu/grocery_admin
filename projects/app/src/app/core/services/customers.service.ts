import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map, Observable, of } from 'rxjs';

import { Customer } from '../models/customer.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class CustomersService extends BaseService<Customer> {
  public constructor(protected override httpClient: HttpClient) {
    super(httpClient);
  }

  public getSelections(): Observable<Customer[]> {
    return this.httpClient
      .get<Customer[]>(`${this.repositoryEndpoint}/selections`)
      .pipe(map(customers => customers.map(item => ({ ...item, fullName: `${item.firstName} ${item.lastName}` }))));
  }

  public getByEmail(email: string): Observable<Customer> {
    return this.httpClient.get<Customer>(`${this.repositoryEndpoint}/${email}`);
  }

  protected defineRepositoryEndpoint(): string {
    return `/api/customers`;
  }
}
