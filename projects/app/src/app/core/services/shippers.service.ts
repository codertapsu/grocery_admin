import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { API_END_POINT } from '@common';

import { Shipper } from '../models/shipper.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class ShippersService extends BaseService<Shipper> {
  public constructor(protected override httpClient: HttpClient, @Inject(API_END_POINT) private apiEndpoint: string) {
    super(httpClient);
  }

  public getSelections(): Observable<Shipper[]> {
    return this.httpClient.get<Shipper[]>(`${this.repositoryEndpoint}/selections`);
  }

  protected defineRepositoryEndpoint(): string {
    return `${this.apiEndpoint}/api/shippers`;
  }
}
