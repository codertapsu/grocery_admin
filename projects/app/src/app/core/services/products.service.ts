import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { API_END_POINT } from '@common';

import { Product } from '../models/product.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class ProductsService extends BaseService<Product> {
  public constructor(protected override httpClient: HttpClient, @Inject(API_END_POINT) private apiEndpoint: string) {
    super(httpClient);
  }

  public insert(formData: FormData): Observable<Product> {
    return this.httpClient.post<Product>(this.repositoryEndpoint, formData);
  }

  protected defineRepositoryEndpoint(): string {
    return `${this.apiEndpoint}/api/products`;
  }
}
