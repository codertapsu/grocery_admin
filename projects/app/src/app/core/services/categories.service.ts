import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { API_END_POINT } from '@common';
import { Observable, of } from 'rxjs';

import { Category } from '../models/category.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class CategoriesService extends BaseService<Category> {
  public constructor(protected override httpClient: HttpClient, @Inject(API_END_POINT) private apiEndpoint: string) {
    super(httpClient);
  }

  public getSelections(): Observable<Category[]> {
    return this.httpClient.get<Category[]>(`${this.repositoryEndpoint}/selections`);
  }

  protected defineRepositoryEndpoint(): string {
    return `${this.apiEndpoint}/api/categories`;
  }
}
