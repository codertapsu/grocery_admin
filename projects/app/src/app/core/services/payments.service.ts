import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PAYMENT_STATUSES } from '../constants/payment-statuses.const';
import { PaymentStatus } from '../models/payment-status.model';

import { Payment } from '../models/payment.model';
import { BaseService } from './base-service.abstraction';

@Injectable()
export class PaymentsService extends BaseService<Payment> {
  public constructor(protected override httpClient: HttpClient) {
    super(httpClient);
  }

  public getPaymentStatuses(): Observable<PaymentStatus[]> {
    return of(PAYMENT_STATUSES);
  }

  protected defineRepositoryEndpoint(): string {
    return `/api/payments`;
  }
}
