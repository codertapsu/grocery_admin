import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { API_END_POINT } from '@common';

import { Observable } from 'rxjs';

import { Currency } from '../models/currency.model';
import { CouponType } from '../models/coupon-type.model';

@Injectable()
export class WellKnownService {
  private readonly _currencyApi: string;
  private readonly _couponTypeApi: string;

  public constructor(private httpClient: HttpClient, @Inject(API_END_POINT) private apiEndpoint: string) {
    this._currencyApi = `${this.apiEndpoint}/api/well-known/currencies`;
    this._couponTypeApi = `${this.apiEndpoint}/api/well-known/coupon-types`;
  }

  public getCurrencies(): Observable<Currency[]> {
    return this.httpClient.get<Currency[]>(this._currencyApi);
  }

  public getCouponTypes(): Observable<CouponType[]> {
    return this.httpClient.get<CouponType[]>(this._couponTypeApi);
  }
}
