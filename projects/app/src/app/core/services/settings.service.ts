import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable()
export class SettingsService {
  get repositoryEndpoint(): string {
    return 'settings';
  }

  public constructor(private httpClient: HttpClient) {}

  public getPaymentSettings(): Observable<unknown> {
    return this.httpClient.get(`${this.repositoryEndpoint}/payment`);
  }
}
