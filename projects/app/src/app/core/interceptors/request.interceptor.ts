import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

import { AuthService } from '@security';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  private readonly _accessTokenError$ = new BehaviorSubject<boolean>(false);

  public constructor(private authService: AuthService) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let clonedRequest = request.clone({
      withCredentials: true,
    });
    return next.handle(clonedRequest).pipe(
      catchError((httpError: HttpErrorResponse) => {
        if (httpError.error instanceof ErrorEvent) {
          return throwError(() => httpError);
        }
        if (httpError.status === 401) {
          if (!this._accessTokenError$.getValue()) {
            this._accessTokenError$.next(true);
            /**
             * Call API and get a New Access Token
             */
            return this.authService.refreshToken().pipe(
              switchMap(() => {
                this._accessTokenError$.next(false);
                return next.handle(clonedRequest);
              }),
              catchError((err: HttpErrorResponse) => {
                if (err.status === 403) {
                  /**
                   * Logout if 403 response - Refresh Token invalid
                   */
                  this.authService.logout();
                  // location.reload();
                }
                return throwError(() => err);
              }),
            );
          } else {
            /**
             * If it's not the first error, it has to wait until get the access/refresh token
             */
            return this.waitNewTokens().pipe(
              switchMap(() => {
                /**
                 * Clone the request with new Access Token
                 */
                return next.handle(clonedRequest);
              }),
            );
          }
        }
        return throwError(() => httpError);
      }),
    );
  }

  private waitNewTokens(): Observable<void> {
    const subject$ = new Subject<void>();
    const waitToken$ = this._accessTokenError$.subscribe(error => {
      if (!error) {
        subject$.next();
        waitToken$.unsubscribe();
      }
    });
    return subject$.asObservable();
  }
}
