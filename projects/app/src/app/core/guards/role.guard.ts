import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, UrlTree } from '@angular/router';

import { Observable } from 'rxjs';

import { AuthService } from '@security';

@Injectable()
export class RoleGuard implements CanLoad {
  public constructor(private authService: AuthService) {}

  public canLoad(
    route: Route,
    segments: UrlSegment[],
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const roles: string[] = route.data['roles'] || [];
    // if (!roles.length || roles.includes(ROLES.ALL) || roles.some(r => this.authService.userRoles.includes(r))) {
    //   return true;
    // }

    return true;
  }
}
