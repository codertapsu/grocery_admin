import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

import { Observable } from 'rxjs';

import { AuthService } from '@security';

@Injectable()
export class AuthGuard implements CanActivate {
  public constructor(private _router: Router, private _authService: AuthService) {}

  public canActivate(
    _: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const authUser = this._authService.snapshot.user;
    if (authUser?.id) {
      return true;
    }
    return this._router.createUrlTree(['/guest'], { queryParams: { returnUrl: state.url } });
  }
}
