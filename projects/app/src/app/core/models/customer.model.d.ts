import { GenericEntity } from '@common';

export interface Customer extends GenericEntity<number> {
  phone: string;
  email: string;
  address: string;
  city: string;
  country: string;
  zip: string;
  firstName: string;
  lastName: string;
  fullName: string;
  isVerified: boolean;
}
