import { GenericEntity } from '@common';

export interface PaymentStatus extends GenericEntity<number> {}
