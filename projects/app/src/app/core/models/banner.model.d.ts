import { GenericEntity } from '@common';

import { Media } from './media.model';

export interface Banner extends GenericEntity<number> {
  medias: Media[];
}
