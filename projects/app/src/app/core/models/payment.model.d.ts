import { GenericEntity } from '../../../../../common';
import { PaymentStatus } from './payment-status.model';
import { PaymentMethod } from '../constants/payment-method.model';

export interface Payment extends GenericEntity<number> {
  description: string;
  method: PaymentMethod;
  price: number;
  priceUnit: string;
  status: PaymentStatus;
  userId: number;
}
