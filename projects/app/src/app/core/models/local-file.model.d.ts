import { GenericEntity } from '@common';

export interface LocalFile extends GenericEntity<number> {}
