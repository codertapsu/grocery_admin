import { GenericEntity } from '@common';

export interface OrderStatus extends GenericEntity<number> {}
