import { PaginationMeta } from './pagination-meta.model';

export interface PaginationData<T> {
  data: T[];
  meta: PaginationMeta;
}
