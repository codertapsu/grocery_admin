import { GenericEntity } from '@common';
import { CouponType } from './coupon-type.model';

export interface Coupon extends GenericEntity<number> {
  categoryId: number;
  endDate: string;
  minimumOrder: number;
  quantity: number;
  startDate: string;
  type: CouponType;
  value: number;
}
