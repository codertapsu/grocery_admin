import { GenericEntity } from '@common';

export interface Media extends GenericEntity<number> {
  mimetype: string;
  path: string;
  size: number;
  file?: Blob | MediaSource;
}
