import { GenericEntity } from '@common';

import { Category } from './category.model';
import { Media } from './media.model';

export interface Product extends GenericEntity<number> {
  capacity: string;
  categories: Category[];
  categoryIds: Category['id'][];
  currencyUnit: string;
  deliverable: boolean;
  description: string;
  discountPrice: number;
  currencyId: number;
  isFeature: boolean;
  isMostPopular: boolean;
  isTopDeal: boolean;
  marketId: number;
  medias: Media[];
  packageItemsCount: number;
  price: number;
  stock: number;
  quantity: number;
  unit: string;
}
