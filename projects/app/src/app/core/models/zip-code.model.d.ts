import { GenericEntity } from '@common';

export interface ZipCode extends GenericEntity<number> {
  country: string;
  state: string;
  city: string;
}
