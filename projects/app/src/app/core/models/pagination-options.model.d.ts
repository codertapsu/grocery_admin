import { SortDirection } from '../constants/sort-direction.constant';

export interface PaginationOptions {
  search?: string;
  sortBy?: string;
  sortDirection?: SortDirection;
  page?: number;
  take?: number;
}
