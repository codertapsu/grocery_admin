import { GenericEntity } from '@common';
import { PaymentMethod } from '../constants/payment-method.model';
import { Shipper } from './shipper.model';

export interface Order extends GenericEntity<number> {
  code: string;
  couponCode: string;
  currencyUnit: string;
  deliveryAddressId: number;
  deliveryFee: number;
  deliveryInstruction: string;
  shipperId: number;
  shipper: Shipper;
  finalAmount: number;
  orderStatusId: number;
  paymentId: number;
  paymentMethod: PaymentMethod;
  promotionalDiscount: number;
  recipientId: number;
  recipientName: string;
  recipientPhone: string;
  subtotal: number;
  tax: number;
}
