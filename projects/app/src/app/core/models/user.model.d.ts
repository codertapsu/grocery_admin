import { GenericEntity } from '@common';

import { Media } from './media.model';

export interface User extends GenericEntity<number> {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  password?: string;
  avatar: Media;
  isActive: boolean;
}
