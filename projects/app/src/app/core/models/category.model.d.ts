import { GenericEntity } from '@common';

import { Media } from './media.model';

export interface Category extends GenericEntity<number> {
  isFeature: boolean;
  medias: Media[];
  orderCounter: number;
}
