import { User } from './user.model';

export interface Shipper extends User {
  totalOrders: number;
}
