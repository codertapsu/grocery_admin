import { GenericEntity } from '@common';

export interface Currency extends GenericEntity<number> {
  code: string;
  decimalDigits: number;
  rounding: number;
  symbol: string;
}
