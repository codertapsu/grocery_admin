import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule, Optional, SkipSelf } from '@angular/core';

import { SecurityModule } from '@security';

import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { RequestInterceptor } from './interceptors/request.interceptor';
import { CategoriesService } from './services/categories.service';
import { CouponTypesService } from './services/coupon-types.service';
import { CouponsService } from './services/coupons.service';
import { CustomersService } from './services/customers.service';
import { LocalFilesService } from './services/local-files.service';
import { OrdersService } from './services/orders.service';
import { PaymentsService } from './services/payments.service';
import { ProductsService } from './services/products.service';
import { SettingsService } from './services/settings.service';
import { ShippersService } from './services/shippers.service';
import { UsersService } from './services/users.service';
import { WellKnownService } from './services/well-known.service';

@NgModule({
  imports: [HttpClientModule, SecurityModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true,
    },
    // {
    //   provide: HTTP_INTERCEPTORS,
    //   useClass: ErrorInterceptor,
    //   multi: true,
    //   deps: [Router, AuthService],
    // },
    AuthGuard,
    RoleGuard,
    CategoriesService,
    CouponTypesService,
    CouponsService,
    CustomersService,
    LocalFilesService,
    OrdersService,
    PaymentsService,
    ProductsService,
    SettingsService,
    ShippersService,
    UsersService,
    WellKnownService,
  ],
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
