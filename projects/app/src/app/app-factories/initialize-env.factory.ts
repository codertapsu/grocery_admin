import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export function initializeEnv(httpClient: HttpClient): () => Observable<any> {
  return () => httpClient.get('https://someUrl.com/api/user').pipe();
}
