import { UserRole } from '@core/constants/user-role.constant';
import { User } from '@core/models/user.model';

export interface AuthenticatedUser extends User {
  username: string;
  isTwoFactorAuthenticationEnabled: boolean;
  isEmailConfirmed: boolean;
  isPhoneConfirmed: boolean;
  rbac: UserRole[];
}
