import { APP_INITIALIZER, NgModule, Optional, SkipSelf } from '@angular/core';
import { API_END_POINT } from '@common';

import { AuthService } from './auth.service';
import { initializeAuthFactory } from './initialize-auth.factory';

@NgModule({
  providers: [
    AuthService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeAuthFactory,
      multi: true,
      deps: [AuthService, API_END_POINT],
    },
  ],
})
export class SecurityModule {
  public constructor(@Optional() @SkipSelf() parentModule: SecurityModule) {
    if (parentModule) {
      throw new Error('SecurityModule is already loaded. Import it in the AppModule only');
    }
  }
}
