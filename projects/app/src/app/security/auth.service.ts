import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { API_END_POINT, LOCAL_STORAGE } from '@common';

import { AuthenticatedUser } from './authenticated-user.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable()
export class AuthService {
  private readonly _authUser$ = new BehaviorSubject<AuthenticatedUser>(null);
  private readonly _localStorage: Storage;
  private readonly _authApiEndpoint: string;
  private readonly _2faApiEndpoint: string;

  get authUser$(): Observable<AuthenticatedUser> {
    return this._authUser$.asObservable();
  }

  get authUser(): AuthenticatedUser {
    return this._authUser$.getValue();
  }

  get snapshot() {
    return {
      user: this._authUser$.getValue(),
    };
  }

  public constructor(private _httpClient: HttpClient, private _injector: Injector) {
    this._authApiEndpoint = this._injector.get(API_END_POINT) + '/api/auth';
    this._2faApiEndpoint = this._injector.get(API_END_POINT) + '/api/2fa';
    this._localStorage = this._injector.get(LOCAL_STORAGE);
  }

  public me(): Observable<AuthenticatedUser> {
    return this._httpClient.get<AuthenticatedUser>(`${this._authApiEndpoint}/me`);
  }

  public login(username: string, password: string) {
    return this._httpClient
      .post<AuthenticatedUser>(
        `${this._authApiEndpoint}/login`,
        {
          username,
          password,
        },
        httpOptions,
      )
      .pipe(tap(user => this.updateCurrentUser(user)));
  }

  public registration(username: string, email: string, password: string): Observable<AuthenticatedUser> {
    return this._httpClient.post<AuthenticatedUser>(`${this._authApiEndpoint}/registration`, {
      username,
      email,
      password,
    });
  }

  public resetPassword(token: string, password: string): Observable<unknown> {
    return this._httpClient.post<unknown>(`${this._authApiEndpoint}/reset-password`, { token, password });
  }

  public forgotPassword(email: string): Observable<unknown> {
    return this._httpClient.post<unknown>(`${this._authApiEndpoint}/forgot-password`, { email });
  }

  public generate2FA(): Observable<Blob> {
    return this._httpClient
      .get(`${this._2faApiEndpoint}/generate`, {
        observe: 'response',
        responseType: 'blob',
      })
      .pipe(map(response => response.body));
  }

  public turn2FaOn(twoFactorAuthenticationCode: string): Observable<boolean> {
    return this._httpClient.post<boolean>(`${this._2faApiEndpoint}/turn-on`, { twoFactorAuthenticationCode });
  }

  public authenticate2fa(twoFactorAuthenticationCode: string): Observable<AuthenticatedUser> {
    console.log(twoFactorAuthenticationCode);

    return this._httpClient
      .post<AuthenticatedUser>(`${this._2faApiEndpoint}/authenticate`, {
        twoFactorAuthenticationCode,
      })
      .pipe(tap(user => this.updateCurrentUser(user)));
  }

  public logout(): Observable<void> {
    return this._httpClient.get<void>(`${this._authApiEndpoint}/logout`).pipe(tap(() => this.clear()));
  }

  public clear(): void {
    this._localStorage.clear();
    this._authUser$.next(null);
  }

  public updateCurrentUser(authUser: AuthenticatedUser): void {
    this._authUser$.next({
      id: authUser.id,
      avatar: authUser.avatar,
      email: authUser.email,
      firstName: authUser.firstName,
      lastName: authUser.lastName,
      name: authUser.name,
      phone: authUser.phone,
      username: authUser.username,
      isActive: authUser.isActive,
      isTwoFactorAuthenticationEnabled: authUser.isTwoFactorAuthenticationEnabled,
      isEmailConfirmed: authUser.isEmailConfirmed,
      isPhoneConfirmed: authUser.isPhoneConfirmed,
      rbac: authUser.rbac?.length ? authUser.rbac : [],
    });
  }

  public refreshToken(): Observable<AuthenticatedUser> {
    return this._httpClient.get<AuthenticatedUser>(`${this._authApiEndpoint}/refresh`);
  }
}
