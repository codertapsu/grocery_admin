import { AuthenticatedUser } from './authenticated-user.model';
import { AuthService } from './auth.service';

export function initializeAuthFactory(authService: AuthService, authApiEndpoint: string): () => Promise<unknown> {
  return () =>
    new Promise(async resolve => {
      try {
        let response = await fetch(`${authApiEndpoint}/api/auth/me`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
          credentials: 'include',
        });
        
        if (!response.ok && response.status !== 401) {
          response = await fetch(`${authApiEndpoint}/api/auth/refresh`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
            credentials: 'include',
          });
        }

        if (!response.ok) {
          throw Error(response.statusText);
        }

        const authUser: AuthenticatedUser = await response.json();
        authService.updateCurrentUser(authUser);

        resolve(true);
      } catch (error) {
        authService.clear();
        resolve(true);
      }
    });
}
