import { map, Observable, timer } from 'rxjs';

export const delayedExecutor = (delay: number, fn: () => Observable<unknown>) => (delay > 0 ? timer(delay * 1000).pipe(map(() => fn())) : fn());
