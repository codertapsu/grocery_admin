import { formatDate } from '@angular/common';
import { ChangeDetectorRef, OnDestroy, Pipe, PipeTransform } from '@angular/core';

import { Subscription } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { LocaleService } from '../services/locale.service';

// import { getTimezoneName } from '@shared/helpers/date-time.helper';
// import { DisplayedDateEnum } from '@shared/models/displayed-date.model';

@Pipe({
  name: 'localeDate',
  pure: false,
})
export class LocaleDatePipe implements PipeTransform, OnDestroy {
  private _value: string | number | Date;
  private _format: string;
  private _formattedDate: string;
  private _subscription: Subscription;

  public constructor(private localeService: LocaleService, private changeDetectorRef: ChangeDetectorRef) {
    this._subscription = this.localeService.localeChanged$
      .pipe(
        distinctUntilChanged(),
        map(locale => formatDate(this._value, this._format || 'shortDate', locale)),
      )
      .subscribe(formattedDate => {
        // const timezoneName = getTimezoneName(this._timezone || Intl.DateTimeFormat().resolvedOptions().timeZone);
        this._formattedDate = formattedDate;
        this.changeDetectorRef.markForCheck();
      });
  }

  public transform(value: Date | string | number, format?: string) {
    if (!value) {
      return '';
    }
    this._value = value;
    this._format = format;

    return this._formattedDate;
  }

  public ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }
}
