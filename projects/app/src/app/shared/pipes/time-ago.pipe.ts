import { ChangeDetectorRef, OnDestroy, Pipe, PipeTransform } from '@angular/core';

import { formatDistanceToNow, parseISO } from 'date-fns';
import { Subscription, timer } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

@Pipe({
  name: 'timeAgo',
  pure: false,
})
export class TimeAgoPipe implements OnDestroy, PipeTransform {
  private _time: Date;
  private _text: string;
  private _subscription: Subscription;

  public constructor(private changeDetectorRef: ChangeDetectorRef) {
    this._subscription = timer(0, 60000)
      .pipe(
        filter(() => !!this._time),
        map(() => formatDistanceToNow(this._time, { addSuffix: true, includeSeconds: true })),
        distinctUntilChanged(),
      )
      .subscribe(value => {
        this._text = value;
        this.changeDetectorRef.markForCheck();
      });
  }

  public ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }

  public transform(value: string): string {
    this._time = parseISO(value);

    return this._text;
  }
}

// export class TimeAgoPipe extends AsyncPipe {

//   private _time: Date;
//   private formatted$: Observable<string>;

//   constructor(private changeDetectorRef: ChangeDetectorRef) {
//     super(changeDetectorRef);

//     this.formatted$ = timer(0, 60000).pipe(
//       map(() => formatDistanceToNow(this._time, { addSuffix: true, includeSeconds: true })),
//       distinctUntilChanged(),
//       tap(time => console.log('new time:', time)),
//     );
//   }

//   public transform(value: any): any {
//     this._time = parseISO(value);
//     return super.transform(this.formatted$);
//   }

// }
