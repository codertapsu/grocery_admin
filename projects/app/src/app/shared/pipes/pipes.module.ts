import { NgModule } from '@angular/core';
import { TimeAgoPipe } from './time-ago.pipe';
import { LocaleDatePipe } from './locale-date.pipe';
import { SafePipe } from './safe.pipe';

@NgModule({
  declarations: [TimeAgoPipe, LocaleDatePipe, SafePipe],
  imports: [],
  exports: [TimeAgoPipe, LocaleDatePipe, SafePipe],
})
export class PipesModule {}
