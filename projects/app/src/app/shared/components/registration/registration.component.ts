import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

import { TypedFormGroup } from '@common';
import { AuthService, Registration } from '@security';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpComponent implements OnInit {
  public form: TypedFormGroup<Registration>;
  public isEmailAsUsername = true;

  public constructor(private authService: AuthService) {}

  public ngOnInit(): void {
    this.form = new TypedFormGroup<Registration>({
      email: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      username: new FormControl('', [Validators.required]),
    });
  }

  public submit(): void {
    this.form.markAsSubmitted();
    const { username, email, password } = this.form.getRawValue();
    this.authService.registration(username, email, password).subscribe({
      next: () => {},
      error: err => {
        console.error(err);
      },
      complete: () => {},
    });
  }
}
