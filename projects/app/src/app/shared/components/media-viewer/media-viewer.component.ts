import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Inject, Input, OnChanges, OnInit } from '@angular/core';
import { API_END_POINT } from '@common';
import { Media } from '@core/models/media.model';

@Component({
  selector: 'app-media-viewer',
  templateUrl: './media-viewer.component.html',
  styleUrls: ['./media-viewer.component.scss'],
  standalone: true,
  imports: [CommonModule],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MediaViewerComponent implements OnChanges {
  public readonly defaultAvatar = '/assets/icons/avatar_default.jpg';

  @Input() public media: Media;
  public isImage: boolean;
  public isVideo: boolean;

  public constructor(@Inject(API_END_POINT) public apiEndpoint: string) {}

  public ngOnChanges(): void {
    this.isImage = this.media?.mimetype?.startsWith('image/');
    this.isVideo = this.media?.mimetype?.startsWith('video/');
  }
}
