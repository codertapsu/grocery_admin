import { Component, OnDestroy, OnInit } from '@angular/core';

import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { shareReplay, startWith, switchMap } from 'rxjs/operators';

import { GenericEntity } from '@common';
import { PaginationData } from '@core/models/pagination-data.model';
import { Pagination } from '@ui';

@Component({
  template: '',
})
export abstract class PaginationTable<T extends GenericEntity> implements OnInit, OnDestroy {
  private readonly _refresh$ = new Subject<void>();

  public readonly pagination$ = new BehaviorSubject<Pagination>({
    index: 0,
    size: 10,
  });

  public source$: Observable<PaginationData<T>>;

  public ngOnInit(): void {
    this.source$ = this._refresh$.pipe(
      startWith(1),
      switchMap(() => this.getSource()),
      shareReplay({ refCount: true, bufferSize: 1 }),
    );
  }

  public ngOnDestroy(): void {
    this._refresh$.complete();
  }

  public trackDataTable(index: number, item: T): T['id'] {
    return item.id || index;
  }

  public refresh(): void {
    this._refresh$.next();
  }

  public changePage(pagination: Pagination): void {
    console.log('pagination', pagination);
    
    this.pagination$.next(pagination);
  }

  protected abstract getSource(): Observable<PaginationData<T>>;
}
