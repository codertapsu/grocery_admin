import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '@ui';

import { CameraComponent } from './camera.component';
import { CameraService } from './camera.service';

@NgModule({
  declarations: [CameraComponent],
  imports: [CommonModule, MaterialModule],
  exports: [CameraComponent],
  providers: [CameraService],
})
export class CameraModule {}
