import { Injectable, Injector, ViewContainerRef } from '@angular/core';
import { OverlayRef, OverlayService } from '@ui';
import { CameraComponent } from './camera.component';

@Injectable()
export class CameraService {
  public constructor(private overlayService: OverlayService) {}

  public open() {
    return this.overlayService.open<{ type: string; payload: unknown }>(CameraComponent).afterClosed$;
  }
}
