import { NgModule } from '@angular/core';
import { SharingComponent } from './sharing.component';
import { SharingService } from './sharing.service';

@NgModule({
  declarations: [SharingComponent],
  providers: [SharingService],
})
export class SharingModule {}
