import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sharing',
  templateUrl: './sharing.component.html',
  styleUrls: ['./sharing.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SharingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
