import { Inject, Injectable } from '@angular/core';

import { map } from 'rxjs/operators';

import { NAVIGATOR } from '@common';
import { OverlayService } from '@ui';

import { SharingComponent } from './sharing.component';
import { from, Observable } from 'rxjs';

@Injectable()
export class SharingService {
  public constructor(private overlayService: OverlayService, @Inject(NAVIGATOR) private navigator: Navigator) {}

  public socialSharing(title: string, text: string, url: string): Observable<string> {
    // const url = window.location.href;
    // const message = `${title} - ${description}`;
    // const hashtags = '#angular #angular-cli #ng-bootstrap #ng-bootstrap-cli';
    // const share = `https://www.facebook.com/sharer/sharer.php?u=${url}&t=${message}&hashtags=${hashtags}`;
    // window.open(share, '_blank');
    if (this.navigator.share) {
      return from(
        this.navigator.share({
          title,
          text,
          url,
        }),
      ).pipe(map(() => 'Thanks for sharing!'));
    }
    return this.openShareDialog(title, text, url);
  }

  private openShareDialog(title: string, text: string, url: string): Observable<string> {
    return this.overlayService
      .open(SharingComponent, {
        data: {
          title,
          text,
          url,
        },
      })
      .afterClosed$.pipe(map(() => 'Thanks for sharing!'));
  }
}
