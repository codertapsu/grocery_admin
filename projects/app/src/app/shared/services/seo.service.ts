import { Inject, Injectable } from '@angular/core';
import { NAVIGATOR } from '@common';
import { OverlayService } from '@ui';

@Injectable({
  providedIn: 'root',
})
export class SeoService {
  public constructor(
    private overlayService: OverlayService,
    @Inject(NAVIGATOR) private navigator: Navigator) {}

  public socialSharing(title: string, text: string, url: string): void {
    // const url = window.location.href;
    // const message = `${title} - ${description}`;
    // const hashtags = '#angular #angular-cli #ng-bootstrap #ng-bootstrap-cli';
    // const share = `https://www.facebook.com/sharer/sharer.php?u=${url}&t=${message}&hashtags=${hashtags}`;
    // window.open(share, '_blank');
    if (this.navigator.share) {
      this.navigator.share({
        title,
        text,
        url,
      });
    } else {
      this.openShareDialog();
    }
  }

  private openShareDialog(){}
}
