import { registerLocaleData } from '@angular/common';
import localeEnglish from '@angular/common/locales/en';
import localeEnglishExtra from '@angular/common/locales/extra/en';
import localeFrenchExtra from '@angular/common/locales/extra/fr';
import localeJapanExtra from '@angular/common/locales/extra/ja';
import localeRussianExtra from '@angular/common/locales/extra/ru';
import localeVietnameseExtra from '@angular/common/locales/extra/vi';
import localeFrench from '@angular/common/locales/fr';
import localeJapan from '@angular/common/locales/ja';
import localeRussian from '@angular/common/locales/ru';
import localeVietnamese from '@angular/common/locales/vi';
import { Injectable, Optional, SkipSelf } from '@angular/core';
import { Router } from '@angular/router';

import { BehaviorSubject } from 'rxjs';

@Injectable()
export class LocaleService {
  private readonly _registeredLocales = new Set<string>();
  private readonly _locale$ = new BehaviorSubject<string>('en');

  public localeChanged$ = this._locale$.asObservable();

  get locale(): string {
    return this._locale$.value;
  }

  public constructor(
    private router: Router,
    @Optional()
    @SkipSelf()
    otherInstance: LocaleService,
  ) {
    if (otherInstance) {
      throw new Error('LocaleService should have only one instance.');
    }
  }

  public registerLocale(newLocale: string) {
    if (!newLocale) {
      return;
    }
    if (!this._registeredLocales.has(newLocale)) {
      this._registeredLocales.add(newLocale);
      switch (newLocale) {
        case 'en': {
          registerLocaleData(localeEnglish, 'en', localeEnglishExtra);
          break;
        }
        case 'fr': {
          registerLocaleData(localeFrench, 'fr', localeFrenchExtra);
          break;
        }
        case 'ja': {
          registerLocaleData(localeJapan, 'ja', localeJapanExtra);
          break;
        }
        case 'ru': {
          registerLocaleData(localeRussian, 'ru', localeRussianExtra);
          break;
        }
        case 'vi': {
          registerLocaleData(localeVietnamese, 'vi', localeVietnameseExtra);
          break;
        }
      }
    }

    this._locale$.next(newLocale);
  }
}
