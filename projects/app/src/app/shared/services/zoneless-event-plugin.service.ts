import { Injectable } from '@angular/core';
import { EventManager } from '@angular/platform-browser';

@Injectable()
export class ZonelessEventPluginService {
  public manager: EventManager;

  public supports(eventName: string): boolean {
    return eventName.endsWith('.zoneless');
  }

  public addEventListener(element: HTMLElement, eventName: string, handler: EventListener): () => void {
    const [nativeEventName] = eventName.split('.');

    this.manager.getZone().runOutsideAngular(() => {
      element.addEventListener(nativeEventName, handler);
    });

    return () => element.removeEventListener(nativeEventName, handler);
  }
}
