import { Injectable } from '@angular/core';
import { PlatformService } from './platform.service';

@Injectable()
export class KeyboardService {
  public constructor(private _platformService: PlatformService) {}

  public onPress() {
    document.addEventListener('keydown', e => {
      if (e.key === 's' && (this._platformService.MAC ? e.metaKey : e.ctrlKey)) {
        e.preventDefault();
      }
    });
  }
}
