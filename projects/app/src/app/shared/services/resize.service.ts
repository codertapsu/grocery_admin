import { Inject, Injectable, NgZone } from '@angular/core';

import { BehaviorSubject, fromEvent } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { Dimension, WINDOW } from '@common';

@Injectable()
export class ResizeService {
  private readonly _viewport$ = new BehaviorSubject<Dimension>({
    width: 0,
    height: 0,
  });

  public constructor(private ngZone: NgZone, @Inject(WINDOW) private window: Window) {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.window, 'resize')
        .pipe(
          startWith(1),
          map(
            () =>
              ({
                width: this.window.innerWidth,
                height: this.window.innerHeight,
              } as Dimension),
          ),
        )
        .subscribe(dimension => {
          this.updateViewport(dimension);
        });
    });
  }

  public updateViewport(dimension: Dimension): void {
    this._viewport$.next(dimension);
  }
}
