import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, NgZone } from '@angular/core';

import { WINDOW } from '@common';

@Injectable()
export class LoadingService {
  private _timerId: number;
  private _loadingElement: HTMLElement;

  public constructor(
    private _ngZone: NgZone,
    @Inject(WINDOW) private _window: Window,
    @Inject(DOCUMENT) private _document: Document,
  ) {}

  public show(): void {
    this.changeLoadingState(true);
  }

  public hide(): void {
    this.changeLoadingState(false);
  }

  private changeLoadingState(isOpen: boolean) {
    this._ngZone.runOutsideAngular(() => {
      if (!this._loadingElement) {
        this._loadingElement = this._document.body.querySelector(':scope > .loading');
      }
      if (this._timerId) {
        this._window.clearTimeout(this._timerId);
      }
      if (isOpen) {
        this._loadingElement.style.display = 'flex';
      } else {
        this._timerId = this._window.setTimeout(() => {
          this._loadingElement.style.display = 'none';
        }, 300);
      }
    });
  }
}
