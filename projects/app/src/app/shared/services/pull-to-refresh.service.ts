import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, NgZone, OnDestroy } from '@angular/core';

import { fromEvent, Observable, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { Coordinates } from '@common';

@Injectable()
export class PullToRefreshService implements OnDestroy {
  private readonly _destroyed$ = new Subject<void>();
  private readonly _pulled$ = new Subject<number>();
  private readonly _pulling$ = new Subject<number>();

  get onPulling$(): Observable<number> {
    return this._pulling$.asObservable();
  }

  get onPulled$(): Observable<number> {
    return this._pulled$.asObservable();
  }

  public constructor(private _ngZone: NgZone, @Inject(DOCUMENT) private _document: Document) {
    this.init();
  }

  public ngOnDestroy(): void {
    this._destroyed$.next();
    this._destroyed$.complete();
  }

  private init(): void {
    // keyboard
    this._ngZone.runOutsideAngular(() => {
      const start$ = fromEvent<TouchEvent>(this._document, 'touchstart')
        .pipe(
          filter(event => event.touches.length === 1),
          map<TouchEvent, Coordinates>(event => ({ x: event.touches[0].screenX, y: event.touches[0].screenY })),
        )
        .pipe(takeUntil(this._destroyed$));

      const end$ = fromEvent<TouchEvent>(this._document, 'touchend').pipe(takeUntil(this._destroyed$));
      const dragging$ = fromEvent<TouchEvent>(this._document, 'touchmove')
        .pipe(
          tap(event => event.preventDefault()),
          filter(event => event.touches.length === 1),
          map<TouchEvent, Coordinates>(event => ({ x: event.touches[0].screenX, y: event.touches[0].screenY })),
        )
        .pipe(takeUntil(end$));

      let delta = 0;
      start$
        .pipe(switchMap(startCoord => dragging$.pipe(map(currentCoord => ({ currentCoord, startCoord })))))
        .subscribe(({ currentCoord, startCoord }) => {
          delta = currentCoord.y - startCoord.y;
          if (this._document.body.scrollTop === 0) {
            this._pulling$.next(Math.abs(delta));
          }
        });

      end$.subscribe(() => {
        if (this._document.body.scrollTop === 0 && delta > 100) {
          this._pulled$.next(Math.abs(delta));
        }
      });
    });
  }
}
