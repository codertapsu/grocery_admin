export interface MenuItem {
  routerLink: string;
  name: string;
  children?: MenuItem[];
}
