export interface DataPagination<T> {
  data: T[];
  recordsFiltered: number;
  recordsTotal: number;
}
