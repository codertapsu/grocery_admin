import { Directive, ElementRef, Inject, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { API_END_POINT } from '@common';

@Directive({
  selector: '[fileSrc]'
})
export class FileSrcDirective implements OnChanges, OnDestroy {
  @Input() fileSrc: string;

  public constructor(private elementRef: ElementRef<HTMLElement>, @Inject(API_END_POINT) private apiEndpoint: string) { }
  
  public ngOnChanges(): void {
    (this.elementRef.nativeElement as HTMLImageElement).src = `${this.apiEndpoint}/${this.fileSrc}`;
  }

  public ngOnDestroy(): void {}
}
