import { DOCUMENT } from '@angular/common';
import { AfterViewInit, Directive, ElementRef, Inject, Input, NgZone, OnDestroy, Renderer2 } from '@angular/core';

import { ElementDraggable, WINDOW } from '@common';

@Directive({
  selector: '[appDraggableFloating]',
})
export class DraggableFloatingDirective implements AfterViewInit, OnDestroy {
  private _elementDraggable: ElementDraggable;

  @Input() public gap = 20;

  public constructor(
    private elementRef: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    private ngZone: NgZone,
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window: Window,
  ) {}

  public ngAfterViewInit(): void {
    this.ngZone.runOutsideAngular(() => {
      this._elementDraggable = new ElementDraggable(
        this.document,
        this.elementRef.nativeElement,
        this.gap,
        this.renderer,
        this.window,
      );
    });
  }

  public ngOnDestroy(): void {
    if (this._elementDraggable) {
      this._elementDraggable.destroy();
    }
  }
}
