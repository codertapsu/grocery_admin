import { AfterViewInit, Directive, ElementRef, Inject } from '@angular/core';

import { BrowserWindow, WINDOW } from '@common';

/**
 * Directive to lazy load images.
 * @example
 * <img data-src="https://placehold.it/200x200" loading="lazy" />
 */
@Directive({
  selector: 'img[lazy], img[loading="lazy"], iframe[loading="lazy"]',
})
export class LazyLoadDirective implements AfterViewInit {
  public constructor(
    private elementRef: ElementRef<HTMLImageElement | HTMLIFrameElement>,
    @Inject(WINDOW) private window: BrowserWindow,
  ) {}

  public ngAfterViewInit(): void {
    // if ('loading' in HTMLImageElement.prototype) {
    //   this.loadSrcImmediately();
    // } else {
    //   this.lazyLoadSrc();
    // }
    if ('IntersectionObserver' in this.window) {
      this.lazyLoadSrc();
    } else {
      this.loadSrcImmediately();
    }
  }

  private loadSrcImmediately(): void {
    this.elementRef.nativeElement.src = this.elementRef.nativeElement.dataset['src'];
  }

  private lazyLoadSrc(): void {
    /**
     * Dynamically import the LazySizes library
     */
    // const script = document.createElement('script');
    // script.src = 'https://cdnjs.cloudflare.com/ajax/libs/lazysizes/5.1.2/lazysizes.min.js';
    // document.body.appendChild(script);
    const observer = new IntersectionObserver(
      entries => {
        entries.forEach(entry => {
          if (entry.intersectionRatio > 0) {
            console.log('in the view');
            const element = entry.target as HTMLImageElement | HTMLIFrameElement;
            element.src = element.dataset['src'];
            observer.unobserve(element);
          } else {
            console.log('out of view');
          }
        });
      },
      {
        threshold: [0.1],
        rootMargin: '0px',
        root: null,
      },
    );

    observer.observe(this.elementRef.nativeElement);
  }
}
