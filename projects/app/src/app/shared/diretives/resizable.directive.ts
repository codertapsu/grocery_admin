import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  Output,
  Renderer2,
} from '@angular/core';

import { fromEvent, Subject } from 'rxjs';
import { map, switchMap, takeUntil, takeWhile } from 'rxjs/operators';

import { Dimension } from '@common';

@Directive({
  selector: '[appResizable]',
})
export class ResizableDirective implements AfterViewInit, OnDestroy {
  private readonly _destroyed$ = new Subject<void>();

  @Input() public resizeMode: 'horizontal' | 'vertical' | 'free' = 'free';
  @Input() public resizeHandler: HTMLElement;
  @Input() public isBlockedResize: boolean;
  @Output() public dimensionChanged = new EventEmitter<Dimension>();

  public constructor(
    private elementRef: ElementRef<HTMLElement>,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: Document,
  ) {}

  public ngAfterViewInit(): void {
    const mousedown$ = fromEvent<MouseEvent>(this.resizeHandler, 'mousedown').pipe(takeUntil(this._destroyed$));
    const mouseup$ = fromEvent<MouseEvent>(this.document, 'mouseup').pipe(takeUntil(this._destroyed$));
    const mousemove$ = fromEvent<MouseEvent>(this.document, 'mousemove').pipe(
      takeWhile(() => !this.isBlockedResize),
      takeUntil(mouseup$),
    );

    mousedown$
      .pipe(
        switchMap(() => mousemove$),
        map(moveEvent => {
          const { width, height } = this.elementRef.nativeElement.getBoundingClientRect();
          const latestWidth =
            this.resizeMode === 'horizontal' || this.resizeMode === 'free' ? width + moveEvent.movementX : width;
          const latestHeight =
            this.resizeMode === 'vertical' || this.resizeMode === 'free' ? height + moveEvent.movementY : height;

          return [latestWidth, latestHeight];
        }),
      )
      .subscribe(([width, height]) => {
        this.renderer.setStyle(this.elementRef.nativeElement, 'width', width);
        this.renderer.setStyle(this.elementRef.nativeElement, 'height', height);

        this.dimensionChanged.emit({ width, height });
      });
  }

  public ngOnDestroy(): void {
    this._destroyed$.next();
    this._destroyed$.complete();
  }
}
