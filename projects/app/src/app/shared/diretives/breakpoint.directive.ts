import {
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
} from '@angular/core';

import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { Dimension, resizeObserver } from '@common';

@Directive({
  selector: '[appBreakpoint]',
})
export class BreakpointDirective implements OnInit, OnDestroy {
  private readonly _destroyed$ = new Subject<void>();

  @Input() public breakpoints: { [p: string]: number } = { sm: 576, md: 768, lg: 992, xl: 1200 };
  @Output() public sizeChange = new EventEmitter<Dimension>();

  public constructor(
    private elementRef: ElementRef<HTMLElement>,
    private ngZone: NgZone,
    private renderer: Renderer2,
  ) {}

  public ngOnInit(): void {
    resizeObserver(this.elementRef.nativeElement)
      .pipe(
        map(entries => entries[0]),
        takeUntil(this._destroyed$),
      )
      .subscribe(({ contentRect: { width, height } }) => {
        const breakpoint = Object.entries(this.breakpoints);

        for (let index = 0, total = breakpoint.length; index < total; index++) {
          const [key, value] = breakpoint[index];
          if (value > width && !this.elementRef.nativeElement.classList.contains(key)) {
            this.renderer.addClass(this.elementRef.nativeElement, key);
          }
          if (value <= width && this.elementRef.nativeElement.classList.contains(key)) {
            this.renderer.removeClass(this.elementRef.nativeElement, key);
          }
        }

        this.sizeChange.emit({ height, width });
      });
  }

  public ngOnDestroy(): void {
    this._destroyed$.next();
    this._destroyed$.complete();
  }
}
