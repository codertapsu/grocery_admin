import { NgModule } from '@angular/core';

import { FileSrcDirective } from './file-src.directive';
import { LazyLoadDirective } from './lazy-load.directive';
import { InViewport } from './in-viewport.directive';
import { ResizableDirective } from './resizable.directive';
import { BreakpointDirective } from './breakpoint.directive';
import { DraggableFloatingDirective } from './draggable-floating.directive';
import { LongPressDirective } from './long-press.directive';
import { ImgErrorDirective } from './img-error.directive';

@NgModule({
  declarations: [
    FileSrcDirective,
    LazyLoadDirective,
    InViewport,
    ResizableDirective,
    BreakpointDirective,
    DraggableFloatingDirective,
    LongPressDirective,
    ImgErrorDirective,
  ],
  exports: [
    FileSrcDirective,
    LazyLoadDirective,
    InViewport,
    ResizableDirective,
    BreakpointDirective,
    DraggableFloatingDirective,
    LongPressDirective,
    ImgErrorDirective,
  ],
})
export class DirectivesModule {}
