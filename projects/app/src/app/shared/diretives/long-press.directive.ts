import { Directive, ElementRef, EventEmitter, Inject, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { fromEvent, merge, of, Subscription, timer } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';

import { WINDOW } from '@common';

@Directive({
  selector: '[appLongPress]',
})
export class LongPressDirective implements OnInit, OnDestroy {
  @Input() public threshold = 500;
  @Output() public longPress = new EventEmitter<void>();

  private _subscription: Subscription;

  public constructor(private elementRef: ElementRef<HTMLElement>, @Inject(WINDOW) private window: Window) {}

  public ngOnInit(): void {
    const mousedown$ = fromEvent<MouseEvent>(this.elementRef.nativeElement, 'mousedown').pipe(
      filter(event => event.button === 0),
      map(() => true),
    );
    const touchstart$ = fromEvent(this.elementRef.nativeElement, 'touchstart').pipe(map(() => true));
    const touchEnd$ = fromEvent(this.elementRef.nativeElement, 'touchend').pipe(map(() => false));
    const mouseup$ = fromEvent<MouseEvent>(this.window, 'mouseup').pipe(
      filter(event => event.button === 0),
      map(() => false),
    );
    this._subscription = merge(mousedown$, mouseup$, touchstart$, touchEnd$)
      .pipe(
        switchMap(state => (state ? timer(this.threshold, 100) : of(null))),
        filter(Boolean),
      )
      .subscribe(() => this.longPress.emit());
  }

  public ngOnDestroy(): void {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }
}
