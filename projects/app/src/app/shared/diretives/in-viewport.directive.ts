import { AfterViewInit, Directive, ElementRef, EventEmitter, OnDestroy, Output } from '@angular/core';

import { Subject, takeUntil } from 'rxjs';

import { intersectionObserver } from '@common';

@Directive({
  selector: '[appInViewport]',
})
export class InViewport implements AfterViewInit, OnDestroy {
  private readonly _destroyed$ = new Subject<void>();

  @Output() public intersecting = new EventEmitter();

  public constructor(private elementRef: ElementRef<HTMLElement>) {}

  public ngAfterViewInit(): void {
    intersectionObserver(this.elementRef.nativeElement, {
      root: this.elementRef.nativeElement.parentElement,
      threshold: 1,
    })
      .pipe(takeUntil(this._destroyed$))
      .subscribe(() => this.intersecting.emit());
  }

  public ngOnDestroy(): void {
    this._destroyed$.next();
    this._destroyed$.complete();
  }
}
