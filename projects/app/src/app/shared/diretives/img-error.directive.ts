import { Directive, ElementRef, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { WINDOW } from '@common';

@Directive({
  selector: 'img[appImgError]',
})
export class ImgErrorDirective implements OnInit, OnDestroy {
  @Input() public retryTimes = 1;
  @Input() public delay = 2000; // milliseconds

  private _timerId: number;

  public constructor(private _elementRef: ElementRef<HTMLImageElement>, @Inject(WINDOW) private _window: Window) {}

  public ngOnInit(): void {
    let counter = this.retryTimes;
    this._elementRef.nativeElement.onerror = () => {
      if (counter) {
        this._timerId = this._window.setTimeout(() => {
          counter--;
          const source = this._elementRef.nativeElement.src;
          this._elementRef.nativeElement.src = source;
        }, this.delay);
      } else {
        this._elementRef.nativeElement.src = '/assets/images/placeholder.png';
      }
    };
  }

  public ngOnDestroy(): void {
    this._window.clearTimeout(this._timerId);
  }
}
