export const SupportedLanguages = [
  {
    locale: 'vi',
    regionCode: 'VN',
    currencyCode: 'VND',
  },
  {
    locale: 'en',
    regionCode: 'US',
    currencyCode: 'USD',
  },
  {
    locale: 'fr',
    regionCode: 'FR',
    currencyCode: 'EUR',
  },
  {
    locale: 'ja',
    regionCode: 'JP',
    currencyCode: 'JPY',
  },
  {
    locale: 'ru',
    regionCode: 'RU',
    currencyCode: 'RUB',
  },
];
