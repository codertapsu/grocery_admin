import { DOCUMENT } from '@angular/common';
import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostListener,
  Inject,
  NgZone,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';

import { fromEvent, map, merge } from 'rxjs';

import { BrowserWindow, LOCAL_STORAGE, THEME_PREFERENCE_KEY, NAVIGATOR, WINDOW } from '@common';
import { NotificationService, OverlayService } from '@ui';
import { ResizeService } from '@shared/services/resize.service';
import { LoadingService } from '@shared/services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements AfterViewInit {
  @ViewChild('themeToggle', { static: true }) private _themeToggle: ElementRef<HTMLButtonElement>;

  private _isDark: boolean;

  public constructor(
    private _viewContainerRef: ViewContainerRef,
    private _notificationService: NotificationService,
    private _loadingService: LoadingService,
    private _ngZone: NgZone,
    private _overlayService: OverlayService,
    @Inject(DOCUMENT) private _document: Document,
    @Inject(WINDOW) private _window: BrowserWindow,
    @Inject(LOCAL_STORAGE) private _localStorage: Storage,
    @Inject(THEME_PREFERENCE_KEY) private _themePreferenceKey: string,
    @Inject(NAVIGATOR) private _navigator: Navigator,
  ) {}

  public ngAfterViewInit(): void {
    this._notificationService.viewContainerRef = this._viewContainerRef;
    this._overlayService.viewContainerRef = this._viewContainerRef;
    this.watchConnectionStatus();
    this.watchThemePreference();
    this._loadingService.hide();
  }

  private watchConnectionStatus(): void {
    this._ngZone.runOutsideAngular(() => {
      merge(fromEvent(this._window, 'online'), fromEvent(this._window, 'offline')).subscribe(() => {
        const isOnline = this._navigator.onLine;
        const message = isOnline
          ? 'The network connection has been restored.'
          : 'The network connection has been lost.';
        const title = isOnline ? 'Online' : 'Offline';
        if (isOnline) {
          this._notificationService.success(message, title);
        } else {
          this._notificationService.warning(message, title);
        }
      });
    });
  }

  private watchThemePreference(): void {
    this._ngZone.runOutsideAngular(() => {
      this._isDark =
        (this._localStorage.getItem(this._themePreferenceKey) ||
        this._window.matchMedia('(prefers-color-scheme: dark)').matches
          ? 'dark'
          : 'light') === 'dark';

      fromEvent(this._themeToggle.nativeElement, 'click')
        .pipe(map(() => !this._isDark))
        .subscribe(isDark => this.setPreference(isDark));
      this._window
        .matchMedia('(prefers-color-scheme: dark)')
        .addEventListener('change', ({ matches: isDark }) => this.setPreference(isDark));
    });
  }

  private setPreference(isDark: boolean): void {
    this._isDark = isDark;
    this._localStorage.setItem(this._themePreferenceKey, isDark ? 'dark' : 'light');
    if (isDark) {
      this._document.body.classList.add('dark');
    } else {
      this._document.body.classList.remove('dark');
    }
  }

  private watchNetWorkInformation(): void {
    const information = this.getConnection();
    if (information) {
      information.addEventListener('change', event => this.updateNetworkInfo(event.target as NetworkInformation));
      this.updateNetworkInfo(information);
    }
  }

  private updateNetworkInfo(information: NetworkInformation) {
    console.log(information);
  }

  private getConnection() {
    return this._navigator.connection;
    /**
     * ||
     * this.navigator.mozConnection ||
     * this.navigator.webkitConnection ||
     * this.navigator.msConnection
     */
  }
}
