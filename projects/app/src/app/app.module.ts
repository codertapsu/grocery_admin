import { LOCALE_ID, NgModule, PLATFORM_ID } from '@angular/core';
import { BrowserModule, EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  ANIMATION_FRAME,
  animationFrameFactory,
  LOCAL_STORAGE,
  localStorageFactory,
  MEDIA_DEVICES,
  mediaDevicesFactory,
  NAVIGATOR,
  navigatorFactory,
  PAGE_VISIBILITY,
  pageVisibilityFactory,
  PERFORMANCE,
  performanceFactory,
  SESSION_STORAGE,
  sessionStorageFactory,
  WINDOW,
  windowFactory,
} from '@common';
import { CoreModule } from '@core/core.module';
import { LocaleId } from '@shared/providers/locale.provider';
import { LoadingService } from '@shared/services/loading.service';
import { LocaleService } from '@shared/services/locale.service';
import { PlatformService } from '@shared/services/platform.service';
import { PullToRefreshService } from '@shared/services/pull-to-refresh.service';
import { ResizeService } from '@shared/services/resize.service';
import { ZonelessEventPluginService } from '@shared/services/zoneless-event-plugin.service';
import { NotificationModule, OverlayModule } from '@ui';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { UnauthorizedComponent } from './pages/unauthorized/unauthorized.component';

@NgModule({
  declarations: [AppComponent, NotFoundComponent, UnauthorizedComponent],
  imports: [BrowserModule, BrowserAnimationsModule, CoreModule, NotificationModule, OverlayModule, AppRoutingModule],
  providers: [
    // {
    //   provide: APP_BOOTSTRAP_LISTENER,
    //   useFactory: bootstrapFactory,
    //   multi: true,
    //   deps: [InitService],
    // },
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: initFactory,
    //   multi: true,
    //   deps: [InitService],
    // },
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: configFactory,
    //   multi: true,
    //   deps: [ConfigService],
    // },
    PlatformService,
    ResizeService,
    LocaleService,
    LoadingService,
    PullToRefreshService,
    {
      provide: LOCALE_ID,
      useClass: LocaleId,
      deps: [LocaleService],
    },
    {
      provide: WINDOW,
      useFactory: windowFactory,
    },
    {
      provide: NAVIGATOR,
      useFactory: navigatorFactory,
    },
    {
      provide: LOCAL_STORAGE,
      useFactory: localStorageFactory,
      deps: [PLATFORM_ID],
    },
    {
      provide: SESSION_STORAGE,
      useFactory: sessionStorageFactory,
      deps: [PLATFORM_ID],
    },
    {
      provide: PERFORMANCE,
      useFactory: performanceFactory,
    },
    {
      provide: ANIMATION_FRAME,
      useFactory: animationFrameFactory,
    },
    {
      provide: PAGE_VISIBILITY,
      useFactory: pageVisibilityFactory,
    },
    {
      provide: MEDIA_DEVICES,
      useFactory: mediaDevicesFactory,
    },
    {
      provide: PAGE_VISIBILITY,
      useFactory: pageVisibilityFactory,
    },
    {
      provide: EVENT_MANAGER_PLUGINS,
      useClass: ZonelessEventPluginService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
// export class AppModule implements DoBootstrap {
//   public ngDoBootstrap(appRef: ApplicationRef): void {}
// }
