import { InjectionToken } from '@angular/core';

export const API_END_POINT = new InjectionToken<string>('Api endpoint');
export const THEME_PREFERENCE_KEY = new InjectionToken<string>('Theme Preference');
