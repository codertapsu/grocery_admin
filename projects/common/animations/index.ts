import { animate, state, style, transition, trigger } from '@angular/animations';

const ANIMATION_TIMINGS = '400ms cubic-bezier(0.25, 0.8, 0.25, 1)';

export const ANIMATIONS = [
  trigger('fade', [
    state('fadeOut', style({ opacity: 0 })),
    state('fadeIn', style({ opacity: 1 })),
    transition('* => fadeIn', animate(ANIMATION_TIMINGS)),
  ]),
  trigger('slideContent', [
    state('void', style({ transform: 'translate3d(0, 25%, 0) scale(0.9)', opacity: 0 })),
    state('enter', style({ transform: 'none', opacity: 1 })),
    state('leave', style({ transform: 'translate3d(0, 25%, 0)', opacity: 0 })),
    transition('* => *', animate(ANIMATION_TIMINGS)),
  ]),
  trigger('messageItem', [
    transition(':enter', [
      style({ transform: 'scale(0.5)', opacity: 0 }),
      animate(ANIMATION_TIMINGS, style({ transform: 'none', opacity: 1 })),
    ]),
    transition(':leave', [
      style({ transform: 'scale(1)', opacity: 1 }),
      animate(ANIMATION_TIMINGS, style({ transform: 'scale(0.5)', opacity: 0 })),
    ]),
  ]),
  trigger('tooltip', [
    transition(':enter', [style({ opacity: 0 }), animate(300, style({ opacity: 1 }))]),
    transition(':leave', [animate(300, style({ opacity: 0 }))]),
  ]),
  trigger('fadeInUpOutDown', [
    transition(
      ':enter',
      [
        style({ opacity: 0, transform: 'translateY(24px)' }),
        animate('{{ duration }}', style({ opacity: 1, transform: 'translateY(0)' })),
      ],
      { params: { duration: '100ms' } },
    ),
    // transition(':leave', [animate('0ms', style({ opacity: 0 }))]),
  ]),
];
