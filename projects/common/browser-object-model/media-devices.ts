import { inject, InjectionToken } from '@angular/core';

import { NAVIGATOR } from './navigator';

const mediaDevicesFactory = () => inject(NAVIGATOR).mediaDevices;

const MEDIA_DEVICES = new InjectionToken<MediaDevices>('Window mediaDevices object');

export { mediaDevicesFactory, MEDIA_DEVICES };
