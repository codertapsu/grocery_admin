import { inject, InjectionToken } from '@angular/core';

import { WINDOW } from './window';

const cryptoFactory = () => inject(WINDOW).crypto;

const CRYPTO = new InjectionToken<Crypto>('Window crypto object');

export { cryptoFactory, CRYPTO };
