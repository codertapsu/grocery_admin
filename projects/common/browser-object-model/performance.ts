import { inject, InjectionToken } from '@angular/core';

import { WINDOW } from './window';

const performanceFactory = () => inject(WINDOW).performance;

const PERFORMANCE = new InjectionToken<Performance>('An abstraction over window.performance object');

export { PERFORMANCE, performanceFactory };
