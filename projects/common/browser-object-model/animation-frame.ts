import { inject, InjectionToken } from '@angular/core';

import { animationFrameScheduler, interval, Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';

import { PERFORMANCE } from './performance';

// const animationFrameFactory = () => {
//   const { requestAnimationFrame, cancelAnimationFrame } = inject(WINDOW);

//   const animationFrame$ = new Observable<DOMHighResTimeStamp>(subscriber => {
//     let id = NaN;
//     const callback = (timestamp: DOMHighResTimeStamp) => {
//       subscriber.next(timestamp);
//       id = requestAnimationFrame(callback);
//     };

//     id = requestAnimationFrame(callback);

//     return () => {
//       cancelAnimationFrame(id);
//     };
//   });

//   return animationFrame$.pipe(share());
// };

const animationFrameFactory = () => {
  const performanceRef = inject(PERFORMANCE);

  return interval(0, animationFrameScheduler).pipe(
    map(() => performanceRef.now()),
    share(),
  );
};

const ANIMATION_FRAME = new InjectionToken<Observable<DOMHighResTimeStamp>>('Window raf Observable');

export { animationFrameFactory, ANIMATION_FRAME };
