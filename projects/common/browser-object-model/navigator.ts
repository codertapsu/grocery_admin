import { inject, InjectionToken } from '@angular/core';

import { WINDOW } from './window';

const navigatorFactory = () => inject(WINDOW).navigator;

const NAVIGATOR = new InjectionToken<Navigator>('Navigator object');

export { navigatorFactory, NAVIGATOR };
