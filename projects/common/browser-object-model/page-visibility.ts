import { DOCUMENT } from '@angular/common';
import { inject, InjectionToken } from '@angular/core';

import { fromEvent, Observable } from 'rxjs';
import { distinctUntilChanged, map, share, startWith } from 'rxjs/operators';

const pageVisibilityFactory = () => {
  const documentRef = inject(DOCUMENT);

  return fromEvent(documentRef, 'visibilitychange').pipe(
    startWith(0),
    map(() => documentRef.visibilityState !== 'hidden'),
    distinctUntilChanged(),
    share(),
  );
};

const PAGE_VISIBILITY = new InjectionToken<Observable<boolean>>('Page visibility observable');

export { pageVisibilityFactory, PAGE_VISIBILITY };
