import { inject, InjectionToken } from '@angular/core';

import { NAVIGATOR } from './navigator';

const networkInformationFactory = () => inject(NAVIGATOR).connection;

const NETWORK_INFORMATION = new InjectionToken<NetworkInformation>('Window navigator connection object');

export { networkInformationFactory, NETWORK_INFORMATION };
