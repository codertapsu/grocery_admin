import { DOCUMENT } from '@angular/common';
import { inject, InjectionToken } from '@angular/core';

const windowFactory = () => inject(DOCUMENT).defaultView;

type BrowserWindow = ReturnType<typeof windowFactory>;

const WINDOW = new InjectionToken<BrowserWindow>('Global window object');

export { windowFactory, WINDOW, BrowserWindow };
