import { isPlatformBrowser } from '@angular/common';
import { inject, InjectionToken } from '@angular/core';

import { WINDOW } from './window';

const localStorageFactory = (platformId: Object) => (isPlatformBrowser(platformId) ? inject(WINDOW).localStorage : new Storage());

const LOCAL_STORAGE = new InjectionToken<ReturnType<typeof localStorageFactory>>('Local storage object');

const sessionStorageFactory = (platformId: Object) => (isPlatformBrowser(platformId) ? inject(WINDOW).sessionStorage : new Storage());

const SESSION_STORAGE = new InjectionToken<ReturnType<typeof sessionStorageFactory>>('Session storage object');

export { localStorageFactory, LOCAL_STORAGE, sessionStorageFactory, SESSION_STORAGE };
