export function dataURItoBlob(dataURI: string, mimeString?: string): Blob {
  let byteString;
  if (dataURI.split(',')[0].indexOf('base64') >= 0) {
    byteString = atob(dataURI.split(',')[1]);
  } else {
    byteString = unescape(dataURI.split(',')[1]);
  }

  if (!mimeString) {
    mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  }

  const length = byteString.length;
  const ia = new Uint8Array(length);
  for (let i = 0; i < length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  return new Blob([ia], { type: mimeString });
}
