export function getTransformValue(element: HTMLElement): { x: number; y: number; z: number } {
  const style = window.getComputedStyle(element);
  const matrix = style.transform;

  if (matrix === 'none' || typeof matrix === 'undefined') {
    return {
      x: 0,
      y: 0,
      z: 0,
    };
  }

  const matrixType = matrix.includes('3d') ? '3d' : '2d';
  const matrixValues = matrix.match(/matrix.*\((.+)\)/)[1].split(', ');

  if (matrixType === '2d') {
    return {
      x: Number(matrixValues[4]),
      y: Number(matrixValues[5]),
      z: 0,
    };
  }

  if (matrixType === '3d') {
    return {
      x: Number(matrixValues[12]),
      y: Number(matrixValues[13]),
      z: Number(matrixValues[14]),
    };
  }

  return {
    x: 0,
    y: 0,
    z: 0,
  };
}
