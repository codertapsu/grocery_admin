export const saveFile = (blob: Blob, fileName: string) => {
  const url = window.URL.createObjectURL(blob);
  const link = document.createElement('a');
  link.href = url;
  link.download = fileName;
  link.click();
  window.URL.revokeObjectURL(url);
  // this.document.body.appendChild(link);

  // link.dispatchEvent(
  //   new MouseEvent('click', {
  //     bubbles: true,
  //     cancelable: true,
  //     view: this.window,
  //   }),
  // );
  // setTimeout(() => {
  //   URL.revokeObjectURL(uriContent);
  //   link.remove();
  // }, 1);
}
