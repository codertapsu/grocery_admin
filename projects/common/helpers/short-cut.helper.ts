import { combineLatest, fromEvent, merge, Observable } from 'rxjs';
import { distinctUntilChanged, filter, share } from 'rxjs/operators';

import { KeyCode } from '../models/key-codes';

export const shortcut = (shortcut: KeyCode[]) => {
  const keyDown$ = fromEvent<KeyboardEvent>(document, 'keydown');
  const keyUp$ = fromEvent<KeyboardEvent>(document, 'keyup');

  const keyEvents = merge(keyDown$, keyUp$).pipe(
    distinctUntilChanged((a, b) => a.code === b.code && a.type === b.type),
    share(),
  );

  const createKeyPressStream = (charCode: KeyCode) =>
    keyEvents.pipe(filter(event => event.code === charCode.valueOf()));

  return combineLatest(shortcut.map(s => createKeyPressStream(s))).pipe(
    filter<KeyboardEvent[]>(arr => arr.every(a => a.type === 'keydown')),
  );
};

/**
 * @example
 * const abc$ = shortcut([KeyCode.KeyA, KeyCode.KeyB, KeyCode.KeyC]).pipe(sequence());
 * Now the abc$ Observable will only emit when the keys are pressed sequentially (a->b->c).
 * */

export const sequence = () => (source: Observable<KeyboardEvent[]>) =>
  source.pipe(
    filter(arr => {
      const sorted = [...arr]
        .sort((a, b) => (a.timeStamp < b.timeStamp ? -1 : 1))
        .map(a => a.code)
        .join();
      const seq = arr.map(a => a.code).join();
      return sorted === seq;
    }),
  );
