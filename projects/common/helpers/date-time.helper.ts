export const dateWithTimeZone = (timeZone: string, value: Date): Date => {
  const date = new Date(Date.UTC(value.getFullYear(), value.getMonth(), value.getDate(), value.getHours(), value.getMinutes(), 0, 0));

  const utcDate = new Date(date.toLocaleString('en-US', { timeZone: 'UTC' }));
  const tzDate = new Date(date.toLocaleString('en-US', { timeZone }));
  const offset = utcDate.getTime() - tzDate.getTime();

  date.setTime(date.getTime() + offset);

  return date;
};

export const getTimezoneName = (timeZone = 'UTC', locale = 'fr') =>
  Intl.DateTimeFormat(locale, {
    timeZone,
    timeZoneName: 'short'
  })
    .formatToParts()
    .find(i => i.type === 'timeZoneName').value;

export const isSameDate = (date1: Date, date2: Date): boolean =>
  date1.getFullYear() === date2.getFullYear() && date1.getMonth() === date2.getMonth() && date1.getDate() === date2.getDate();

export const isBeforeOrSameDate = (date1: Date, date2: Date, isIgnoreTime = false): boolean => {
  const copiedDate1 = new Date(date1.valueOf());
  const copiedDate2 = new Date(date2.valueOf());
  if (isIgnoreTime) {
    copiedDate1.setHours(0, 0, 0, 0);
    copiedDate2.setHours(0, 0, 0, 0);
  }
  return copiedDate1.getTime() <= copiedDate2.getTime();
};

export const isAfterOrSameDate = (date1: Date, date2: Date, isIgnoreTime = false): boolean => {
  const copiedDate1 = new Date(date1.valueOf());
  const copiedDate2 = new Date(date2.valueOf());
  if (isIgnoreTime) {
    copiedDate1.setHours(0, 0, 0, 0);
    copiedDate2.setHours(0, 0, 0, 0);
  }
  return copiedDate1.getTime() >= copiedDate2.getTime();
};

export const isAfterDate = (date1: Date, date2: Date, isIgnoreTime = false): boolean => {
  const copiedDate1 = new Date(date1.valueOf());
  const copiedDate2 = new Date(date2.valueOf());
  if (isIgnoreTime) {
    copiedDate1.setHours(0, 0, 0, 0);
    copiedDate2.setHours(0, 0, 0, 0);
  }
  return copiedDate1.getTime() > copiedDate2.getTime();
};

export const subtractDate = (date: Date, subtract: number): Date => {
  const copiedDate = new Date(date.valueOf());
  copiedDate.setDate(copiedDate.getDate() - subtract);

  return copiedDate;
}
