import { fromEvent, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

/**
 * @example
 * sm$ = media(`(max-width: 767px)`);
 * md$ = media(`(min-width: 768px) and (max-width: 1023px)`);
 * lg$ = media(`(min-width: 1024px) and (max-width: 1279px)`);
 * xl$ = media(`(min-width: 1280px) and (max-width: 1535px)`);
 * xl2$ = media(`(min-width: 1536px)`);
 * prefersLight$ = media('(prefers-color-scheme: light)');
 * prefersDark$ = media('(prefers-color-scheme: dark)');
 * prefersReducedMotion$ = media('(prefers-reduced-motion:reduce)');
 * prefersReducedTransparency$ = media('(prefers-reduced-transparency:reduce)');
 * prefersReducedData$ = media('(prefers-reduced-data: reduce)');
 * prefersContrast$ = media('(prefers-contrast:high)');
 * portrait$ = media('(orientation: portrait)');
 * landscape$ = media('(orientation: landscape)');
 * media('(max-width: 767px)').subscribe(matches => console.log(matches));
 */

export const media = (query: string): Observable<boolean> => {
  const mediaQuery = window.matchMedia(query);
  return fromEvent<MediaQueryList>(mediaQuery, 'change').pipe(
    startWith(mediaQuery),
    map((list: MediaQueryList) => list.matches),
  );
};
