export function orderBy<T>(array: T[], keys: (keyof T)[], order: 'asc' | 'desc' = 'asc'): T[] {
  let isSwapped = false;

  for (let index = 0, total = array.length; index < total; index++) {
    isSwapped = false;

    for (let innerIndex = 0; innerIndex < total - index - 1; innerIndex++) {
      let condition: number;

      for (let indexKey = 0, totalKey = keys.length; indexKey < totalKey; indexKey++) {
        const item = array[innerIndex][keys[indexKey]];
        const otherItem = array[innerIndex + 1][keys[indexKey]];

        if (typeof item === 'string') {
          condition = String(item).localeCompare(String(otherItem));
        } else {
          condition = Number(item) - Number(otherItem);
        }

        if (condition) {
          break;
        }
      }

      if ((order === 'asc' && condition > 0) || (order === 'desc' && condition < 0)) {
        [array[innerIndex], array[innerIndex + 1]] = [array[innerIndex + 1], array[innerIndex]];

        isSwapped = true;
      }
    }

    if (!isSwapped) {
      break;
    }
  }

  return array;
}

export const groupBy = <T>(array: T[], key: keyof T) => {
  const result: { [s: string]: T[] } = {};

  for (const element of array) {
    if (!result[element[key] as unknown as string]) {
      result[element[key] as unknown as string] = [];
    }
    result[element[key] as unknown as string].push(element);
  }

  return result;
};

export function uniqueBy<T>(array: T[], key?: keyof T): T[] {
  const flags = new Set();
  const output = new Array<T>();
  for (let index = 0, length = array.length; index < length; index++) {
    const item = array[index];
    const hashKey = key ? item[key] : item;
    if (flags.has(hashKey)) {
      continue;
    }
    flags.add(hashKey);
    output.push(item);
  }

  return output;
}
