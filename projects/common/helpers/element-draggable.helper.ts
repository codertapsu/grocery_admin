import { Renderer2 } from '@angular/core';

import { fromEvent, merge, Observable, Subject } from 'rxjs';
import { filter, map, switchMap, takeUntil, tap } from 'rxjs/operators';

import { Coordinates } from '../models/coordinates.model';
import { getTransformValue } from './get-transform-value.helper';

export class ElementDraggable {
  private readonly _destroyed$: Subject<void>;
  private readonly _positionChange$: Subject<Coordinates>;
  private readonly _document: Document;
  private readonly _element: HTMLElement;
  private readonly _gap: number;
  private readonly _renderer: Renderer2;
  private readonly _window: Window;

  get positionChange$(): Observable<Coordinates> {
    return this._positionChange$.asObservable();
  }

  public constructor(document: Document, element: HTMLElement, gap: number, renderer: Renderer2, window: Window) {
    this._destroyed$ = new Subject<void>();
    this._document = document;
    this._element = element;
    this._gap = gap;
    this._renderer = renderer;
    this._window = window;
    this.init();
  }

  public destroy(): void {
    this._destroyed$.next();
    this._destroyed$.complete();
  }

  private init(): void {
    let currentX: number;
    let currentY: number;
    let initialX: number;
    let initialY: number;
    let isDragging = false;

    const start$ = merge(
      fromEvent<PointerEvent>(this._element, 'mousedown').pipe(
        map(event => ({ clientX: event.clientX, clientY: event.clientY })),
      ),
      fromEvent<TouchEvent>(this._element, 'touchstart').pipe(
        filter(event => event.touches.length === 1),
        map(event => ({ clientX: event.touches[0].clientX, clientY: event.touches[0].clientY })),
      ),
    ).pipe(takeUntil(this._destroyed$));

    const end$ = merge(
      fromEvent<PointerEvent>(this._document, 'mouseup'),
      fromEvent<TouchEvent>(this._document, 'touchend'),
    ).pipe(takeUntil(this._destroyed$));

    const dragging$ = merge(
      fromEvent<PointerEvent>(this._document, 'mousemove').pipe(
        tap(event => event.preventDefault()),
        map(event => ({ clientX: event.clientX, clientY: event.clientY })),
      ),
      fromEvent<TouchEvent>(this._document, 'touchmove').pipe(
        tap(event => event.preventDefault()),
        filter(event => event.touches.length === 1),
        map(event => ({ clientX: event.touches[0].clientX, clientY: event.touches[0].clientY })),
      ),
    ).pipe(takeUntil(end$));

    start$
      .pipe(
        tap(({ clientX, clientY }) => {
          isDragging = true;
          const leftValue = Number(this._window.getComputedStyle(this._element).left.replace('px', ''));
          const topValue = Number(this._window.getComputedStyle(this._element).top.replace('px', ''));
          initialX = clientX - leftValue;
          initialY = clientY - topValue;
          this._renderer.setStyle(this._element, 'transition', 'none');
        }),
        switchMap(() => dragging$),
      )
      .subscribe(({ clientX, clientY }) => {
        currentX = clientX - initialX;
        currentY = clientY - initialY;
        this.updatePosition(currentX, currentY);
      });
    end$.subscribe(() => {
      if (isDragging) {
        isDragging = false;
        initialX = currentX;
        initialY = currentY;
        this.snapToSide();
      }
    });
  }

  private updatePosition(currentX: number, currentY: number): void {
    this._renderer.setStyle(this._element, 'inset', `${currentY}px auto auto ${currentX}px`);
  }

  private snapToSide(): void {
    this._renderer.setStyle(this._element, 'transition', 'left 0.3s ease-in-out, top 0.3s ease-in-out');
    const { x, y } = getTransformValue(this._element);
    const viewportWidth = this._window.innerWidth;
    const leftValue = Number(this._window.getComputedStyle(this._element).left.replace('px', ''));
    const isOnLeft = leftValue < viewportWidth / 2;
    if (isOnLeft) {
      this._renderer.setStyle(this._element, 'left', `${this._gap - x}px`);
      this._renderer.addClass(this._element, 'on-left');
      this._renderer.removeClass(this._element, 'on-right');
    } else {
      this._renderer.setStyle(this._element, 'left', `${viewportWidth + x - this._gap}px`);
      this._renderer.addClass(this._element, 'on-right');
      this._renderer.removeClass(this._element, 'on-left');
    }

    const topValue = Number(this._window.getComputedStyle(this._element).top.replace('px', ''));
    const minTop = this._gap - y;
    if (topValue < minTop) {
      this._renderer.setStyle(this._element, 'top', `${minTop}px`);
    }
    const maxTop = this._window.innerHeight + y - this._gap;
    if (topValue > maxTop) {
      this._renderer.setStyle(this._element, 'top', `${maxTop}px`);
    }
  }
}
