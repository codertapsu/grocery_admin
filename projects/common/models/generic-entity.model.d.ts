export interface GenericEntity<T = number> {
  id?: T;
  uuid?: string;
  name: string;
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
}
