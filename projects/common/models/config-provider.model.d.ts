export interface ConfigProvider {
  apiEndpoint: string;
  themePreferenceKey: string;
}
