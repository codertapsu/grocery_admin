import { AbstractControl, AbstractControlOptions, AsyncValidatorFn, UntypedFormGroup, ValidatorFn } from '@angular/forms';

export class TypedFormGroup<T> extends UntypedFormGroup {
  private _submitted: boolean;

  public override value: T;

  get submitted(): boolean {
    return this._submitted;
  }

  public constructor(
    controls: {
      [P in keyof Partial<T>]: AbstractControl;
    },
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null,
  ) {
    super(controls, validatorOrOpts, asyncValidator);
  }

  public override getRawValue() {
    return super.getRawValue() as T;
  }

  public markAsSubmitted(): void {
    this._submitted = true;
  }

  public override markAsPristine(opts?: { onlySelf?: boolean }): void {
    this._submitted = false;
    super.markAsPristine(opts);
  }
}
