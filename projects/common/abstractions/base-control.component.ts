import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

type VoidCallback<T = unknown> = (_?: T) => void;

@Component({
  selector: 'base-control',
  template: '<div>NO UI TO BE FOUND HERE!</div>',
})
export class BaseControl<T> implements ControlValueAccessor {
  private readonly onChangedFns = new Array<VoidCallback<T>>();
  private readonly onTouchedFns = new Array<VoidCallback>();

  private _readonly: boolean;
  @Input() set readonly(value: boolean) {
    this._readonly = value !== null && `${value}` !== 'false';
  }

  get readonly(): boolean {
    return this._readonly;
  }

  private _value: T;
  get value(): T {
    return this._value;
  }

  public static ngAcceptInputType_readonly: boolean | '';

  protected constructor(protected changeDetectorRef: ChangeDetectorRef) {}

  public registerOnChange(fn: VoidCallback): void {
    this.onChangedFns.push(fn);
  }

  public registerOnTouched(fn: VoidCallback): void {
    this.onTouchedFns.push(fn);
  }

  public writeValue(value: T): void {
    this._value = value;
    this.changeDetectorRef.detectChanges();
  }

  public setDisabledState(isDisabled: boolean): void {
    this.readonly = isDisabled;
  }

  public markAsTouched(): void {
    this.onTouchedFns.forEach(fn => fn());
  }

  public updateValue(value: T): void {
    this._value = value;
    this.onChangedFns.forEach(fn => fn(value));
  }
}
