import { Observable } from 'rxjs';

type IntersectionObserverConfig = IntersectionObserverInit & { isDestroyWhenInvoked?: boolean };
const defaultConfig: IntersectionObserverConfig = {
  threshold: [0.5],
  rootMargin: '100px',
  root: null,
  isDestroyWhenInvoked: false,
};

export function intersectionObserver(
  element: HTMLElement,
  config?: IntersectionObserverConfig,
): Observable<IntersectionObserverEntry> {
  const combinedConfig: IntersectionObserverInit = {
    threshold: config.threshold || defaultConfig.threshold,
    rootMargin: config.rootMargin || defaultConfig.rootMargin,
    root: config.root || defaultConfig.root,
  };

  return new Observable<IntersectionObserverEntry>(subscriber => {
    const observer = new IntersectionObserver((entries, obs) => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          subscriber.next(entry);

          if (config.isDestroyWhenInvoked) {
            obs.unobserve(entry.target);
          }
        }
      });
    }, combinedConfig);

    observer.observe(element);

    return {
      unsubscribe: (): void => observer.disconnect(),
    };
  });
}
