import { Observable } from 'rxjs';

export function resizeObserver(element: HTMLElement): Observable<ResizeObserverEntry[]> {
  return new Observable(subscriber => {
    const resizeObserver = new ResizeObserver((entries: ResizeObserverEntry[]) => subscriber.next(entries));

    resizeObserver.observe(element);

    return {
      unsubscribe: (): void => resizeObserver.unobserve(element),
    };
  });
}
